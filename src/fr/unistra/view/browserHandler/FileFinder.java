package fr.unistra.view.browserHandler;

import static java.nio.file.FileVisitResult.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import fr.unistra.model.beans.FileBean;

/**
 * This Class manages recursive file searching giving a patern and the root folder to be processed
 * @author Abdoul-djawadou SALAOU
 */
public class FileFinder extends SimpleFileVisitor<Path> {
	private PathMatcher	           matcher;
	private Path	               rootFilePath;
	private DefaultTreeModel	   searchModel;
	private FileVisitResult	       stopCriteria	= CONTINUE;
	private DefaultMutableTreeNode	searchRootNode;
	private DefaultMutableTreeNode	childNode;

	public FileFinder(File rootFile, DefaultTreeModel searchModel) {
		rootFilePath = rootFile.toPath();
		this.searchModel = searchModel;

	}

	public void setSeachTerm(String pattern) {
		matcher = FileSystems.getDefault().getPathMatcher("regex:" + "(?i).*" + pattern + ".*");
	}

	// Compares the glob pattern against
	// the file or directory name.
	boolean find(Path file) {
		Path name = file.getFileName();
		if (name != null && matcher.matches(name))
			return true;
		return false;
	}

	// Invoke the pattern matching
	// method on each file.
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
		if (find(file)) {
			childNode = new DefaultMutableTreeNode(new FileBean(file.toFile()));
			searchModel.insertNodeInto(childNode, searchRootNode, searchRootNode.getChildCount());
		}

		return stopCriteria;
	}

	// Invoke the pattern matching
	// method on each directory.
	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
		if (find(dir)) {
			childNode = new DefaultMutableTreeNode(new FileBean(dir.toFile()));
			searchModel.insertNodeInto(childNode, searchRootNode, searchRootNode.getChildCount());
			BrowserPanel.parseNode(childNode, dir.toFile());
			return SKIP_SUBTREE;
		}
		return stopCriteria;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) {
		return stopCriteria;
	}

	public void startSearching(final String searchTerm) {
		stopSearching();
		searchRootNode = new DefaultMutableTreeNode(" Search Results ");
		searchModel.setRoot(searchRootNode);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					setSeachTerm(searchTerm);
					stopCriteria = CONTINUE;
					// System.out.println("Search Started");
					Files.walkFileTree(rootFilePath, FileFinder.this);
					// System.out.println("Search Finished");
				} catch (IOException e) {}
			}
		});
	}

	public void stopSearching() {
		stopCriteria = TERMINATE;
	}

}
