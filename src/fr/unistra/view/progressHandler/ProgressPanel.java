package fr.unistra.view.progressHandler;

import static fr.unistra.view.downloadHandler.DownloadPanel.componentFont;
import static fr.unistra.view.downloadHandler.DownloadPanel.initComponentProperties;

import java.awt.*;

import javax.swing.*;

import fr.unistra.model.interfaces.DownloadEndingProcessor;
import fr.unistra.model.interfaces.ProgressObserver;

/**
 * Manages progressbar, the number of organisms label and the chrono
 * @author Abdoul-djawadou SALAOU
 */
public class ProgressPanel extends JPanel {
	private static final long	    serialVersionUID	= 1L;
	private JProgressBar	        progressBar;
	private JLabel	                nbeOrganismLabel;
	private int	                    lastValue	     = -1;
	private int	                    newValue	     = 0;
	private String	                labelSuffix	     = "Organismes";
	private boolean	                isLocked	     = false;
	private Chrono	                chrono;
	private boolean	                startChrono	     = true;
	private DownloadEndingProcessor	taskEndProcessor;

	public ProgressPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setOpaque(false);

		final Component verticalStrut = Box.createVerticalStrut(30);
		add(verticalStrut);

		progressBar = new JProgressBar(0, 100);
		progressBar.setStringPainted(true);
		progressBar.setFont(new Font("SansSerif", Font.BOLD, 15));
		progressBar.setValue(0);
		progressBar.setFont(componentFont);
		changeProgressBarColor(0);
		add(progressBar);
		add(Box.createVerticalStrut(10));

		nbeOrganismLabel = new JLabel("Organisms");
		initComponentProperties(nbeOrganismLabel);
		nbeOrganismLabel.setFont(new Font("SansSerif", Font.BOLD, 15));
		nbeOrganismLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(nbeOrganismLabel);
		add(Box.createVerticalStrut(10));
		chrono = new Chrono();
		add(chrono);
		add(Box.createVerticalStrut(10));
	}

	/**
	 * Reset the progressbar by reverse animating it to 0
	 */
	public void resetProgressBar() {
		lastValue = -1;
		newValue = 0;
		chrono.stop();
		if (isLocked)
			return;
		isLocked = true;
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				int value = progressBar.getValue();
				value = value < 0 ? 0 : value;
				while (value >= 0) {
					changeProgressBarColor(value);
					progressBar.setValue(value);
					try { // dummy task
						Thread.sleep(50);
					} catch (InterruptedException ie) {}
					value--;
				}
				isLocked = false;
				startChrono = true;
				chrono.reset();
				return null;
			}
		};
		worker.execute();
	}

	public void stopProgress() {
		progressBar.setValue(100);
		chrono.stop();
	}

	public void changeProgressBarColor(int progress) {

		if (progress == 0 || progress == 40 || progress == 66)
			paintProgressBar(new Color(72, 209, 204));
		else if (progress == 41 || progress == 65)
			paintProgressBar(new Color(26, 235, 26));
	}

	public void paintProgressBar(Color color) {
		UIDefaults defaults = new UIDefaults();
		ProgressBarPainter barPainter = new ProgressBarPainter(color);
		defaults.put("ProgressBar[Enabled].foregroundPainter", barPainter);
		defaults.put("ProgressBar[Enabled+Finished].foregroundPainter", barPainter);
		progressBar.putClientProperty("Nimbus.Overrides.InheritDefaults", Boolean.TRUE);
		progressBar.putClientProperty("Nimbus.Overrides", defaults);
	}

	public void updateInfoLabel(int numberOfOrganism) {
		if (numberOfOrganism >= 0)
			nbeOrganismLabel.setText(numberOfOrganism + " " + labelSuffix);
		else
			nbeOrganismLabel.setText("");
	}

	public void updateInfoLabel(int numberProcessed, int numberOfOrganism) {
		String text = numberProcessed + " / " + numberOfOrganism + " " + labelSuffix;
		nbeOrganismLabel.setText(text);
	}

	public JLabel getNbeOrganismLabel() {
		return nbeOrganismLabel;
	}

	public void setNbeOrganismLabel(JLabel nbeOrganismLabel) {
		this.nbeOrganismLabel = nbeOrganismLabel;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public String getLabelSuffix() {
		return labelSuffix;
	}

	public void setLabelSuffix(String labelSuffix) {
		this.labelSuffix = labelSuffix;
		if (this.labelSuffix.toLowerCase() == "organisms")
			chrono.setDayString("D");
		else
			chrono.setDayString("J");
	}

	public DownloadEndingProcessor getTaskEndProcessor() {
		return taskEndProcessor;
	}

	public void setTaskEndProcessor(DownloadEndingProcessor taskEndProcessor) {
		this.taskEndProcessor = taskEndProcessor;
	}

	public class TaskProgressObserver implements ProgressObserver {

		@Override
		public void updateProgressValue(int currentValue, int maxValue) {
			maxValue = maxValue != 0 ? maxValue : 1;
			newValue = (100 * currentValue) / maxValue;
			updateInfoLabel(currentValue, maxValue);
			if (newValue == lastValue)
				return;

			lastValue = newValue;
			changeProgressBarColor(newValue);
			if (newValue != 100)
				progressBar.setValue(newValue);
			else {
				if (taskEndProcessor != null)
					taskEndProcessor.process();
				else {
					progressBar.setValue(newValue);
					chrono.stop();
				}
			}
			if (startChrono) {
				chrono.start();
				startChrono = false;
			}

		}

	}

}
