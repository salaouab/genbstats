package fr.unistra.model.beans;

import static fr.unistra.utils.FileUtil.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import fr.unistra.controller.organismParser.*;
import fr.unistra.model.interfaces.ProgressObserver;

/**
 * Gets the organism name and associated bioproject list
 * @author Abdoul-Djawadou SALAOU
 */
public class OrganismList {
	private ArrayList<Organism>	organismList	= new ArrayList<Organism>();
	private int	                cursorPosition	= 0;
	private int	                numberOfOrganim	= 0;
	private ProgressObserver	progressObserver;

	/**
	 * Default constructor
	 * @throws IOException
	 */
	public OrganismList() {}

	public OrganismList(boolean loadByURL) throws IOException {
		if (loadByURL)
			loadOrganismDataByURL();
		else
			loadOrganismData();
	}

	/**
	 * Check wether {@link Organism} is available in the list
	 * @return Return <b>true</b> if some organism is available in the list <b>false</b> otherwise
	 */
	public boolean isOrganismAvailable() {
		return cursorPosition < numberOfOrganim;
	}

	/**
	 * Reset the {@link Organism} reader cursor to zero
	 */
	public void resetCursor() {
		cursorPosition = 0;
	}

	/**
	 * Return the next {@linkOrganism}.
	 * @return return the {@linkOrganism} if it is available, null otherwise
	 */
	public synchronized Organism getNextOrganism() {
		try {
			if (progressObserver != null)
				progressObserver.updateProgressValue(cursorPosition + 1, numberOfOrganim);
			return organismList.get(cursorPosition++);
		} catch (Exception exception) {
			return null;
		}
	}

	/**
	 * Return the {@link Organism} list size
	 * @return return the size of orgnism list
	 */
	public int getNumberOfOrganism() {
		return numberOfOrganim;
	}

	/**
	 * Load {@link Organism} into the list
	 * @throws IOException
	 */
	private void loadOrganismData() throws IOException {
		organismList.addAll(new EukaryoteOrganismParser(EUKARYOTES_PATH).loadOrganisms());
		organismList.addAll(new ProkaryoteOrganismParser(PROKARYOTES_PATH).loadOrganisms());
		organismList.addAll(new VirusOrganismParser(VIRUSES_PATH).loadOrganisms());
		numberOfOrganim = organismList.size();
	}

	public void shuffle() {
		Collections.shuffle(organismList);
	}

	private void loadOrganismDataByURL() throws IOException {
		organismList.addAll(new EukaryoteOrganismParser().loadOrganismsByURL());
		organismList.addAll(new ProkaryoteOrganismParser().loadOrganismsByURL());
		organismList.addAll(new VirusOrganismParser().loadOrganismsByURL());
		numberOfOrganim = organismList.size();
	}

	/**
	 * Return the {@link Organism} reader curson position
	 * @return Return the cursor postion
	 */
	public int getCursorPosition() {
		return cursorPosition;
	}

	/**
	 * Set the cursor position
	 * @param organismNameCursorPosition : the cursor postion
	 */
	public void setCursorPosition(int organismNameCursorPosition) {
		cursorPosition = organismNameCursorPosition;
	}

	public void clear() {
		organismList.clear();
		numberOfOrganim = 0;
		cursorPosition = 0;
	}

	public void add(ArrayList<Organism> organismList) {
		if (organismList != null) {
			this.organismList.addAll(organismList);
			numberOfOrganim = this.organismList.size();
		}
	}

	public ProgressObserver getProgressObserver() {
		return progressObserver;
	}

	public void setProgressObserver(ProgressObserver progressObserver) {
		this.progressObserver = progressObserver;
	}

}
