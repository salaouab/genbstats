/**
 *
 */
package fr.unistra.view.downloadHandler;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.MatteBorder;

import fr.unistra.view.actionsHandler.HelpAction;
import fr.unistra.view.language.Messages;

/**
 * Holds some menu buttons:<br/>
 * - Show/Hide file browser<br/>
 * - Show/Hide the console<br/>
 * - Switch languages<br/>
 * - The About & Quit button
 * @author Abdoul-djawadou SALAOU
 */
public class MenuPanel extends JPanel {
	private static final long	serialVersionUID	= 1L;
	private JButton	          helpButton;
	private JButton	          langButton;
	private JToggleButton	  displayBrowserButton;
	private JToggleButton	  displayConsoleButton;
	private JButton	          aboutButton;
	private JButton	          quitButton;

	public MenuPanel() {
		Font font = new Font("SansSerif", Font.BOLD, 12);
		Color borderColor = new Color(72, 209, 204);
		setBackground(Color.DARK_GRAY);
		setBorder(new MatteBorder(0, 0, 5, 0, borderColor));
		setLayout(new FlowLayout(FlowLayout.CENTER, 15, 5));

		helpButton = new JButton("  Help  ");
		helpButton.setFont(font);
		helpButton.setIcon(new ImageIcon(DownloadFrame.class.getResource("/image/help.png")));
		helpButton.addActionListener(new HelpAction(5));
		add(helpButton);

		displayBrowserButton = new JToggleButton(" Browser ");
		displayBrowserButton.setFont(font);
		displayBrowserButton.setIcon(new ImageIcon(DownloadFrame.class.getResource("/image/folder.png")));
		add(displayBrowserButton);

		displayConsoleButton = new JToggleButton(" Console ");
		displayConsoleButton.setFont(font);
		displayConsoleButton.setIcon(new ImageIcon(DownloadFrame.class.getResource("/image/console.png")));
		add(displayConsoleButton);

		langButton = new JButton("Language");
		langButton.setFont(font);
		langButton.setIcon(new ImageIcon(DownloadFrame.class.getResource("/image/flag.png")));
		add(langButton);

		aboutButton = new JButton(" About ");
		aboutButton.setFont(font);
		aboutButton.setIcon(new ImageIcon(DownloadFrame.class.getResource("/image/about.png")));
		aboutButton.addActionListener(new HelpAction(4));
		add(aboutButton);

		quitButton = new JButton(" Quit ");
		quitButton.setFont(font);
		quitButton.setIcon(new ImageIcon(DownloadFrame.class.getResource("/image/close.png")));
		quitButton.addActionListener(new QuitAction());
		add(quitButton);

		setMaximumSize(new Dimension(Integer.MAX_VALUE, getPreferredSize().height + 50));
		resetText();
	}

	public JButton getLangButton() {
		return langButton;
	}

	public void setLangButton(JButton langButton) {
		this.langButton = langButton;
	}

	public JToggleButton getDisplayConsoleButton() {
		return displayConsoleButton;
	}

	public void setDisplayConsoleButton(JToggleButton displayConsoleButton) {
		this.displayConsoleButton = displayConsoleButton;
	}

	public JButton getAboutButton() {
		return aboutButton;
	}

	public void setAboutButton(JButton aboutButton) {
		this.aboutButton = aboutButton;
	}

	public JToggleButton getDisplayBrowserButton() {
		return displayBrowserButton;
	}

	public void setDisplayBrowserButton(JToggleButton displayBrowserButton) {
		this.displayBrowserButton = displayBrowserButton;
	}

	public JButton getQuitButton() {
		return quitButton;
	}

	public void resetText() {

		helpButton.setText(Messages.getString("menuHelpButton"));
		langButton.setText(Messages.getString("langButton"));
		displayBrowserButton.setText(Messages.getString("displayBrowserButton"));
		displayConsoleButton.setText(Messages.getString("displayConsoleButton"));
		aboutButton.setText(Messages.getString("aboutButton"));
		quitButton.setText(Messages.getString("quitButton"));
	}

	class QuitAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}

	}
}
