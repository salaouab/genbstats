package fr.unistra.controller.dataParser;

import static fr.unistra.utils.MainConstants.PRINT_LOGS;
import static fr.unistra.utils.MainConstants.getIntValue;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.algosome.eutils.io.InputStreamParser;

/**
 * Handle Esearch querry ids retreiving.
 * 
 * @author Abdoul-djawadou SALAOU
 */
public class ESearchIdParser implements InputStreamParser {
	public int					numberOfIds	= 0;
	public int					returnStart	= 0;
	public int					returnMax	= 0;
	public String				webEnv		= "";
	public String				queryKey;
	private ArrayList<String>	idList;

	/**
	 * Default Constructor
	 */
	public ESearchIdParser() {
		idList = new ArrayList<String>();
	}

	/**
	 * Constructor with id holder List provided.
	 * 
	 * @param idList
	 *            A list that will hold ids retreive
	 */
	public ESearchIdParser(ArrayList<String> idList) {
		this.idList = idList;
	}

	/**
	 * Parse and retreive the genome ids from the input stream.
	 * 
	 * @see com.algosome.eutils.io.InputStreamParser#parseInput(java.io.InputStream)
	 */
	@Override
	public void parseInput(InputStream inputStream) throws IOException {
		InputStreamReader streamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(streamReader);
		String line = null;
		String term;
		idList.clear();
		while ((line = bufferedReader.readLine()) != null) {

			// get Id
			if (line.indexOf("<Id>") != -1) {
				term = parseTextFromLine(line, "Id");
				if (!term.trim().isEmpty()) {
					idList.add(term);
				}
			}
			// Number of ids
			if (line.indexOf("</Count><Ret") != -1) {
				term = parseTextFromLine(line, "Count");
				numberOfIds = getIntValue(term);
			}
			// number of ids retreive
			if (line.indexOf("<RetMax>") != -1) {
				term = parseTextFromLine(line, "RetMax");

				returnMax = getIntValue(term);
			}
			// fetch offset
			if (line.indexOf("<RetStart>") != -1) {
				term = parseTextFromLine(line, "RetStart");
				returnStart = getIntValue(term);
			}
			// webEnv
			if (line.indexOf("<WebEnv>") != -1) {
				webEnv = parseTextFromLine(line, "WebEnv");
			}
			// query key
			if (line.indexOf("<QueryKey>") != -1) {
				queryKey = parseTextFromLine(line, "QueryKey");
			}

		}
		try {
			if (PRINT_LOGS) {
				System.out.println("==> WebEnv: " + webEnv);
				System.out.print("\tNumber of Ids: " + numberOfIds);
				System.out.print(" | Number of Ids retreive: " + returnMax);
				System.out.println(" | Fetch offset: " + returnStart + "\n");
			}
			bufferedReader.close();
			streamReader.close();
			inputStream.close();

		} catch (Exception e) {
		}

	}

	@Override
	public void parseFrom(int start) {
	}// do nothing

	@Override
	public void parseTo(int end) {
	}// do nothing

	public ArrayList<String> getIdList() {
		return idList;
	}

	public void setIdList(ArrayList<String> idList) {
		this.idList = idList;
	}

	/**
	 * Parses out an XML value tag out of line using a regular expression.
	 * 
	 * @param line
	 *            A line of text to parse.
	 * @param tag
	 *            The XML tag identifier.
	 * @return A string representation of the text between the given tags, or an
	 *         empty string if nothing was found.
	 */
	protected String parseTextFromLine(String line, String tag) {
		Pattern pattern = Pattern.compile(tag + ">(.+)</" + tag);
		Matcher matcher = pattern.matcher(line);
		matcher.find();
		try {
			return matcher.group(1);
		} catch (IllegalStateException ise) {
			return "";
		}
	}

}