package fr.unistra.view.consoleHandler;

import java.awt.*;

import javax.swing.JTextPane;
import javax.swing.UIDefaults;

/**
 * Redirect the standard output/error to a text pane.
 * @author Abdoul-djawadou SALAOU
 */
public class ConsoleMessagePanel extends JTextPane {
	private static final long	serialVersionUID	= 1L;

	public ConsoleMessagePanel() {
		setFont(new Font("SansSerif", Font.BOLD, 15));
		setMargin(new Insets(3, 2, 5, 1));

		UIDefaults defaults = new UIDefaults();
		defaults.put("TextPane[Enabled].backgroundPainter", Color.DARK_GRAY);
		putClientProperty("Nimbus.Overrides", defaults);
		putClientProperty("Nimbus.Overrides.InheritDefaults", true);
		setBackground(Color.DARK_GRAY);

		// outPut/error redirector
		MessageConsole console = new MessageConsole(this);
		// console.redirectErr(new Color(72, 209, 204), null);
		console.redirectOut(Color.WHITE, null);
		// Freeze the download panel...why???
		// console.setMessageLines(1000);

	}

}
