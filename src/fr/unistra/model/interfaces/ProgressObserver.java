package fr.unistra.model.interfaces;

/**
 * Service to be implemented by the download progress observer
 * @author Abdoul-djawadou SALAOU
 */
public interface ProgressObserver {
	void updateProgressValue(int currentValue, int maxValue);
}
