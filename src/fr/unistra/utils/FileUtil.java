package fr.unistra.utils;

import static fr.unistra.utils.MainConstants.KINGDOM;
import static fr.unistra.utils.MainConstants.LOCUS;
import static fr.unistra.utils.MainConstants.containsTerm;
import static fr.unistra.utils.MainConstants.getDateString;

import java.io.*;
import java.net.URI;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.InvalidOperationException;

import fr.unistra.model.textFiileHandler.FTPFileDownloader;

/**
 * This class holds some utils methods on file managing
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class FileUtil {
	public static final String	FILE_SEPARATOR	                              = File.separator;
	public static final String	BASE_DIRECTORY	                              = "Application_data/";
	public static final String	ORGANISM_BASE_DIRECTORY	                      = BASE_DIRECTORY + KINGDOM + "/";
	public static final String	GENOME_REPORT_DIRECTORY	                      = BASE_DIRECTORY + "Genome_report_files/";
	public static final String	README_PATH	                                  = GENOME_REPORT_DIRECTORY + "README";
	public static final String	OVERVIEW_PATH	                              = GENOME_REPORT_DIRECTORY + "overview.txt";
	public static final String	PLASMIDS_PATH	                              = GENOME_REPORT_DIRECTORY + "plasmids.txt";
	public static final String	EUKARYOTES_PATH	                              = GENOME_REPORT_DIRECTORY + "eukaryotes.txt";
	public static final String	PROKARYOTES_PATH	                          = GENOME_REPORT_DIRECTORY + "prokaryotes.txt";
	public static final String	PROKARYOTES_REFERENCE_GENOME_PATH	          = GENOME_REPORT_DIRECTORY + "prok_reference_genomes.txt";
	public static final String	PROKARYOTES_REPRESENTATIVE_GENOME_PATH	      = GENOME_REPORT_DIRECTORY + "prok_representative_genomes.txt";
	public static final String	VIRUSES_PATH	                              = GENOME_REPORT_DIRECTORY + "viruses.txt";
	/************* Server Path ****************************/
	public static final String	NCBI_FTP_SERVER	                              = "ftp.ncbi.nlm.nih.gov";
	public static final String	SERVER_GENOME_REPORT_PATH	                  = "/genomes/GENOME_REPORTS/";
	public static final String	SERVER_README_PATH	                          = SERVER_GENOME_REPORT_PATH + "README";
	public static final String	SERVER_OVERVIEW_PATH	                      = SERVER_GENOME_REPORT_PATH + "overview.txt";
	public static final String	SERVER_PLASMIDS_PATH	                      = SERVER_GENOME_REPORT_PATH + "plasmids.txt";
	public static final String	SERVER_EUKARYOTES_PATH	                      = SERVER_GENOME_REPORT_PATH + "eukaryotes.txt";
	public static final String	SERVER_PROKARYOTES_PATH	                      = SERVER_GENOME_REPORT_PATH + "prokaryotes.txt";
	public static final String	SERVER_PROKARYOTES_REFERENCE_GENOME_PATH	  = SERVER_GENOME_REPORT_PATH + "prok_reference_genomes.txt";
	public static final String	SERVER_PROKARYOTES_REPRESENTATIVE_GENOME_PATH	= SERVER_GENOME_REPORT_PATH + "prok_representative_genomes.txt";
	public static final String	SERVER_VIRUS_PATH	                          = SERVER_GENOME_REPORT_PATH + "viruses.txt";

	/**
	 * Return the last modification date in the text file
	 * 
	 * @param filePath : text file path
	 * @return Return the last modification date. If an error occurs the default 01-JAN-1000 date is
	 *         return
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public static Date getLastModificationDate(String filePath) {
		File file = new File(filePath);
		Date defaultDate = new Date("01-JAN-1000");

		if (!file.isFile())
			return defaultDate;
		try {
			FileReader reader = new FileReader(file);
			BufferedReader br = new BufferedReader(reader);
			String line;
			while ((line = br.readLine()) != null) {
				if (containsTerm(line, LOCUS)) {
					defaultDate = new Date(getDateString(line));
					break;
				}
			}
			br.close();
			reader.close();
		} catch (InvalidOperationException ex) {
			file.delete();
		} catch (Exception exception) {}
		return defaultDate;
	}

	public static String readAllContent(URI fileURI) {
		return readAllContent(new File(fileURI));
	}

	public static String readAllContent(String fileName) {
		File file = new File(fileName);
		return readAllContent(file);
	}

	public static String readAllContent(File file) {
		String data = "";
		try {
			// si le fichier n'existe pas
			if (!file.exists())
				return data;

			FileReader fileReader = new FileReader(file.getAbsoluteFile());
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null)
				data += line;

			bufferedReader.close();
			fileReader.close();
		} catch (IOException e) {
			data = "Unable to load the file";
			if (file != null)
				data += " -" + file.getName() + " -";
		}
		return data;
	}

	public static String readAllContent(InputStream fileStream) {
		String data = "";
		try {

			InputStreamReader fileReader = new InputStreamReader(fileStream);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null)
				data += line;

			bufferedReader.close();
			fileReader.close();
			fileStream.close();
		} catch (IOException e) {}
		return data;
	}

	public static void main(String[] args) {
		FTPFileDownloader.updateGenomeReportFiles();
	}

}
