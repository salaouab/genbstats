package fr.unistra.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainConstants {

	public static final String	 GENE_ID	            = "gene id";
	public static final String	 PROTEIN	            = "protein";
	public static final String	 PROTEIN_ID	            = "protein id";
	public static final String	 LOCATION	            = "location";
	public static final String	 CHROMOSOME	            = "Chromosome";
	public static final String	 MITOCHONDRION	        = "Mitochondrion";
	public static final String	 PLASMID	            = "Plasmid";
	public static final String	 PLASTID	            = "Plastid";
	public static final String	 CHLOROPLAST	        = "Chloroplast";
	public static final String	 APICOPLAST	            = "Apicoplast";
	public static final String	 CHROMOPLAST	        = "Chromoplast";
	public static final String	 LEUCOPLAST	            = "Leucoplast";
	public static final String	 CYANELLE	            = "Cyanelle";
	public static final String	 CHROMATOPHORE	        = "Chromatophore";
	public static final String	 DNA	                = "DNA";
	public static final String	 RNA	                = "RNA";
	public static final String	 LINKAGE_GROUP	        = "Linkage Group";
	public static final String	 LOCUS	                = "Locus";
	public static final String	 ACCESSION	            = "Accession";
	public static final String	 DEFINITION	            = "Definition";
	public static final String	 ORGANISM	            = "Organism";
	public static final String	 ORGANISM_TAXONOMY	    = "Taxonomy";
	public static final String	 REFERENCE	            = "Reference";
	public static final String	 TITLE	                = "Title";
	public static final String	 ORGANISM_NAME	        = "Organism Name";
	public static final String	 BIOPROJECT	            = "BioProject";
	public static final String	 KINGDOM	            = "Kingdoms";
	public static final String	 GROUP	                = "Group";
	public static final String	 SUB_GROUP	            = "SubGroup";
	public static final String	 NUMBER_OF_NUCLEOTIDE	= "Number of nucleotides";
	public static final String	 NUMBER_OF_SEQUENCE	    = "Number of cds sequences";
	public static final String	 NUMBER_OF_INVALID_CDS	= "Number of invalid cds";
	public static final String	 UNFOUND	            = "Unfound";
	public static final String	 UNKNOWN	            = "Unknown";
	public static final String	 LAST_MODIFICATION_DATE	= "Modification date";
	public static final String	 GENETIC_STRUCTURE_TYPE	= "Genetic structure type";
	public static final String	 ORGANISM_SHEET_NAME	= "Orgaism file name";
	public static final String	 ORGANISM_FILE_PATH	    = "Orgaism file directory";
	public static final String	 EMAIL	                = "abdoul-djawadou.salaou@etu.unistra.fr";
	public static final String	 EXCEL_FILE_EXTENSION	= ".xlsx";
	public static final String	 TEXT_FILE_EXTENSION	= ".txt";
	private static final String	 PUNCT_SPACE	        = "[!\"#$%&'()*+,-./:;<=>?@\\\\^_`{|}~\\[\\]\\s]";
	public static final String[]	genomeType	        = { CHROMOSOME, PLASMID, MITOCHONDRION, PLASTID, CHLOROPLAST, CHROMOPLAST, APICOPLAST,
		LEUCOPLAST, CYANELLE, CHROMATOPHORE, DNA, RNA, LINKAGE_GROUP, UNKNOWN };
	/** Print download evolution logs in the console */
	public static boolean	     PRINT_LOGS	            = true;
	/** Print debug (exception/error) information in the console */
	public static boolean	     DEBUG	                = false;
	/** Flag to interrupt all background threads */
	public static boolean	     INTERUPTOR_FLAG	    = false;

	/**
	 * Verify whether the <b>line</b> contains the <b>term</b>
	 * 
	 * @param line the line to analyse
	 * @param term term to search for
	 * @return Return <b>true</b> if it starts with the term, <b>false</b> otherwise
	 */
	public static boolean startWithTerm(String line, String term) {
		if (line == null)
			return false;

		String regex = "^(?i)" + term.trim() + ".*";
		return line.trim().matches(regex);
	}

	/**
	 * Verify whether the <b>line</b> contains the <b>term</b>
	 * 
	 * @param line the line to analyse
	 * @param term term to search for
	 * @return Return <b>true</b> if it starts with the term, <b>false</b> otherwise
	 */
	public static boolean startWithKingdom(String line) {
		if (line == null)
			return false;

		if (line.trim().matches("^(?i)Bacteri.*"))
			return true;

		if (line.trim().matches("^(?i)Eukaryot.*"))
			return true;

		if (line.trim().matches("^(?i)Archaea.*"))
			return true;

		if (line.trim().matches("^(?i)Viruses.*"))
			return true;

		if (line.trim().matches("^(?i)Viroids.*"))
			return true;

		return false;
	}

	/**
	 * Verify whether the <b>line</b> contains the <b>term</b>
	 * 
	 * @param line : the line to analyse
	 * @param term : term to search for
	 * @return Return <b>true</b> if it contains the term, <b>false</b> otherwise
	 */
	public static boolean containsTerm(String line, String term) {
		String regex = ".*(?i)" + term.trim() + ".*";
		return line.trim().matches(regex);
	}

	/**
	 * Stops the exécution of the caller thread for number of "second"
	 * 
	 * @param second : number of second to sleep the thread
	 */
	public static void sleep(double second) {
		try {
			Thread.sleep((int) (second * 1000));
		} catch (Exception e) {}
	}

	/**
	 * Return integer value of a string and handle exceptions
	 * 
	 * @param intValue : the string value
	 * @return Return the integer value, or 0 if an exception occur
	 */
	public static int getIntValue(String intValue) {
		try {
			return (int) Double.parseDouble(intValue.trim());
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Return long value of a string and handle exceptions
	 * 
	 * @param longValue : the string value
	 * @return Return the integer value, or 0 if an exception occure
	 */
	public static long getLongValue(String longValue) {
		try {
			return (long) Double.parseDouble(longValue.trim());
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Format the organism name for file naming, by replacing white and unwanted character by
	 * '<b>-</b>'
	 * 
	 * @param name : Contains the organism name
	 * @return Return a string optimised for file naming
	 */
	static public String normalise(String name) {
		return name.replaceAll(PUNCT_SPACE, "_").replaceAll("__+", "_");
	}

	/**
	 * Get some giving term.
	 * 
	 * @param token :Term to fecht
	 * @param geneInfos : String containing gene infos,specific formatted.
	 * @return return the tocken
	 */
	static public String getTerm(String token, String geneInfos) {
		try {
			return geneInfos.split(token + "=")[1].split("\\]")[0].trim();
		} catch (Exception exception) {}
		return " ";
	}

	/**
	 * Return the gene id
	 * 
	 * @param geneInfos : String containing gene infos,specific formatted.
	 * @return Return a string representing the gene id
	 * @see #getTerm(String, String)
	 */
	static public String getGene(String geneInfos) {
		return getTerm(GENE_ID, geneInfos);
	}

	/**
	 * Return the protein name
	 * 
	 * @param geneInfos : String containing gene infos,specific formatted.
	 * @return Return a string representing the protein name
	 * @see #getTerm(String, String)
	 */
	public String getProteinName(String geneInfos) {
		return getTerm(PROTEIN, geneInfos);
	}

	/**
	 * Return the protein Id
	 * 
	 * @param geneInfos : String containing gene infos,specific formatted.
	 * @return Return a string representing the protein Id
	 * @see #getTerm(String, String)
	 */
	public String getProteinId(String geneInfos) {
		return getTerm(PROTEIN_ID, geneInfos);
	}

	/**
	 * Return the gene locus reference
	 * 
	 * @param geneInfos : String containing gene infos,specific formatted.
	 * @return Return a string representing the gene locus reference
	 * @see #getTerm(String, String)
	 */
	static public String getLocusId(String geneInfos) {
		String locusInfo = geneInfos.split("_cds_")[0].trim();
		return locusInfo.substring(locusInfo.indexOf('|') + 1);
	}

	/**
	 * Extract date from the String field
	 * 
	 * @param field : String containing date to extract
	 * @return Return date in String
	 */
	static public String getDateString(String field) {
		Matcher matcher = Pattern.compile("([0-9]{2}-[a-zA-Z]{3}-[0-9]{4})").matcher(field);
		if (matcher.find())
			return matcher.group(1).trim();
		return "";
	}
}
