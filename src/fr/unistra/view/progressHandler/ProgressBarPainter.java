package fr.unistra.view.progressHandler;

import java.awt.Color;
import java.awt.Graphics2D;

import javax.swing.JProgressBar;
import javax.swing.Painter;

/**
 * Change a progressbar color
 * @author Abdoul-djawadou SALAOU
 */
public class ProgressBarPainter implements Painter<JProgressBar> {
	private final Color	color;

	public ProgressBarPainter(Color color) {
		this.color = color;
	}

	@Override
	public void paint(Graphics2D gd, JProgressBar t, int width, int height) {
		gd.setColor(color);
		gd.fillRect(0, 0, width, height);
	}

}
