package fr.unistra.controller.organismParser;

import java.util.ArrayList;

import fr.unistra.model.beans.Organism;

/**
 * Parses Eukaryote {@link Organism}s name and associated bioProject
 * 
 * @see OrganismParser
 * @author Abdoul-Djawadou SALAOU
 */
public class EukaryoteOrganismParser extends OrganismParser {
	private int	numberOfChromosomesIndex	= -1;
	private int	numberOfOrganellesIndex		= -1;
	private int	numberOfPlasmsidsIndex		= -1;

	/**
	 * Default Constructor
	 */
	public EukaryoteOrganismParser() {
		this("");
	}

	/**
	 * Constructor with file path informed
	 * 
	 * @param filePath
	 *            : the {@link Organism} file path
	 */
	public EukaryoteOrganismParser(String filePath) {
		super(filePath);
		organismUrl = URL_BASE + "&orgn=&report=euks&status=50|40|30|20|&group=--%20All%20Eukaryota%20--&subgroup=--%20All%20Eukaryota%20--";
		kingdomName = "Eukaryotes";
	}

	/**
	 * Check wether to save this {@link Organism} or not.
	 * 
	 * @param line
	 *            : holds organism information
	 * @return Return <b>true</b> if the organism worth saving, <b>false</b>
	 *         otherwise
	 * @see OrganismParser#isThisOrganismWorthSaving(String)
	 */
	@Override
	protected boolean isThisOrganismWorthSaving(String line) {
		fieldsData = line.split(FIELD_SEPARATOR);
		try {
			if (useRepliconsField)
				return !fieldsData[numberOfRepliconsIndex].trim().matches("-");

			if (!fieldsData[numberOfChromosomesIndex].trim().matches("-"))
				return true;
			if (!fieldsData[numberOfOrganellesIndex].trim().matches("-"))
				return true;
			if (!fieldsData[numberOfPlasmsidsIndex].trim().matches("-"))
				return true;
		} catch (Exception exception) {
		}

		return false;
	}

	/**
	 * Extract some organism spécific information indexes
	 * 
	 * @param line
	 *            : holds the organism information
	 * @return Return <b>true</b> if it the extraction succeded, <b>false</b>
	 *         otherwise
	 * @see OrganismParser#extractIndexes(String)
	 */
	@Override
	protected boolean extractIndexes(String line) {

		if (super.extractIndexes(line)) {
			numberOfChromosomesIndex = getNumberOfChromosomesIndex(line);
			numberOfOrganellesIndex = getNumberOfOrganellesIndex(line);
			numberOfPlasmsidsIndex = getNumberOfPlasmidsIndex(line);
			return true;
		}

		return false;
	}

	/**
	 * Get the index of field giving the number of chromosomes
	 * 
	 * @param line
	 *            : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	private int getNumberOfChromosomesIndex(String line) {
		return getTermIndex(line, ".*chromosome.");
	}

	/**
	 * Get the index of field giving the number of organelles
	 * 
	 * @param line
	 *            : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	private int getNumberOfOrganellesIndex(String line) {
		return getTermIndex(line, ".*organelle.");
	}

	/**
	 * Get the index of field giving the number of plasmids
	 * 
	 * @param line
	 *            : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	private int getNumberOfPlasmidsIndex(String line) {
		return getTermIndex(line, ".*plasmid.");
	}

	public static void main(String[] args) {
		ArrayList<Organism> list = new EukaryoteOrganismParser().loadOrganismsByURL();
		int i = 0;
		System.out.println("===>> Total organism  " + list.size() + "\n");
		for (Organism organism : list) {
			System.out.println(organism);
			if (i++ > 20)
				break;
		}
	}

}
