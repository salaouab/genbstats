package fr.unistra.Launcher;

import java.io.IOException;

import fr.unistra.controller.dataFetchingHandler.NcbiDataFetchingWorker;
import fr.unistra.controller.downloadManagement.DownloadManager;
import fr.unistra.model.beans.DownloadParameters;

/**
 * Download data over consoleby using default parameters, means: - All kingdoms are downloaded<br/>
 * - Only stats over the codon apparition in phase 0, 1 & 2 is performed <br/>
 * - Hierrachical sum is computed at the end of downloading to agglomerate<br>
 * Results are presented in excel files under Application_data/Kingdoms folder.This folder is
 * located in the project tree. <b>Note</b> You can submit integer argument to the programme. This
 * argument will be taken to be a cursor position of the download. This is a convenient way to
 * resume download from its last position.
 * @author Abdoul-djawadou SALAOU
 */
public class ConsoleLauncher {
	/**
	 * Help to resum a download from some postion instead of starting from begining agin
	 */
	private static int	CURSOR_POSITION	= 0;

	/**
	 * Download data over console. <b>Note</b> You can submit integer argument to the programme.
	 * This argument will be taken to be a cursor position of the download. This is a convenient way
	 * to resume download from its last postion.
	 * @param args Main arguments: You can submit integer argument that stands for cursor start
	 *            position. For other option you can get {@link #DownloadParameter} to fine from
	 *            {@link #DownloadManager}
	 * @see DownloadManager
	 * @see DownloadParameters
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		// cursor position handling
		if (args.length != 0)
			try {
				CURSOR_POSITION = Integer.parseInt(args[0]);
				CURSOR_POSITION = CURSOR_POSITION > 0 ? CURSOR_POSITION : 0;
			} catch (Exception e) {}
		// initializing the download manager
		DownloadManager downloadManager = new DownloadManager();
		// downloadManager.getDownloadParameters()
		downloadManager.setCursorPosition(CURSOR_POSITION);
		// if kingdoms are well loaded
		if (downloadManager.processDownloadParameters())
			downloadManager.startDownload();
		else
			System.err.println("Unable to download the organisms list. Please check your internet connection and try again!");
		NcbiDataFetchingWorker.COMPUTE_HIERARCHICAL_SUM = true;
	}

}
