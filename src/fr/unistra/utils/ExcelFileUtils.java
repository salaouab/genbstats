package fr.unistra.utils;

import static fr.unistra.utils.MainConstants.*;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * This class holds some util methods on excel file managing. It also contains some static cell row
 * and colum
 * 
 * @author Abdoul-Djawadou
 */
public class ExcelFileUtils {

	public static final int	     ADDITIONAL_DATA_WIDTH	    = 5;
	public static final int	     ADDITIONAL_DATA_COLUMN	    = 11;
	public static final int	     ORGANISM_NAME_ROW	        = 2;
	public static final int	     NUMBER_OF_NUCLEOTIDE_ROW	= 4;
	public static final int	     NUMBER_OF_SEQUENCE_ROW	    = 6;
	public static final int	     NUMBER_OF_INVALID_CDS_ROW	= 8;
	public static final int	     LAST_MODIFICATION_DATE_ROW	= 10;
	public static final int	     ACCESSION_ROW	            = 12;
	public static final int	     BIOPROJECT_ROW	            = 14;
	public static final int	     ORGANISM_TAXONOMY_ROW	    = 16;
	public static final int	     DEFINITION_ROW	            = 20;
	public static final int	     TITLE_ROW	                = 24;
	public static final int	     TOTAL_ROW	                = 66;
	public static final int[]	 ADDITIONAL_DATA_ROWS	    = { ORGANISM_NAME_ROW, NUMBER_OF_NUCLEOTIDE_ROW, NUMBER_OF_SEQUENCE_ROW,
	                NUMBER_OF_INVALID_CDS_ROW, LAST_MODIFICATION_DATE_ROW, ACCESSION_ROW, BIOPROJECT_ROW, ORGANISM_TAXONOMY_ROW, DEFINITION_ROW,
	                TITLE_ROW	                            };
	public static final String[]	ADDITIONAL_DATA	        = { ORGANISM_NAME, NUMBER_OF_NUCLEOTIDE, NUMBER_OF_SEQUENCE, NUMBER_OF_INVALID_CDS,
	                LAST_MODIFICATION_DATE, ACCESSION, BIOPROJECT, ORGANISM_TAXONOMY, DEFINITION, TITLE };
	public static final String[]	STATISTIC_HEADER	    = { "Phase 0", "Freq Phase 0", "Phase 1", "Freq Phase 1", "Phase 2", "Freq Phase 2",
	                "Pref Phase 0", "Pref Phase 1", "Pref Phase 2" };
	public static final String	 SUM_SHEET_NAME	            = "sum_sheet";

	/**
	 * Check if the sheet exists in the excel file pointed by the path
	 * 
	 * @param filePath : excel file path
	 * @param sheetName : the sheet name to check
	 * @return return <b>true</b> if it exist, <b>false</b> otherwise
	 */
	public static boolean isSheetExist(String filePath, String sheetName) {
		File file = new File(filePath);
		try {
			Workbook workbook = WorkbookFactory.create(file);
			return workbook.getSheet(sheetName) != null;
		} catch (Exception exception) {
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * Return the last modification date in the excel sheet
	 * 
	 * @param filePath : excel file path
	 * @param sheetName : the sheet name
	 * @return Return the last modification date. If an error occurs the default 01-JAN-1000 date is
	 *         return
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public static Date getLastModificationDate(String filePath, String sheetName) {
		OPCPackage opcPackage = null;
		Date defaultDate = new Date("01-JAN-1000");
		File file = null;
		try {
			file = new File(filePath);
		} catch (Exception exception1) {
			return defaultDate;
		}
		if (!file.isFile())
			return defaultDate;
		try {
			opcPackage = OPCPackage.open(file);
			XSSFWorkbook workbook = new XSSFWorkbook(opcPackage);
			XSSFSheet sheet = workbook.getSheet(sheetName);
			if (sheet == null)
				return defaultDate;
			XSSFRow row = sheet.getRow(LAST_MODIFICATION_DATE_ROW);
			Cell cell = row.getCell(ADDITIONAL_DATA_COLUMN + 1);
			defaultDate = new Date(cell.getStringCellValue());
			opcPackage.close();
		} catch (InvalidOperationException ex) {
			file.delete();
		} catch (Exception exception) {
			try {
				if (opcPackage != null) {
					opcPackage.close();
				}
			} catch (Exception e) {}
		}
		return defaultDate;
	}
}
