package fr.unistra.view.downloadHandler;

import static fr.unistra.utils.MainConstants.PRINT_LOGS;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Locale;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.unistra.view.browserHandler.BrowserPanel;
import fr.unistra.view.consoleHandler.ConsoleMessagePanel;
import fr.unistra.view.language.Messages;

/**
 * The main frame of the application. It gether all panels in the main windows
 * 
 * @author Abdoul-djawadou SALAOU
 */
public class DownloadFrame extends JFrame {
	private static final long	serialVersionUID	= 1L;
	private MenuPanel	      informationPanel;
	private JSplitPane	      consoleSplitPane;
	private DownloadPanel	  downloadPanel;
	private BrowserPanel	  browserPanel;
	private JPanel	          consolePanel;

	public DownloadFrame() {
		informationPanel = new MenuPanel();
		informationPanel.getDisplayBrowserButton().addItemListener(new DisplayBrowserAction());
		informationPanel.getDisplayConsoleButton().addItemListener(new DisplayConsoleAction());
		informationPanel.getLangButton().addActionListener(new LangAction());

		downloadPanel = new DownloadPanel();
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.add(informationPanel);
		mainPanel.add(downloadPanel);

		LineBorder lineBorder = new LineBorder(new Color(72, 209, 204), 4, false);
		TitledBorder border = new TitledBorder(lineBorder, "Console", TitledBorder.LEADING, TitledBorder.TOP, new Font("SansSerif", Font.BOLD, 21), Color.WHITE);
		JScrollPane consoleScrollPane = new JScrollPane(new ConsoleMessagePanel());
		consoleScrollPane.setBorder(new LineBorder(Color.DARK_GRAY));
		consolePanel = new JPanel(new BorderLayout());
		consolePanel.setBorder(new CompoundBorder(new EmptyBorder(5, 5, 3, 5), border));
		consolePanel.setBackground(Color.DARK_GRAY);
		consolePanel.add(consoleScrollPane, BorderLayout.CENTER);

		consoleSplitPane = new JSplitPane();
		consoleSplitPane.setOneTouchExpandable(true);
		consoleSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		consoleSplitPane.setLeftComponent(mainPanel);
		consoleSplitPane.setRightComponent(null);
		consoleSplitPane.setBackground(Color.DARK_GRAY);
		consoleSplitPane.setBorder(new LineBorder(Color.DARK_GRAY));
		consoleSplitPane.setDividerSize(0);
		consoleSplitPane.addPropertyChangeListener(new SplitPanPropertyChangeListener());
		getContentPane().add(consoleSplitPane, BorderLayout.CENTER);

		browserPanel = new BrowserPanel();
		getContentPane().add(browserPanel, BorderLayout.WEST);

		informationPanel.getDisplayBrowserButton().setSelected(true);
		informationPanel.getDisplayConsoleButton().doClick();
		setBackground(Color.DARK_GRAY);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowCloseAction());
		setMinimumSize(new Dimension(511, 441));
		setLocationRelativeTo(null);
		setTitle("GenBStats");
		try {
			URL iconURL = getClass().getResource("/image/genBStats.png");
			ImageIcon icon = new ImageIcon(iconURL);
			setIconImage(icon.getImage());
		} catch (Exception e) {}
		// pack();

	}

	/**
	 * Handles application exit action
	 */
	public void onExit() {
		if (downloadPanel.getDownloadManager() != null && downloadPanel.getDownloadManager().isTaskRunning()) {
			boolean choice = GeneralDialog.showQuestion(Messages.getString("quit.message"), Messages.getString("quit.title"), downloadPanel);
			if (!choice)
				return;
			System.exit(0);
		}
		else
			System.exit(0);
	}

	public void initDownloadManager() {
		downloadPanel.initDownloadManager();
	}

	class SplitPanPropertyChangeListener implements PropertyChangeListener {
		boolean	isSelected;

		@Override
		public void propertyChange(PropertyChangeEvent changeEvent) {
			JSplitPane sourceSplitPane = (JSplitPane) changeEvent.getSource();
			String propertyName = changeEvent.getPropertyName();
			isSelected = informationPanel.getDisplayConsoleButton().isSelected();
			if (isSelected && propertyName.equals(JSplitPane.LAST_DIVIDER_LOCATION_PROPERTY)) {
				if (sourceSplitPane.getRightComponent().getSize().height < 30) {
					PRINT_LOGS = false;
					informationPanel.getDisplayConsoleButton().setSelected(false);
				}
				else {
					PRINT_LOGS = true;
					informationPanel.getDisplayConsoleButton().setSelected(true);
				}

			}
		}
	}

	/**
	 * Console dispplay button action
	 */
	class DisplayConsoleAction implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent ev) {
			if (ev.getStateChange() == ItemEvent.SELECTED) {
				PRINT_LOGS = true;
				consoleSplitPane.setRightComponent(consolePanel);

				Dimension dimension = getSize();
				dimension.setSize(dimension.width, dimension.height + 150);
				setSize(dimension);
				revalidate();
				repaint();
			}
			else if (ev.getStateChange() == ItemEvent.DESELECTED) {
				PRINT_LOGS = false;
				consoleSplitPane.setRightComponent(null);
				pack();

			}
		}

	}

	/**
	 * Browser dispplay button action
	 */
	class DisplayBrowserAction implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent ev) {
			if (ev.getStateChange() == ItemEvent.SELECTED) {
				browserPanel.setVisible(true);
				pack();
			}
			else if (ev.getStateChange() == ItemEvent.DESELECTED) {
				browserPanel.setVisible(false);
				pack();
			}
		}

	}

	/**
	 * Language button action
	 */
	class LangAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (Locale.getDefault() == Locale.ENGLISH)
				Locale.setDefault(Locale.FRENCH);
			else
				Locale.setDefault(Locale.ENGLISH);

			Messages.resetRessource();
			downloadPanel.resetText();
			informationPanel.resetText();
			browserPanel.resetText();
		}

	}

	/**
	 * Handles main window closing
	 */
	class WindowCloseAction extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			onExit();
		}
	}
}
