/**
 *
 */
package fr.unistra.view.actionsHandler;

import static fr.unistra.utils.FileUtil.readAllContent;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JComponent;

import fr.unistra.view.downloadHandler.InformationDialog;

/**
 * Handles dialog actions showing
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class HelpAction implements ActionListener {
	private int	              type	     = 0;
	private static JComponent	parent;
	private static String	  fileEnding	= "en.html";

	public HelpAction(int type) {
		this.type = type;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		InputStream fileInputStream;
		switch (type) {
			case 1:
				fileInputStream = getClass().getResourceAsStream("/help/kingdomsHelp_" + fileEnding);
				new InformationDialog(readAllContent(fileInputStream), new Dimension(780, 320), parent).setVisible(true);
				break;
			case 2:
				fileInputStream = getClass().getResourceAsStream("/help/elementsHelp_" + fileEnding);
				new InformationDialog(readAllContent(fileInputStream), new Dimension(780, 385), parent).setVisible(true);
				break;
			case 3:
				fileInputStream = getClass().getResourceAsStream("/help/optionsHelp_" + fileEnding);
				new InformationDialog(readAllContent(fileInputStream), new Dimension(780, 410), parent).setVisible(true);
				break;
			case 4:
				fileInputStream = getClass().getResourceAsStream("/help/about_" + fileEnding);
				new InformationDialog(readAllContent(fileInputStream), new Dimension(780, 410), parent).setVisible(true);
				break;

			case 5:
				fileInputStream = getClass().getResourceAsStream("/help/menuHelp_" + fileEnding);
				new InformationDialog(readAllContent(fileInputStream), new Dimension(780, 410), parent).setVisible(true);
				break;

		}
	}

	public static void setParent(JComponent parentc) {
		parent = parentc;
	}

	public static void setFileEnding(String fileEndingc) {
		if (fileEndingc != null && fileEndingc.endsWith("html"))
			fileEnding = fileEndingc;
	}

	public static void openFile(File file) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if (desktop != null && desktop.isSupported(Desktop.Action.OPEN)) {
			try {
				desktop.open(file);
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
	}

	public static void openWebpage(URI uri) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(uri);
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
	}

	public static void openWebpage(URL url) {
		try {
			openWebpage(url.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

}
