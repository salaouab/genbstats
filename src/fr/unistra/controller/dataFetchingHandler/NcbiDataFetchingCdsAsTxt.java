package fr.unistra.controller.dataFetchingHandler;

import static fr.unistra.utils.FileUtil.getLastModificationDate;
import static fr.unistra.utils.MainConstants.*;

import java.io.IOException;
import java.util.Date;

import com.algosome.eutils.EntrezSearch;

import fr.unistra.controller.dataParser.*;
import fr.unistra.model.beans.Organism;
import fr.unistra.model.beans.OrganismData;
import fr.unistra.model.excelFileHandler.ExcelFileWritter;

/**
 * Fetches data from ncbi databases by organism name as input and writes them in excel file
 * @author Abdoul-Djawadou SALAOU
 */
public class NcbiDataFetchingCdsAsTxt extends NcbiDataFetching {
	private SequenceSaver	sequenceSaver;

	/**
	 * Initilize {@link EntrezSearch} , {@link ExcelFileWritter}, {@link OrganismInfosParser} ,
	 * {@link CodingSequenceParser} and the , {@link OrganismData}
	 */
	public NcbiDataFetchingCdsAsTxt() {
		super();
		sequenceSaver = new SequenceSaver(organismData);
		sequenceSaver.setDownloadCdsOnly(true);
		fetchDataFormat = CODING_SEQUENCE_IN_FASTA;
		organismInfosParser.setRootName("KingdomsCoDingSequence");
	}

	/**
	 * Constructor with organism named informed
	 * @param organismName : the organism name
	 * @see #NcbicDataFetching()
	 */
	public NcbiDataFetchingCdsAsTxt(String organismName) {
		this();
		setOrganismName(organismName);
	}

	/**
	 * Constructor with the {@link Organism} informed
	 * @param organism : the {@link Organism} information
	 */
	public NcbiDataFetchingCdsAsTxt(Organism organism) {
		this();
		setOrganism(organism);
	}

	@Override
	public void fetchAndSaveData(String genomeId) {
		// Get the organism cds.
		fetchCodingSequenceDataAsTxt();
		if (PRINT_LOGS)
			System.out.println("  Ecris avec Succes - " + genomeId + " - " + organism.getName() + " --> "
			        + organismData.getOrganismInfosMap().get(ORGANISM_SHEET_NAME));
	}

	/**
	 * Fetching coding sequence by retrieving data in fastat format
	 */
	protected void fetchCodingSequenceDataAsTxt() {
		int tryAgain = 0;
		boolean querySuccess = true;
		initFetchEngine(fetchDataFormat);

		do {
			try {
				entrezFetch.doQuery(sequenceSaver);
				querySuccess = true;
			} catch (IOException exception) {
				if (DEBUG)
					exception.printStackTrace();
				tryAgain++;
				querySuccess = false;
			}
		} while (!querySuccess && tryAgain < MAX_TRIAL);
	}

	@Override
	protected void setFileExtention() {
		String path = organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH);
		String sheetName = organismData.getOrganismInfosMap().get(ORGANISM_SHEET_NAME);
		path += "_" + sheetName + TEXT_FILE_EXTENSION;
		organismData.getOrganismInfosMap().put(ORGANISM_FILE_PATH, path);
	}

	/**
	 * Check if the file being download exists on the disc and has been updated
	 * @return Return <b>true</b> if the file being downloaded exists and an update uccurred,
	 *         <b>false</b> otherwise
	 */
	@Override
	@SuppressWarnings("deprecation")
	protected boolean fileExistsAndIsNewer() {
		String oldFilePath = organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH);
		// Date stored in file
		Date oldDate = getLastModificationDate(oldFilePath);
		// Date newly Fetch
		try {
			Date newDate = new Date(organismData.getOrganismInfosMap().get(LAST_MODIFICATION_DATE));
			// check if an update occurred
			return newDate.after(oldDate);
		} catch (Exception e) {}
		return false;
	}

	public static void main(String[] args) {
		// Création de l'organisme
		Organism organism = new Organism("Oreochromis niloticus", "PRJNA72943");
		organism.addId("NC_022219.1");
		organism.setGroup("Animals");
		organism.setSubGroup("Fishes");
		NcbiDataFetchingCdsAsTxt dataFetchingEngine = new NcbiDataFetchingCdsAsTxt(organism);
		// Téléchargement des donnéds
		dataFetchingEngine.fecthData();
	}

}
