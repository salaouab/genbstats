package fr.unistra.view.downloadHandler;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.unistra.controller.downloadManagement.DownloadManager;
import fr.unistra.controller.organismParser.OrganismParser;
import fr.unistra.model.beans.DownloadParameters;
import fr.unistra.model.beans.DownloadParameters.KindOfTask;
import fr.unistra.model.excelFileHandler.ComputeSum;
import fr.unistra.model.interfaces.DownloadEndingProcessor;
import fr.unistra.utils.FileUtil;
import fr.unistra.view.actionsHandler.HelpAction;
import fr.unistra.view.language.Messages;
import fr.unistra.view.progressHandler.ProgressPanel;

/**
 * Main Panel for download parameter management
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class DownloadPanel extends JPanel {
	private static final long	serialVersionUID	    = 1L;
	private final ButtonGroup	buttonGroup	            = new ButtonGroup();
	private TitledBorder	   parameterBorder, kingdomBorder, operationBorder, optionBorder, downloadBorder;
	private JButton	           kingdomInfoButton;
	private JCheckBox	       eukaryotaCheckbox, prokaryotaCheckbox, virusCheckbox;
	private JRadioButton	   cdsRbutton, wholeSequenceRButton, computeStatsRButton;
	private JButton	           operationInfoButton, optInfoButton;
	private JCheckBox	       hSumCheckBox, extendSearchCheckBox;
	private JButton	           startButton, stopButton;
	private int	               numberOfOrganismSelected	= 0;
	private DownloadParameters	downloadParameters;
	private DownloadManager	   downloadManager;
	private ProgressPanel	   progressPanel;
	private EndingProcessing	endingProcessor;

	public static Font	       componentFont	        = new Font("SansSerif", Font.BOLD, 12);
	public static Color	       foregroundColor	        = Color.WHITE;
	public static Color	       backgroundColor	        = Color.DARK_GRAY;

	public DownloadPanel() {
		setSize(new Dimension(805, 417));
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		setBackground(backgroundColor);

		Font font = new Font("SansSerif", Font.BOLD, 15);
		Color titleColor = new Color(255, 255, 255);
		Color borderColor = new Color(72, 209, 204);
		LineBorder lineBorder = new LineBorder(borderColor, 4, true);

		final JPanel contentPanel = new JPanel();
		parameterBorder = new TitledBorder(lineBorder, Messages.getString("parameterBorder"), TitledBorder.LEADING, TitledBorder.TOP, new Font("SansSerif", Font.BOLD, 21), titleColor);
		contentPanel.setBorder(parameterBorder);
		contentPanel.setOpaque(false);
		add(contentPanel);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] { 558, 0 };
		gbl_contentPanel.rowHeights = new int[] { 74, 63, 38, 29, 0 };
		gbl_contentPanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPanel.setLayout(gbl_contentPanel);

		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setBackground(backgroundColor);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel kingdomsPanel = new JPanel();
		kingdomsPanel.setOpaque(false);
		kingdomBorder = new TitledBorder(lineBorder, Messages.getString("kingdomsBorder"), TitledBorder.LEADING, TitledBorder.TOP, font, titleColor);
		kingdomsPanel.setBorder(kingdomBorder);
		panel.add(kingdomsPanel);

		kingdomInfoButton = new JButton(Messages.getString("kingdomInfoButton"));
		kingdomInfoButton.setIcon(new ImageIcon(DownloadPanel.class.getResource("/image/help.png")));
		kingdomInfoButton.setFont(componentFont);
		kingdomInfoButton.addActionListener(new HelpAction(1));
		kingdomsPanel.add(kingdomInfoButton);

		eukaryotaCheckbox = new JCheckBox(Messages.getString("eukaryotaCheckbox")); //$NON-NLS-1$
		initComponentProperties(eukaryotaCheckbox);
		eukaryotaCheckbox.setSelected(true);
		eukaryotaCheckbox.addActionListener(new OptionAction(1));
		kingdomsPanel.add(eukaryotaCheckbox);

		prokaryotaCheckbox = new JCheckBox(Messages.getString("prokaryotaCheckbox")); //$NON-NLS-1$
		initComponentProperties(prokaryotaCheckbox);
		prokaryotaCheckbox.setSelected(true);
		prokaryotaCheckbox.addActionListener(new OptionAction(2));
		kingdomsPanel.add(prokaryotaCheckbox);

		virusCheckbox = new JCheckBox(Messages.getString("virusCheckbox")); //$NON-NLS-1$
		initComponentProperties(virusCheckbox);
		virusCheckbox.setSelected(true);
		virusCheckbox.addActionListener(new OptionAction(3));
		kingdomsPanel.add(virusCheckbox);

		JPanel elementDownloadPanel = new JPanel();
		elementDownloadPanel.setOpaque(false);
		operationBorder = new TitledBorder(lineBorder, Messages.getString("operationBorder"), TitledBorder.LEADING, TitledBorder.TOP, font, titleColor);
		elementDownloadPanel.setBorder(operationBorder);
		panel.add(elementDownloadPanel);

		computeStatsRButton = new JRadioButton(Messages.getString("computeStatsRButton")); //$NON-NLS-1$
		initComponentProperties(computeStatsRButton);
		computeStatsRButton.setSelected(true);
		buttonGroup.add(computeStatsRButton);
		elementDownloadPanel.add(computeStatsRButton);
		computeStatsRButton.addActionListener(new TaskAction(KindOfTask.COMPUTE_STATS));

		wholeSequenceRButton = new JRadioButton(Messages.getString("wholeSequenceRButton"));
		initComponentProperties(wholeSequenceRButton);
		buttonGroup.add(wholeSequenceRButton);
		elementDownloadPanel.add(wholeSequenceRButton);
		wholeSequenceRButton.addActionListener(new TaskAction(KindOfTask.GET_WHOLE_SEQUENCE));

		cdsRbutton = new JRadioButton(Messages.getString("cdsRbutton")); //$NON-NLS-1$
		initComponentProperties(cdsRbutton);
		buttonGroup.add(cdsRbutton);
		elementDownloadPanel.add(cdsRbutton);
		cdsRbutton.addActionListener(new TaskAction(KindOfTask.GET_CDS_SEQUENCE));

		operationInfoButton = new JButton(Messages.getString("operationInfoButton"));
		operationInfoButton.setIcon(new ImageIcon(DownloadPanel.class.getResource("/image/help.png")));
		operationInfoButton.setFont(componentFont);
		operationInfoButton.addActionListener(new HelpAction(2));
		elementDownloadPanel.add(operationInfoButton);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPanel.add(panel, gbc_panel);

		final JPanel optionPanel = new JPanel();
		final FlowLayout fl_optionPanel = (FlowLayout) optionPanel.getLayout();
		fl_optionPanel.setHgap(8);
		optionPanel.setOpaque(false);
		optionBorder = new TitledBorder(lineBorder, Messages.getString("optionsBorder"), TitledBorder.LEADING, TitledBorder.TOP, font, titleColor);
		optionPanel.setBorder(optionBorder);
		GridBagConstraints gbc_optionPanel = new GridBagConstraints();
		gbc_optionPanel.insets = new Insets(0, 0, 5, 0);
		gbc_optionPanel.gridx = 0;
		gbc_optionPanel.gridy = 1;
		contentPanel.add(optionPanel, gbc_optionPanel);

		optInfoButton = new JButton(Messages.getString("optInfoButton")); //$NON-NLS-1$
		optInfoButton.setIcon(new ImageIcon(DownloadPanel.class.getResource("/image/help.png")));
		optInfoButton.setFont(componentFont);
		optionPanel.add(optInfoButton);
		optInfoButton.addActionListener(new HelpAction(3));

		hSumCheckBox = new JCheckBox(Messages.getString("hSumCheckBox")); //$NON-NLS-1$
		initComponentProperties(hSumCheckBox);
		hSumCheckBox.setSelected(true);
		hSumCheckBox.addActionListener(new OptionAction(4));
		optionPanel.add(hSumCheckBox);

		extendSearchCheckBox = new JCheckBox(Messages.getString("extendSearchCheckBox")); //$NON-NLS-1$
		initComponentProperties(extendSearchCheckBox);
		extendSearchCheckBox.addActionListener(new OptionAction(5));
		optionPanel.add(extendSearchCheckBox);

		final JPanel downloadPanel = new JPanel();
		downloadBorder = new TitledBorder(lineBorder, Messages.getString("downloadBorder"), TitledBorder.LEADING, TitledBorder.TOP, font, titleColor);
		downloadPanel.setBorder(downloadBorder);
		downloadPanel.setOpaque(false);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.VERTICAL;
		gbc_panel_1.insets = new Insets(15, 0, 5, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 2;
		contentPanel.add(downloadPanel, gbc_panel_1);
		downloadPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5));

		startButton = new JButton(Messages.getString("startButton")); //$NON-NLS-1$
		startButton.setIcon(new ImageIcon(DownloadPanel.class.getResource("/image/download.png")));
		startButton.setFont(componentFont);
		startButton.addActionListener(new StartAction());
		downloadPanel.add(startButton);

		stopButton = new JButton(Messages.getString("stopButton")); //$NON-NLS-1$
		stopButton.setIcon(new ImageIcon(DownloadPanel.class.getResource("/image/stop_download.png")));
		stopButton.setFont(componentFont);
		stopButton.addActionListener(new StopAction());
		downloadPanel.add(stopButton);

		progressPanel = new ProgressPanel();
		GridBagConstraints gbc_progressPanel = new GridBagConstraints();
		gbc_progressPanel.gridx = 0;
		gbc_progressPanel.gridy = 3;
		contentPanel.add(progressPanel, gbc_progressPanel);

		endingProcessor = new EndingProcessing();
		progressPanel.setTaskEndProcessor(endingProcessor);
		progressPanel.setLabelSuffix(Messages.getString("organismLabelSuffix"));
		HelpAction.setFileEnding(Messages.getString("helpFileEnding"));
		HelpAction.setParent(this);
		downloadManager = new DownloadManager();
		downloadParameters = downloadManager.getDownloadParameters();
		resetText();
	}

	/**
	 * Set default background, forground and font to a component
	 * 
	 * @param component A component to initialise
	 */
	public static void initComponentProperties(JComponent component) {
		component.setForeground(foregroundColor);
		component.setBackground(backgroundColor);
		component.setFont(componentFont);
	}

	public void setDownloadManager(DownloadManager downloadManager) {
		this.downloadManager = downloadManager;
		downloadParameters = downloadManager.getDownloadParameters();
		initDownloadManager();
	}

	/**
	 * Initialiase the download engine by loading kingdoms
	 */
	public void initDownloadManager() {
		if (downloadManager.initKingdoms())
			System.out.println("Success");
		// else
		// System.out.println("Probleme lors du chargement des organismes");
		numberOfOrganismSelected = downloadManager.getTotalOrganisms();
		progressPanel.updateInfoLabel(numberOfOrganismSelected);
		downloadManager.setProgressObserver(progressPanel.new TaskProgressObserver());
	}

	/**
	 * Reset text for components
	 */
	public void resetText() {
		parameterBorder.setTitle(Messages.getString("parameterBorder"));
		kingdomBorder.setTitle(Messages.getString("kingdomsBorder"));
		kingdomInfoButton.setText(Messages.getString("kingdomInfoButton"));
		eukaryotaCheckbox.setText(Messages.getString("eukaryotaCheckbox"));
		prokaryotaCheckbox.setText(Messages.getString("prokaryotaCheckbox"));
		virusCheckbox.setText(Messages.getString("virusCheckbox"));
		operationBorder.setTitle(Messages.getString("operationBorder"));
		operationInfoButton.setText(Messages.getString("operationInfoButton"));
		computeStatsRButton.setText(Messages.getString("computeStatsRButton"));
		wholeSequenceRButton.setText(Messages.getString("wholeSequenceRButton"));
		cdsRbutton.setText(Messages.getString("cdsRbutton"));
		optionBorder.setTitle(Messages.getString("optionsBorder"));
		optInfoButton.setText(Messages.getString("optInfoButton"));
		hSumCheckBox.setText(Messages.getString("hSumCheckBox"));
		extendSearchCheckBox.setText(Messages.getString("extendSearchCheckBox"));
		downloadBorder.setTitle(Messages.getString("downloadBorder"));
		startButton.setText(Messages.getString("startButton"));
		stopButton.setText(Messages.getString("stopButton"));
		progressPanel.setLabelSuffix(Messages.getString("organismLabelSuffix"));
		HelpAction.setFileEnding(Messages.getString("helpFileEnding"));
		progressPanel.updateInfoLabel(numberOfOrganismSelected);

	}

	public DownloadManager getDownloadManager() {
		return downloadManager;
	}

	/**
	 * Toggle component state (enable/disable)
	 * 
	 * @param state True to enable, false to disable
	 */
	private void toggleComponentEnabledStateTo(boolean state) {
		Color newFgColor;
		if (state)
			newFgColor = foregroundColor;
		else
			newFgColor = Color.BLACK;
		eukaryotaCheckbox.setEnabled(state);
		prokaryotaCheckbox.setEnabled(state);
		virusCheckbox.setEnabled(state);
		computeStatsRButton.setEnabled(state);
		wholeSequenceRButton.setEnabled(state);
		cdsRbutton.setEnabled(state);
		hSumCheckBox.setEnabled(state);
		extendSearchCheckBox.setEnabled(state);

		eukaryotaCheckbox.setForeground(newFgColor);
		prokaryotaCheckbox.setForeground(newFgColor);
		virusCheckbox.setForeground(newFgColor);
		computeStatsRButton.setForeground(newFgColor);
		wholeSequenceRButton.setForeground(newFgColor);
		cdsRbutton.setForeground(newFgColor);
		hSumCheckBox.setForeground(newFgColor);
		extendSearchCheckBox.setForeground(newFgColor);
	}

	/** Handles elements to fetch choice */
	class TaskAction implements ActionListener {
		KindOfTask	kindOfTask;

		public TaskAction(KindOfTask kindOfTask) {
			this.kindOfTask = kindOfTask;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (downloadParameters != null)
				downloadParameters.setWorkToDo(kindOfTask);
			hSumCheckBox.setEnabled(computeStatsRButton.isSelected());
		}
	}

	/** Handles Options values */
	class OptionAction implements ActionListener {
		int		type	= 0;
		boolean	isSelected;

		public OptionAction(int type) {
			this.type = type;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (downloadParameters != null)
				switch (type) {
					case 1:
						isSelected = eukaryotaCheckbox.isSelected();
						downloadParameters.setEukaryotaSelected(isSelected);
						if (downloadManager != null) {
							int nbEukaryotes = downloadManager.getNumberOfEukaryotes();
							if (isSelected)
								numberOfOrganismSelected += nbEukaryotes;
							else
								numberOfOrganismSelected -= nbEukaryotes;
						}
						break;
					case 2:
						isSelected = prokaryotaCheckbox.isSelected();
						downloadParameters.setProkaryoteSelected(isSelected);
						if (downloadManager != null) {
							int nbProkaryotes = downloadManager.getNumberOfProkaryotes();
							if (isSelected)
								numberOfOrganismSelected += nbProkaryotes;
							else
								numberOfOrganismSelected -= nbProkaryotes;
						}
						break;
					case 3:
						isSelected = virusCheckbox.isSelected();
						downloadParameters.setViruesSelected(isSelected);
						if (downloadManager != null) {
							int nbVirus = downloadManager.getNumberOfViruses();
							if (isSelected)
								numberOfOrganismSelected += nbVirus;
							else
								numberOfOrganismSelected -= nbVirus;
						}
						break;
					case 4:
						downloadParameters.setHSumSelected(hSumCheckBox.isSelected());
						break;
					case 5:
						downloadParameters.setHSumSelected(extendSearchCheckBox.isSelected());
						break;
				}
			progressPanel.updateInfoLabel(numberOfOrganismSelected);
		}
	}

	/** Download start action handler */
	class StartAction implements ActionListener {
		boolean	isParametersSuccesfullyHandeled;

		@Override
		public void actionPerformed(ActionEvent e) {
			if (downloadManager == null)
				return;

			isParametersSuccesfullyHandeled = downloadManager.processDownloadParameters();

			if (!isParametersSuccesfullyHandeled) {
				if (downloadManager.isConnectionTrouble())
					GeneralDialog.showInformation(Messages.getString("connection.message"), Messages.getString("connection.title"), DownloadPanel.this);
				else if (!OrganismParser.NCBI_ERROR_MESSAGE.isEmpty()) {
					String genBankMessage = OrganismParser.NCBI_ERROR_MESSAGE;
					String title = " NCBI SITE MESSAGE ";
					Component parent = getParent();
					if (parent.getParent() != null)
						parent = parent.getParent();
					if (parent.getParent() != null)
						parent = parent.getParent();

					new InformationDialog(genBankMessage, title, new Dimension(900, 600), parent).setVisible(true);
				}
				else
					GeneralDialog.showInformation("Please contact the author with this issue screen shot", "Unknown error", DownloadPanel.this);

				return;
			}

			numberOfOrganismSelected = downloadManager.getTaskNumber();
			if (numberOfOrganismSelected == 0) {
				GeneralDialog.showInformation(Messages.getString("noKingdom.message"), Messages.getString("noKingdom.title"), DownloadPanel.this);
				return;
			}
			if (GeneralDialog.showQuestion(Messages.getString("resume.message"), Messages.getString("resume.title"), DownloadPanel.this)) {
				int cursorPosition = GeneralDialog.showNumericInput(Messages.getString("input.title") + numberOfOrganismSelected, DownloadPanel.this);

				while (cursorPosition < 0 || cursorPosition >= numberOfOrganismSelected) {
					GeneralDialog.showInformation(Messages.getString("invalidInput.message"), Messages.getString("invalidInput.title"), DownloadPanel.this);
					cursorPosition = GeneralDialog.showNumericInput(Messages.getString("input.title") + numberOfOrganismSelected, DownloadPanel.this);
				}
				downloadManager.setCursorPosition(cursorPosition);
			}
			else
				downloadManager.setCursorPosition(0);

			stopButton.setEnabled(true);
			startButton.setEnabled(false);
			toggleComponentEnabledStateTo(false);
			downloadManager.startDownload();

		}
	}

	/** Download stop action handler */
	class StopAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (downloadManager != null && downloadManager.isTaskRunning()) {
				boolean choice = GeneralDialog.showQuestion(Messages.getString("abort.message"), Messages.getString("abort.title"), DownloadPanel.this);
				if (!choice)
					return;
				if (downloadManager != null)
					downloadManager.stopDownload();
			}
			stopButton.setEnabled(false);
			startButton.setEnabled(true);
			endingProcessor.stopEndProcessing();
			progressPanel.resetProgressBar();
			progressPanel.updateInfoLabel(numberOfOrganismSelected);
			toggleComponentEnabledStateTo(true);
		}
	}

	/** Process to handle downloading ending actions */
	class EndingProcessing implements DownloadEndingProcessor {
		SwingWorker<Void, Void>	endProcessor;

		@Override
		public void process() {
			if (endProcessor != null && !endProcessor.isDone())
				return;

			endProcessor = new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					while (!downloadManager.isTaskFinished() && !isCancelled()) {
						try {
							// Sleep 5s
							Thread.sleep(5000);
						} catch (Exception e) {}
					}
					if (downloadParameters.isHierarchicalSumEnabled())
						new ComputeSum(FileUtil.ORGANISM_BASE_DIRECTORY).compute();

					progressPanel.stopProgress();
					GeneralDialog.showInformation(Messages.getString("taskCompleted.message"), Messages.getString("taskCompleted.title"), DownloadPanel.this);
					return null;
				}

			};
			endProcessor.execute();
		}

		public void stopEndProcessing() {
			if (endProcessor != null)
				endProcessor.cancel(true);
		}

	}

}
