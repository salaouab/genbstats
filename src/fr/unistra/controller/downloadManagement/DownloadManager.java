package fr.unistra.controller.downloadManagement;

import static fr.unistra.utils.MainConstants.INTERUPTOR_FLAG;
import static fr.unistra.utils.MainConstants.PRINT_LOGS;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import fr.unistra.controller.dataFetchingHandler.NcbiDataFetchingCdsAsTxt;
import fr.unistra.controller.dataFetchingHandler.NcbiDataFetchingCdsForStats;
import fr.unistra.controller.dataFetchingHandler.NcbiDataFetchingWholeSequenceAsTxt;
import fr.unistra.controller.dataFetchingHandler.NcbiDataFetchingWorker;
import fr.unistra.controller.organismParser.EukaryoteOrganismParser;
import fr.unistra.controller.organismParser.OrganismParser;
import fr.unistra.controller.organismParser.ProkaryoteOrganismParser;
import fr.unistra.controller.organismParser.VirusOrganismParser;
import fr.unistra.model.beans.DownloadParameters;
import fr.unistra.model.beans.Organism;
import fr.unistra.model.beans.OrganismList;
import fr.unistra.model.interfaces.ProgressObserver;

/**
 * Download manager for stats computing (excel file), cds or the whole genome data fetching as text
 * 
 * @author Abdoul-djawadou SALAOU
 */
public class DownloadManager {

	private DownloadParameters	downloadParameters;
	private ArrayList<Organism>	eukaryotes;
	private ArrayList<Organism>	prokaryotes;
	private ArrayList<Organism>	viruses;
	private OrganismList	    organismToDownload;
	private ExecutorService	    executorService;
	private int	                numberOfEukaryotes	     = 0;
	private int	                numberOfProkaryotes	     = 0;
	private int	                numberOfViruses	         = 0;
	private ProgressObserver	progressObserver;
	private boolean	            isConnectionTrouble	     = true;
	/**
	 * Number of organism handle by each thread
	 */
	private final static int	NUMBER_OF_TASK_BY_THREAD	= 5;
	/**
	 * Help to resum a download from some postion instead of starting from begining agin
	 */
	private int	                CURSOR_POSITION	         = 0;

	public DownloadManager() {
		organismToDownload = new OrganismList();
		downloadParameters = new DownloadParameters();
	}

	/**
	 * Process download parameters by adding kingdoms selected in the download list
	 * 
	 * @return Returns true if kingdoms are well loaded and parametter processing goes fine,
	 *         otherwise false
	 * @see #initKingdoms()
	 */
	public boolean processDownloadParameters() {
		if (!initKingdoms()) {
			isConnectionTrouble = !OrganismParser.NCBI_ERROR_TRIGGERED;
			System.out.println("Trouble during organisms list loading!");
			return false;

		}
		else if (!isConnectionAvailable()) {
			isConnectionTrouble = true;
			return false;
		}

		organismToDownload.clear();

		if (downloadParameters.isViruesSelected())
			organismToDownload.add(viruses);

		if (downloadParameters.isEukaryotaSelected())
			organismToDownload.add(eukaryotes);

		if (downloadParameters.isProkaryoteSelected())
			organismToDownload.add(prokaryotes);

		return true;
	}

	/**
	 * Load kingdoms organisms data (name, bioproject,...) list
	 * 
	 * @return Returns true if all kingdoms are well loaded , false otherwise
	 */
	public boolean initKingdoms() {
		ArrayList<Organism> tmpList;
		if (eukaryotes == null) {
			tmpList = new EukaryoteOrganismParser().loadOrganismsByURL();
			if (tmpList.isEmpty())
				return false;

			eukaryotes = tmpList;
			numberOfEukaryotes = tmpList.size();
		}

		if (prokaryotes == null) {
			tmpList = new ProkaryoteOrganismParser().loadOrganismsByURL();
			if (tmpList.isEmpty())
				return false;

			prokaryotes = tmpList;
			numberOfProkaryotes = tmpList.size();
		}
		if (viruses == null) {
			tmpList = new VirusOrganismParser().loadOrganismsByURL();
			if (tmpList.isEmpty())
				return false;
			viruses = tmpList;
			numberOfViruses = tmpList.size();
		}
		if (PRINT_LOGS)
			System.out.println("\t==> Kingdoms loaded succesfully!");
		return true;

	}

	/**
	 * Starts data downloading based on parameters value
	 */
	public void startDownload() {
		INTERUPTOR_FLAG = false;
		int nbThread = Runtime.getRuntime().availableProcessors() + 1;
		if (PRINT_LOGS)
			System.out.println("Number of Thread:  " + nbThread);
		executorService = Executors.newFixedThreadPool(nbThread);
		// organismToDownload.shuffle();
		organismToDownload.setCursorPosition(CURSOR_POSITION);
		int numberOfThreadWorker = 1 + (organismToDownload.getNumberOfOrganism() - CURSOR_POSITION) / NUMBER_OF_TASK_BY_THREAD;

		switch (downloadParameters.getWorkToDo()) {
			case COMPUTE_STATS:
				for (int i = 0; i < numberOfThreadWorker; i++)
					executorService.execute(new NcbiDataFetchingWorker(new NcbiDataFetchingCdsForStats(), organismToDownload, NUMBER_OF_TASK_BY_THREAD));
				break;
			case GET_CDS_SEQUENCE:
				for (int i = 0; i < numberOfThreadWorker; i++)
					executorService.execute(new NcbiDataFetchingWorker(new NcbiDataFetchingCdsAsTxt(), organismToDownload, NUMBER_OF_TASK_BY_THREAD));
				break;
			case GET_WHOLE_SEQUENCE:
				for (int i = 0; i < numberOfThreadWorker; i++)
					executorService.execute(new NcbiDataFetchingWorker(new NcbiDataFetchingWholeSequenceAsTxt(), organismToDownload, NUMBER_OF_TASK_BY_THREAD));
				break;
		}
		executorService.shutdown();
	}

	public void stopDownload() {
		INTERUPTOR_FLAG = true;
		if (executorService == null)
			return;
		executorService.shutdownNow();
		try {
			executorService.awaitTermination(3, TimeUnit.SECONDS);
		} catch (InterruptedException e) {}
	}

	public boolean isTaskFinished() {
		return executorService == null || executorService.isTerminated();
	}

	public boolean isTaskRunning() {
		return !isTaskFinished();
	}

	public ProgressObserver getProgressObserver() {
		return progressObserver;
	}

	public void setProgressObserver(ProgressObserver progressObserver) {
		this.progressObserver = progressObserver;
		organismToDownload.setProgressObserver(progressObserver);
	}

	public int getNumberOfEukaryotes() {
		return numberOfEukaryotes;
	}

	public int getNumberOfProkaryotes() {
		return numberOfProkaryotes;
	}

	public int getNumberOfViruses() {
		return numberOfViruses;
	}

	public int getTotalOrganisms() {
		return numberOfEukaryotes + numberOfProkaryotes + numberOfViruses;
	}

	public int getTaskNumber() {
		return organismToDownload.getNumberOfOrganism();
	}

	public DownloadParameters getDownloadParameters() {
		return downloadParameters;
	}

	public void setCursorPosition(int cursorPosition) {
		CURSOR_POSITION = cursorPosition;
	}

	public boolean isConnectionTrouble() {
		return isConnectionTrouble;
	}

	/**
	 * Try to detect wether the internet connection is available by connecting with two servers
	 * 
	 * @return Returns true if the internet connection is available, false otherwise
	 */
	public static boolean isConnectionAvailable() {
		return isURLReachable("https://www.google.com") && isURLReachable("http://www.ncbi.nlm.nih.gov/genomes/Genome2BE/genome2srv.cgi");
	}

	/**
	 * Check weather it is possible to reach the url adress
	 * 
	 * @param url The url to connet with
	 * @return Returns true if it connects with the url, false otherwise
	 */
	static public boolean isURLReachable(String url) {
		URL entrez;
		URLConnection conn;
		int numberOfTrial = 0;
		boolean tryAgain = false;
		do
			try {
				entrez = new URL(url);
				conn = entrez.openConnection();
				conn.setConnectTimeout(60000);// 60 s for timeout
				conn.connect();
				return true;
			} catch (Exception exception) {
				tryAgain = true;
				numberOfTrial++;
			}
		while (tryAgain && numberOfTrial < 3);

		return false;
	}
}
