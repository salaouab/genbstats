package fr.unistra.view.language;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Handle application string data for internationalisation
 * @author Abdoul-djawadou SALAOU
 */
public class Messages {
	private static final String	  BUNDLE_NAME	    = "fr.unistra.view.language.messages";	 //$NON-NLS-1$

	private static ResourceBundle	RESOURCE_BUNDLE	= ResourceBundle.getBundle(BUNDLE_NAME);

	private Messages() {}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}

	}

	public static void resetRessource() {
		RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	}
}
