package fr.unistra.controller.dataFetchingHandler;

import static fr.unistra.utils.MainConstants.*;

import java.io.IOException;
import java.util.ArrayList;

import com.algosome.eutils.*;

import fr.unistra.controller.dataParser.*;
import fr.unistra.model.beans.Organism;
import fr.unistra.model.beans.OrganismData;
import fr.unistra.model.excelFileHandler.ExcelFileWritter;
import fr.unistra.utils.MainConstants;

/**
 * Fetches data from ncbi databases by organism name as input and writes them in excel file
 * @author Abdoul-Djawadou SALAOU
 */
public class NcbiDataFetching {
	public static final String	  CODING_SEQUENCE_IN_FASTA	= "fasta_cds_na";
	public static final String	  GENBANK_FULL	           = "gbwithparts";
	public static final String	  GENEPEPT_REPORT	       = "gp";
	public static final String	  AVOID_PARTIAL_CDS	       = " NOT partial[Title]";
	public static final String	  AVOID_PREDICTED_TERM	   = " NOT predicted[Title]";
	public static final String	  AVOID_SHOTGUN_TERM	   = " NOT shotgun[Title]";
	public static final String	  AVOID_SCAFFOLD_TERM	   = " NOT scaffold[Title]";
	public static final String	  SEQUENCE_REFERENCE	   = " AND refseq[Filter]";
	public static final String	  COMPLETE_CODING_SEQUENCE	= " AND complete ";
	public static final String	  CODING_SEQUENCE	       = " AND cds[Feature Key]";
	public static final String	  AVOID_TERM	           = AVOID_PARTIAL_CDS + AVOID_PREDICTED_TERM + AVOID_SHOTGUN_TERM + AVOID_SCAFFOLD_TERM;
	public static final String	  QUERY_BASE	           = CODING_SEQUENCE + SEQUENCE_REFERENCE;
	public static final int	      MAX_TRIAL	               = 3;
	public static final int	      MAX_RETRIEVAL	           = 500;
	protected static int	      TIME_OUT_SECOND	       = 30;
	protected Organism	          organism;
	protected EntrezSearch	      searchGenomeIDs;
	protected EntrezSearch	      searchData;
	protected EntrezFetch	      entrezFetch;
	protected OrganismData	      organismData;
	protected OrganismInfosParser	organismInfosParser;
	protected ESearchIdParser	  eSearchIdParser;
	public static boolean	      useExpandSearch	       = false;
	protected String	          fileExtention	           = EXCEL_FILE_EXTENSION;
	protected String	          fetchDataFormat	       = CODING_SEQUENCE_IN_FASTA;

	/**
	 * Initilize {@link EntrezSearch} , {@link ExcelFileWritter}, {@link OrganismInfosParser} ,
	 * {@link CodingSequenceParser} and the , {@link OrganismData}
	 */
	public NcbiDataFetching() {
		organism = new Organism();
		organismData = new OrganismData();
		eSearchIdParser = new ESearchIdParser();
		organismInfosParser = new OrganismInfosParser(organismData);

		searchGenomeIDs = new EntrezSearch();
		initEntrezEgine(searchGenomeIDs);
		searchData = new EntrezSearch();
		initEntrezEgine(searchGenomeIDs);
	}

	/**
	 * Constructor with organism named informed
	 * @param organismName : the organism name
	 * @see #NcbicDataFetching()
	 */
	public NcbiDataFetching(String organismName) {
		this();
		setOrganismName(organismName);
	}

	/**
	 * Constructor with the {@link Organism} informed
	 * @param organism : the {@link Organism} information
	 */
	public NcbiDataFetching(Organism organism) {
		this();
		setOrganism(organism);
	}

	/**
	 * make query based on {@link Organism} information, fetch each of its data and writes it in
	 * excel file. Data that are not updated are escaped.
	 */
	public void fecthData() {
		ArrayList<String> genomeStructureIds;
		int returnStart = 0;
		int alreatdyTried = 0;
		boolean finished = false;

		// Wheather to use replicon field or not
		if (useExpandSearch) {

			String query = getQuery();
			if (query.isEmpty())
				return;
			searchGenomeIDs.setTerm(query);
		}
		do
			try {
				genomeStructureIds = getIds(returnStart);
				returnStart += MAX_RETRIEVAL;
				for (String genomeId : genomeStructureIds) {
					fetchGenomeData(genomeId);
					// flag to break downloading
					if (INTERUPTOR_FLAG)
						break;
				}

				if (INTERUPTOR_FLAG || genomeStructureIds.size() < MAX_RETRIEVAL)
					finished = true;
			} catch (Exception e) {
				if (DEBUG)
					e.printStackTrace();
				alreatdyTried++;
			}
		while (!finished && (alreatdyTried < MAX_TRIAL));
	}

	/**
	 * Build query for organism Ids retrieval.
	 * @return Return Ids retrieval query
	 */
	protected String getQuery() {
		String query = "";

		if (organism.getName().isEmpty())
			return query;
		query = organism.getName() + "[Organism]";

		if (!organism.getBioProject().isEmpty())
			query += " AND " + organism.getBioProject() + "[BioProject]";

		query += QUERY_BASE + AVOID_TERM;
		return query;
	}

	/**
	 * Fetch organism data Ids from index returnStart to returnStart + {@value #MAX_RETRIEVAL}
	 * @param returnStart : data fetching first index
	 * @return Return string list containing organism data ids
	 */
	protected ArrayList<String> getIds(int returnStart) {

		/*
		 * Use Replicons/Chr/plasmid/organelle field of the organism
		 */
		if (!useExpandSearch)
			if (!organism.isIdListEmpty())
				return organism.getIdList();

		/*
		 * Retrieve Ids list by performing an expanded search with the organism name and its
		 * associated bioProject. This way rietrieve lot of id even those classified scaffold and
		 * WSG
		 */
		int tryAgain = 0;
		boolean querySuccess = true;
		searchGenomeIDs.setRetStart(returnStart);

		do {
			try {
				searchGenomeIDs.doQuery(eSearchIdParser);
				querySuccess = true;

			} catch (IOException exception) {
				if (DEBUG)
					exception.printStackTrace();
				tryAgain++;
				querySuccess = false;
			}
		} while (!querySuccess && tryAgain < MAX_TRIAL);

		return eSearchIdParser.getIdList();
	}

	/**
	 * Fetch data and writes it in excel file
	 * @param genomeId : the id of organism data
	 */
	protected void fetchGenomeData(String genomeId) {
		if (genomeId.trim().isEmpty())
			return;

		// id of data to fetch
		searchData.setIds(genomeId);
		entrezFetch = new EntrezFetch(searchData);
		try {
			organismData.clear();// reset data
			organism.setActiveId(genomeId);
			organismData.setOrganism(organism);
			fetchMetaData();// get organism structure information
			// Check weather an error occur
			if (organismInfosParser.isErrorFound() || organismData.getOrganismInfosMap().size() == 0) {
				return;
			}

			setFileExtention();
			// return if data has not been updated
			if (!fileExistsAndIsNewer()) {
				if (PRINT_LOGS)
					System.out.println("  Le fichier " + organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH) + " existe deja!");
				return;
			}
			fetchAndSaveData(genomeId);
		} catch (Exception exception) {
			if (DEBUG)
				exception.printStackTrace();
			return;
		}
	}

	/**
	 * Fetching organism structure information by retrieving data in genbank report format
	 */
	protected void fetchMetaData() {
		initFetchEngine(GENEPEPT_REPORT);
		int tryAgain = 0;
		boolean querySuccess = true;

		do {
			try {
				entrezFetch.doQuery(organismInfosParser);
				querySuccess = true;
			} catch (IOException exception) {
				if (DEBUG)
					exception.printStackTrace();
				tryAgain++;
				querySuccess = false;
			}
		} while (!querySuccess && tryAgain < MAX_TRIAL);
	}

	/**
	 * Fetching Data with suitable format
	 */
	public void fetchAndSaveData(String genomeId) {}

	/**
	 * Check if the file being download exists on the disc and has been updated
	 * @return Return <b>true</b> if the file being downloaded exists and an update occurred,
	 *         <b>false</b> otherwise
	 */
	protected boolean fileExistsAndIsNewer() {
		return true;
	}

	protected void setFileExtention() {
		String path = organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH);
		path += EXCEL_FILE_EXTENSION;
		organismData.getOrganismInfosMap().put(ORGANISM_FILE_PATH, path);
	}

	/**
	 * Initilize entrez engine : {@link EntrezSearch} or {@link EntrezFetch}
	 * @param entrezEngine the engine to initialize. It can be {@link EntrezSearch} or
	 *            {@link EntrezFetch}
	 */
	protected void initEntrezEgine(Entrez entrezEngine) {
		entrezEngine.setDatabase(Entrez.DB_NUCCORE);
		entrezEngine.setQueryMethod(true);// Utilisation de la methode GET
		entrezEngine.setMaxRetrieval(MAX_RETRIEVAL);
		entrezEngine.setTimeout(TIME_OUT_SECOND);
		entrezEngine.setEmail(MainConstants.EMAIL);

	}

	/**
	 * Initialize the fetch engine
	 * @param dataReturnFormat : String indicating in what format to fetch data
	 */
	protected void initFetchEngine(String dataReturnFormat) {
		initEntrezEgine(entrezFetch);
		entrezFetch.setRetType(dataReturnFormat);
	}

	/**
	 * Return the {@link Organism}
	 * @return return the organism name
	 */
	public Organism getOrganis() {
		return organism;
	}

	/**
	 * Set the {@link Organism} information
	 * @param organism
	 */
	public void setOrganism(Organism organism) {
		if (organism != null)
			this.organism = organism;
	}

	/**
	 * Check and sets the organism name
	 * @param organismName : the organism name
	 */
	public void setOrganismName(String organismName) {
		organism.setName(organismName);
		organism.setBioProject("");
	}

	/**
	 * Weather to extract or not the organism toxonomy in the stream being parsed. This taxonomy
	 * will so be the saved file path denotation
	 * @param state true to extract , false otherwise
	 */
	public void extractTaxonomy(boolean state) {
		organismInfosParser.setExtractTaxonomy(state);
	}

}
