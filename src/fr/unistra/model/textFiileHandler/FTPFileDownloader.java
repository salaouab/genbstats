package fr.unistra.model.textFiileHandler;

import static fr.unistra.utils.FileUtil.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 * This class handles file downloading on ftp server
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class FTPFileDownloader {
	private String	server	                   = NCBI_FTP_SERVER;
	private String	pathToDownloadFileOnServer	= "";
	private String	pathToSaveFileOnLocal	   = "";
	private String	userName	               = "anonymous";
	private String	password	               = "password";
	private int	   port	                       = 21;
	FTPClient	   ftpClient	               = new FTPClient();

	/**
	 * Default constructor
	 */
	public FTPFileDownloader() {}

	/**
	 * Constructor with additional information
	 * 
	 * @param server : ftp server url
	 * @param pathToDownloadFileOnServer : file path on the ftp server
	 * @param pathToSaveFileOnLocal : file path on local disc to save downloading file
	 * @param userName : the user name
	 * @param password : the user password
	 */
	public FTPFileDownloader(String server, String pathToDownloadFileOnServer, String pathToSaveFileOnLocal, String userName, String password) {
		this.server = server;
		this.pathToDownloadFileOnServer = pathToDownloadFileOnServer;
		this.pathToSaveFileOnLocal = pathToSaveFileOnLocal;
		this.userName = userName;
		this.password = password;
	}

	/**
	 * Constructor with server, username and password informed
	 * 
	 * @param server : ftp server url
	 * @param userName : the user name
	 * @param password : user password
	 */
	public FTPFileDownloader(String server, String userName, String password) {
		this.server = server;
		this.userName = userName;
		this.password = password;
	}

	/**
	 * Set the files path
	 * 
	 * @param pathToDownloadFileOnServer file path on ftp server
	 * @param pathToSaveFileOnLocal file path on local disc
	 */
	public void setFilesPath(String pathToDownloadFileOnServer, String pathToSaveFileOnLocal) {
		this.pathToDownloadFileOnServer = pathToDownloadFileOnServer;
		this.pathToSaveFileOnLocal = pathToSaveFileOnLocal;
	}

	/**
	 * Download file from ftp server to local disc
	 * 
	 * @param pathToDownloadFileOnServer : file path on ftp server
	 * @param pathToSaveFileOnLocal : local path to save the downloading file
	 * @return Return <b>true</b> if it precess well, <b>false</b> otherwise
	 */
	public boolean download(String pathToDownloadFileOnServer, String pathToSaveFileOnLocal) {
		setFilesPath(pathToDownloadFileOnServer, pathToSaveFileOnLocal);
		return download();
	}

	/**
	 * Dowload file on the ftp server
	 * 
	 * @return Return <b>true</b> if it process well , <b>false</b> otherwise
	 */
	public boolean download() {
		boolean status = false;
		try {

			ftpClient.connect(server, port);
			ftpClient.login(userName, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.ASCII_FILE_TYPE);

			File downloadFile = new File(pathToSaveFileOnLocal);
			OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
			status = ftpClient.retrieveFile(pathToDownloadFileOnServer, outputStream);
			outputStream.close();

		} catch (Exception e) {}
		return status;
	}

	/**
	 * Return the ftp server url
	 * 
	 * @return the server url
	 */
	public String getServer() {
		return server;
	}

	/**
	 * Set the ftp server url
	 * 
	 * @param server : the ftp server url
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * Return the file on the server path
	 * 
	 * @return the server file path
	 */
	public String getPathToDownloadFileOnServer() {
		return pathToDownloadFileOnServer;
	}

	/**
	 * Set the file to download on the server path
	 * 
	 * @param pathToDownloadFileOnServer server file path
	 */
	public void setPathToDownloadFileOnServer(String pathToDownloadFileOnServer) {
		this.pathToDownloadFileOnServer = pathToDownloadFileOnServer;
	}

	/**
	 * Return the output file path
	 * 
	 * @return the output file path
	 */
	public String getPathToSaveFileOnLocal() {
		return pathToSaveFileOnLocal;
	}

	/**
	 * Set the file output path on the local machine
	 * 
	 * @param pathToSaveFileOnLocal : output file path
	 */
	public void setPathToSaveFileOnLocal(String pathToSaveFileOnLocal) {
		this.pathToSaveFileOnLocal = pathToSaveFileOnLocal;
	}

	/**
	 * Return the user name on the ftp server
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Set the user name on the ftp server
	 * 
	 * @param userName : the user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * get the user password
	 * 
	 * @return the user password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * set user password on the ftp server
	 * 
	 * @param password: the user password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Return the server listening port number
	 * 
	 * @return the server port
	 */
	/**
	 * @return
	 */
	public int getPort() {
		return port;
	}

	/**
	 * set the port of the server listener
	 * 
	 * @param port : port number
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Downloads some spécifique genome file on the ncbi ftp server
	 */
	public static void updateGenomeReportFiles() {
		FTPFileDownloader fileDownloader = new FTPFileDownloader();

		new File(GENOME_REPORT_DIRECTORY).mkdirs();
		fileDownloader.download(SERVER_README_PATH, README_PATH);
		fileDownloader.download(SERVER_OVERVIEW_PATH, OVERVIEW_PATH);
		fileDownloader.download(SERVER_PLASMIDS_PATH, PLASMIDS_PATH);
		fileDownloader.download(SERVER_EUKARYOTES_PATH, EUKARYOTES_PATH);
		fileDownloader.download(SERVER_PROKARYOTES_PATH, PROKARYOTES_PATH);
		fileDownloader.download(SERVER_PROKARYOTES_REFERENCE_GENOME_PATH, PROKARYOTES_REFERENCE_GENOME_PATH);
		fileDownloader.download(SERVER_PROKARYOTES_REPRESENTATIVE_GENOME_PATH, PROKARYOTES_REPRESENTATIVE_GENOME_PATH);
		fileDownloader.download(SERVER_VIRUS_PATH, VIRUSES_PATH);
	}

}
