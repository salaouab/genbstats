package fr.unistra.Launcher;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unistra.view.downloadHandler.DownloadFrame;

/**
 * Launch the download manager Wingows.
 * @author Abdoul-djawadou SALAOU
 */
public class GuiLauncher {

	public static void main(String[] args) {

		// Setting look & feel for the application
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (Exception e) {

		}
		// initialising the windows
		DownloadFrame downloadFrame = new DownloadFrame();
		downloadFrame.setVisible(true);
		// Initiliasing download manager
		downloadFrame.initDownloadManager();
	}

}
