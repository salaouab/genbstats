package fr.unistra.controller.dataFetchingHandler;

import static fr.unistra.utils.MainConstants.PRINT_LOGS;
import fr.unistra.model.beans.Organism;
import fr.unistra.model.beans.OrganismList;
import fr.unistra.model.excelFileHandler.ComputeSum;
import fr.unistra.utils.FileUtil;
import fr.unistra.utils.MainConstants;

/**
 * Fetch some number of {@link Organism} data on the ncbi dataBases
 * @author Abdoul-Djawadou SALAOU
 */
public class NcbiDataFetchingWorker implements Runnable {
	private int	             numberOfDataToFetch;
	private boolean	         continueDataFetching	  = true;
	private NcbiDataFetching	ncbiDataFetching;
	private OrganismList	 organismNamesList;
	private int	             workerId	              = 0;
	private static int	     Number	                  = 0;
	public static boolean	 COMPUTE_HIERARCHICAL_SUM	= false;

	public NcbiDataFetchingWorker(NcbiDataFetchingCdsForStats workType, OrganismList organismList, int numberOfDataToFetch) {
		initParameters(organismList, numberOfDataToFetch);
		ncbiDataFetching = workType;
	}

	public NcbiDataFetchingWorker(NcbiDataFetchingCdsAsTxt workType, OrganismList organismList, int numberOfDataToFetch) {
		initParameters(organismList, numberOfDataToFetch);
		ncbiDataFetching = workType;
	}

	public NcbiDataFetchingWorker(NcbiDataFetchingWholeSequenceAsTxt workType, OrganismList organismList, int numberOfDataToFetch) {
		initParameters(organismList, numberOfDataToFetch);
		ncbiDataFetching = workType;
	}

	/**
	 * Constructor with additional information
	 * @param organismNamesList : the OrganismList
	 * @param numberOfDataToFetch : Number of organism to fetch
	 * @return
	 */
	private void initParameters(OrganismList organismNamesList, int numberOfDataToFetch) {
		this.organismNamesList = organismNamesList;
		this.numberOfDataToFetch = numberOfDataToFetch;
		workerId = ++Number;
		if (numberOfDataToFetch <= 0)
			throw new IllegalArgumentException("The number of data to fetch must be greater than zero ");
	}

	@Override
	public void run() {
		int numberOfDataAlreadyFetched = 0;
		Organism organism = null;
		do {
			organism = organismNamesList.getNextOrganism();

			if (organism == null) {

				continueDataFetching = organismNamesList.isOrganismAvailable() && numberOfDataAlreadyFetched < numberOfDataToFetch;
				continue;
			}
			if (PRINT_LOGS)
				System.out.println("\nThread " + workerId + " -- " + organism + "      (" + organismNamesList.getCursorPosition() + ")");
			ncbiDataFetching.setOrganism(organism);
			ncbiDataFetching.fecthData();
			numberOfDataAlreadyFetched++;
			continueDataFetching = organismNamesList.isOrganismAvailable() && numberOfDataAlreadyFetched < numberOfDataToFetch;
			// sleep(1);
		} while (!MainConstants.INTERUPTOR_FLAG && continueDataFetching);
		if (PRINT_LOGS)
			System.out.println("CursorPosition  --- " + organismNamesList.getCursorPosition() + " / " + organismNamesList.getNumberOfOrganism());

		if (organismNamesList.getCursorPosition() == organismNamesList.getNumberOfOrganism() && COMPUTE_HIERARCHICAL_SUM) {
			computeHierarchicalSum();
		}
	}

	/**
	 * Compute the hierarchical sum of genome structures of organisms
	 */
	public synchronized static void computeHierarchicalSum() {
		if (!COMPUTE_HIERARCHICAL_SUM)
			return;
		COMPUTE_HIERARCHICAL_SUM = false;
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(10000);
				} catch (Exception e) {}
				new ComputeSum(FileUtil.ORGANISM_BASE_DIRECTORY).compute();
			}
		}).start();

	}
}
