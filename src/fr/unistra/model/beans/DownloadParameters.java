package fr.unistra.model.beans;

/**
 * Holds downloads parameters It is initialised to match default configuration
 * @author Abdoul-Djawadou SALAOU
 */
public class DownloadParameters {
	public enum KindOfTask {
		COMPUTE_STATS, GET_WHOLE_SEQUENCE, GET_CDS_SEQUENCE
	}

	private boolean	   isEukaryotaSelected	  = true;
	private boolean	   isProkaryoteSelected	  = true;
	private boolean	   isViruesSelected	      = true;
	private boolean	   isHSumSelected	      = true;
	private boolean	   isExtendSearchSelected	= false;
	private KindOfTask	workToDo	          = KindOfTask.COMPUTE_STATS;

	public boolean isEukaryotaSelected() {
		return isEukaryotaSelected;
	}

	public void setEukaryotaSelected(boolean isEukaryotaSelected) {
		this.isEukaryotaSelected = isEukaryotaSelected;
	}

	public boolean isProkaryoteSelected() {
		return isProkaryoteSelected;
	}

	public void setProkaryoteSelected(boolean isProkaryoteSelected) {
		this.isProkaryoteSelected = isProkaryoteSelected;
	}

	public boolean isViruesSelected() {
		return isViruesSelected;
	}

	public void setViruesSelected(boolean isViruesSelected) {
		this.isViruesSelected = isViruesSelected;
	}

	public boolean isHSumSelected() {
		return isHSumSelected;
	}

	public void setHSumSelected(boolean isHSumSelected) {
		this.isHSumSelected = isHSumSelected;
	}

	public boolean isExtendSearchSelected() {
		return isExtendSearchSelected;
	}

	public void setExtendSearchSelected(boolean isExtendSearchSelected) {
		this.isExtendSearchSelected = isExtendSearchSelected;
	}

	public KindOfTask getWorkToDo() {
		return workToDo;
	}

	public void setWorkToDo(KindOfTask workToDo) {
		this.workToDo = workToDo;
	}

	public boolean isHierarchicalSumEnabled() {
		return workToDo == KindOfTask.COMPUTE_STATS && isHSumSelected;
	}
}
