package fr.unistra.model.excelFileHandler;

import static fr.unistra.model.excelFileHandler.ComputeSum.*;
import static fr.unistra.utils.ExcelFileUtils.ADDITIONAL_DATA_WIDTH;
import static fr.unistra.utils.ExcelFileUtils.SUM_SHEET_NAME;
import static fr.unistra.utils.FileUtil.FILE_SEPARATOR;
import static fr.unistra.utils.MainConstants.*;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

/**
 * This class handles hierarchical sum writing handling.
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class WriteSumData {
	private int	                  level	                      = -1;
	private TreeMap<String, Long>	genomeSumMap;
	private TreeMap<String, ?>	  additionnalValueSumMap;
	private String	              organismFilePath	          = "";
	private String	              taxonomyLevelKey;
	private String	              taxonomyLevelValue;
	private String	              kingdomList	              = "Kingdoms";
	private File	              organismFile;

	private XSSFWorkbook	      workbook;
	private XSSFSheet	          sheet;
	private XSSFFont	          sheetFont;
	// Styles
	private XSSFCellStyle	      totalValueStyle;
	private XSSFCellStyle	      totalStyle;
	private XSSFCellStyle	      additionalDataStyle;
	private XSSFCellStyle	      additionalDataStyle2;
	public static final String[]	SUM_ADDITIONAL_DATA_FIELD	= { NUMBER_OF_KINGDOM, NUMBER_OF_GROUP, NUMBER_OF_SUBGROUP, NUMBER_OF_ORGANISM,
		NUMBER_OF_NUCLEOTIDE, NUMBER_OF_SEQUENCE, NUMBER_OF_INVALID_CDS, };

	/**
	 * Default constructor
	 */
	public WriteSumData() {}

	/**
	 * Constructor with genome sum and additional value sum map given.
	 * 
	 * @param genomeSumMap : Map that holds genome hierarchical sum
	 * @param additionnalValueSumMap : Map that holds additional value sum
	 */
	public WriteSumData(TreeMap<String, Long> genomeSumMap, TreeMap<String, ?> additionnalValueSumMap) {
		this.genomeSumMap = genomeSumMap;
		this.additionnalValueSumMap = additionnalValueSumMap;
	}

	/**
	 * Constructor with default value informed
	 * 
	 * @param genomeSumMap : Map that holds genome hierarchical sum
	 * @param additionnalValueSumMap : Map that holds additional value sum
	 * @param level : Hierarchical level (1->Group; 2-> Subgroup; 3->Organism)
	 * @param organismFilePath : Path to save excel file
	 */
	public WriteSumData(TreeMap<String, Long> genomeSumMap, TreeMap<String, ?> additionnalValueSumMap, int level, String organismFilePath) {
		setGenomeSumMap(genomeSumMap);
		setAdditionnalValueSumMap(additionnalValueSumMap);
		setLevel(level);
		setOrganismFilePath(organismFilePath);
	}

	/**
	 * Writes data to the excel file denoted by the organism file path field
	 * 
	 * @param addSuffix: if true , sum_sheet_xxx is add to the file path ending.
	 */

	public void write(boolean addSuffix) {

		try {
			openWorkbook(addSuffix);
			createSheet();
		} catch (InvalidFormatException | IOException exception) {
			exception.printStackTrace();
		}

		initTotalStyle();
		writeGenomeSum();
		initAdditonalDataStyle();
		writeSumAdditionalData();
		autosizeColumns();
		save();
	}

	public void write() {
		write(true);
	}

	/**
	 * Creates sheet
	 */
	private void createSheet() {
		if (workbook.getSheet(SUM_SHEET_NAME) != null)
			workbook.removeSheetAt(workbook.getSheetIndex(SUM_SHEET_NAME));
		sheet = workbook.createSheet(SUM_SHEET_NAME);
		int maxLineSize = genomeSumMap.size();
		if (additionnalValueSumMap.size() > maxLineSize)
			maxLineSize = additionnalValueSumMap.size();
		int finalLineIndex = maxLineSize + SUM_GENOME_LINE_START + 2;
		for (int lineIndex = SUM_GENOME_LINE_START; lineIndex <= finalLineIndex; lineIndex += 2)
			sheet.createRow(lineIndex);

	}

	/**
	 * Open the work book
	 * 
	 * @throws IOException
	 * @throws InvalidFormatException
	 */
	private void openWorkbook(boolean addSuffix) throws IOException, InvalidFormatException {
		if (addSuffix)
			organismFilePath += FILE_SEPARATOR + getSumWorkbookName(organismFilePath);

		organismFile = new File(organismFilePath);
		organismFile.delete();
		organismFile.getParentFile().mkdirs();
		organismFile.createNewFile();
		workbook = new XSSFWorkbook();
	}

	/**
	 * Write genome hierarchical sum map values
	 */
	private void writeGenomeSum() {
		sheet.getRow(SUM_GENOME_LINE_START).createCell(SUM_GENOME_COLUMN).setCellStyle(totalStyle);
		sheet.getRow(SUM_GENOME_LINE_START).createCell(SUM_GENOME_COLUMN + 1).setCellStyle(totalValueStyle);
		sheet.getRow(SUM_GENOME_LINE_START).getCell(SUM_GENOME_COLUMN).setCellValue(SUM_GENOME_HEADER[0]);
		sheet.getRow(SUM_GENOME_LINE_START).getCell(SUM_GENOME_COLUMN + 1).setCellValue(SUM_GENOME_HEADER[1]);

		XSSFRow row;
		int rowNumber = SUM_GENOME_LINE_START + 2;
		for (Entry<String, Long> entry : genomeSumMap.entrySet()) {
			if (entry.getValue() == 0)
				continue;

			row = sheet.getRow(rowNumber);
			if (row == null) {
				row = sheet.createRow(rowNumber);
			}
			try {
				row.createCell(SUM_GENOME_COLUMN).setCellStyle(totalStyle);
				row.createCell(SUM_GENOME_COLUMN + 1).setCellStyle(totalValueStyle);
				row.getCell(SUM_GENOME_COLUMN).setCellValue(entry.getKey());
				row.getCell(SUM_GENOME_COLUMN + 1).setCellValue(entry.getValue());
				rowNumber += 2;
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	/**
	 * Writes additional data values
	 */
	private void writeSumAdditionalData() {
		XSSFRow row;
		int currentRowIndex = SUM_GENOME_LINE_START;

		/* Write level down : kingdom? group ? subgroup? */
		checkTaxonomyLevel();
		addMergedRegion(currentRowIndex);
		row = sheet.getRow(currentRowIndex);
		row.getCell(SUM_ADDITIONAL_DATA_COLUMN).setCellValue(taxonomyLevelKey);
		row.getCell(SUM_ADDITIONAL_DATA_COLUMN + 1).setCellValue(taxonomyLevelValue);
		currentRowIndex += 2;
		/* additionnal values */
		for (String element : SUM_ADDITIONAL_DATA_FIELD) {
			if (additionnalValueSumMap.get(element) != null) {
				addMergedRegion(currentRowIndex);
				row = sheet.getRow(currentRowIndex);
				row.getCell(SUM_ADDITIONAL_DATA_COLUMN).setCellValue(element);
				row.getCell(SUM_ADDITIONAL_DATA_COLUMN + 1).setCellValue(additionnalValueSumMap.get(element) + "");
				currentRowIndex += 2;
			}
		}

	}

	/**
	 * Initialize total row style
	 */
	private void initTotalStyle() {

		initSheetFont();
		// total cell style
		totalStyle = workbook.createCellStyle();
		totalStyle.setFont(sheetFont);
		totalStyle.setAlignment(CellStyle.ALIGN_LEFT);
		totalStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		totalStyle.setFillForegroundColor(new XSSFColor(new Color(255, 128, 0)));
		totalStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		// tatal values style
		totalValueStyle = workbook.createCellStyle();
		totalValueStyle.setFont(sheetFont);
		totalValueStyle.setAlignment(CellStyle.ALIGN_CENTER);
		totalValueStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		totalValueStyle.setFillForegroundColor(new XSSFColor(new Color(102, 178, 255)));
		totalValueStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	}

	/**
	 * Initalize a font for the sheet
	 */
	private void initSheetFont() {
		// initialize font
		sheetFont = workbook.createFont();
		sheetFont.setFontName("Arial");
		sheetFont.setFontHeightInPoints((short) 12);
		// sheetFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		sheet.setDefaultRowHeightInPoints(18);
	}

	/**
	 * Merge some cells of the givin row number for additional data and set the reginons style.
	 * 
	 * @param additional_data_row Row to merge cells number
	 * @see #additionalDataStyle
	 */
	private void addMergedRegion(int additional_data_row) {
		int firstC = SUM_ADDITIONAL_DATA_COLUMN + 1;
		int lastC = firstC + ADDITIONAL_DATA_WIDTH;
		initAdditonalDataStyle();
		CellRangeAddress region;
		sheet.getRow(additional_data_row).createCell(SUM_ADDITIONAL_DATA_COLUMN).setCellStyle(additionalDataStyle);
		sheet.getRow(additional_data_row).createCell(SUM_ADDITIONAL_DATA_COLUMN + 1).setCellStyle(additionalDataStyle2);
		region = new CellRangeAddress(additional_data_row, additional_data_row, firstC, lastC);
		sheet.addMergedRegion(region);
	}

	/**
	 * Initialize additional data style
	 */
	private void initAdditonalDataStyle() {
		additionalDataStyle = workbook.createCellStyle();
		additionalDataStyle.setAlignment(CellStyle.ALIGN_LEFT);
		additionalDataStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		additionalDataStyle.setFont(sheetFont);
		additionalDataStyle.setFillForegroundColor(new XSSFColor(new Color(255, 228, 196)));
		additionalDataStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		additionalDataStyle2 = workbook.createCellStyle();
		additionalDataStyle2.setAlignment(CellStyle.ALIGN_LEFT);
		additionalDataStyle2.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		additionalDataStyle2.setWrapText(true);
		additionalDataStyle2.setFont(sheetFont);
		additionalDataStyle2.setFillForegroundColor(new XSSFColor(new Color(176, 196, 222)));
		additionalDataStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);

	}

	/**
	 * Auto-size excel file columns
	 */
	private void autosizeColumns() {

		// create collapsable row zone
		try {

			int endColumn = SUM_ADDITIONAL_DATA_COLUMN + ADDITIONAL_DATA_WIDTH + 1;

			sheet.groupColumn((short) 1, (short) SUM_ADDITIONAL_DATA_COLUMN - 2);
			sheet.setColumnGroupCollapsed((short) 1, false);

			sheet.groupColumn((short) SUM_ADDITIONAL_DATA_COLUMN, (short) endColumn);
			sheet.setColumnGroupCollapsed((short) SUM_ADDITIONAL_DATA_COLUMN + 1, false);

			sheet.groupRow(5, 12);
			sheet.groupRow(14, 23);
			sheet.groupRow(25, 33);
		} catch (Exception e) {}

		// resize column
		for (int i = 0; i < SUM_ADDITIONAL_DATA_COLUMN + 2; i++)
			try {
				sheet.autoSizeColumn(i);
			} catch (Exception e) {}

		sheet.setColumnWidth(SUM_ADDITIONAL_DATA_COLUMN - 1, 7000);
	}

	/**
	 * Save modifications made in workbook. <br>
	 * Note: After this method calling the workbook is not available anymore.
	 */
	public void save() {
		try {
			if (workbook != null) {
				OutputStream organismOutputStream = new FileOutputStream(organismFile);
				workbook.write(organismOutputStream);
				organismOutputStream.close();
				if (PRINT_LOGS)
					System.out.println("\t==> Saving " + organismFile.getPath());
				workbook = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public TreeMap<String, ?> getGenomeSumMap() {
		return genomeSumMap;
	}

	public void setGenomeSumMap(TreeMap<String, Long> genomeSumMap) {
		this.genomeSumMap = genomeSumMap;
	}

	public TreeMap<String, ?> getAdditionnalValueSumMap() {
		return additionnalValueSumMap;
	}

	public void setAdditionnalValueSumMap(TreeMap<String, ?> additionnalValueSumMap) {
		this.additionnalValueSumMap = additionnalValueSumMap;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getOrganismFilePath() {
		return organismFilePath;
	}

	public void setOrganismFilePath(String organismFilePath) {
		this.organismFilePath = organismFilePath;
	}

	public void setParameter(TreeMap<String, Long> genomeSumMap, TreeMap<String, ?> additionnalValueSumMap, int level, String organismFilePath) {
		setGenomeSumMap(genomeSumMap);
		setAdditionnalValueSumMap(additionnalValueSumMap);
		setLevel(level);
		setOrganismFilePath(organismFilePath);
	}

	public String getKingdomList() {
		return kingdomList;
	}

	public void setKingdomList(String kingdomList) {
		this.kingdomList = kingdomList;
	}

	/**
	 * Check and store hierarchical informations
	 */
	public void checkTaxonomyLevel() {
		String[] pathElements = null;
		try {
			String path = organismFilePath.substring(organismFilePath.indexOf(KINGDOM));
			if (FILE_SEPARATOR.equals("/"))
				pathElements = path.split("/");
			else
				pathElements = path.split("\\\\");
		} catch (Exception exception) {
			taxonomyLevelKey = "Name";
			taxonomyLevelValue = UNKNOWN;
			return;
		}
		switch (level) {
			case 0:
				taxonomyLevelKey = "Kingdoms";
				taxonomyLevelValue = kingdomList;
				break;
			case 1:
				taxonomyLevelKey = "Kingdom name";
				taxonomyLevelValue = pathElements[1];
				break;
			case 2:
				taxonomyLevelKey = "Group name";
				taxonomyLevelValue = pathElements[1] + "; " + pathElements[2];
				break;
			case 3:
				taxonomyLevelKey = "Subgroup name";
				taxonomyLevelValue = pathElements[1] + "; " + pathElements[2] + "; " + pathElements[3];
				break;
			case 4:
				taxonomyLevelKey = "Kingdom name";
				taxonomyLevelValue = "Prokaryota: " + kingdomList;
				break;
			default:
				taxonomyLevelKey = "Name";
				taxonomyLevelValue = UNKNOWN;
		}
	}

	public static String getSumWorkbookName(String path) {
		String sumWorkbookName = SUM_SHEET_NAME + "_";
		sumWorkbookName += path.substring(path.lastIndexOf(FILE_SEPARATOR) + 1);
		sumWorkbookName += EXCEL_FILE_EXTENSION;
		return sumWorkbookName;
	}
}
