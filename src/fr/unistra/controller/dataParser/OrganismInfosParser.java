package fr.unistra.controller.dataParser;

import static fr.unistra.utils.FileUtil.BASE_DIRECTORY;
import static fr.unistra.utils.MainConstants.*;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.algosome.eutils.io.InputStreamParser;

import fr.unistra.model.beans.OrganismData;

/**
 * This class parses the fetching organism information from ncbi databases
 * @author Abdoul-Djawadou SALAOU
 */

public class OrganismInfosParser implements InputStreamParser {

	Map<String, String>	        organismInfos	= new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
	// true means error found in parsing
	private boolean	            errorFound	    = true;
	// false means not to store extracted value
	private boolean	            extractTaxonomy	= false;
	private StringBuilder	    field;
	private int	                taxonomyStart	= 0;
	// Root name of file path
	private String	            rootName	    = KINGDOM;

	private final static Random	random	        = new Random();

	/**
	 * Constructor with the organism information map informed
	 * @param organismInfos : map holding organism information
	 */
	public OrganismInfosParser(TreeMap<String, String> organismInfos) {
		this.organismInfos = organismInfos;
		field = new StringBuilder();
	}

	/**
	 * Constructor with the {@link OrganismData} informed
	 * @param organismData : holds the organism data
	 */
	public OrganismInfosParser(OrganismData organismData) {
		this(organismData.getOrganismInfosMap());
	}

	/**
	 * Parses and store the organism information - such as name, taxonomy , modification date ,
	 * accession number- into the map associated
	 * @see com.algosome.eutils.io.InputStreamParser#parseInput(java.io.InputStream)
	 */
	@Override
	public void parseInput(InputStream inputStream) throws IOException {
		if (organismInfos == null) {
			errorFound = true;
			if (PRINT_LOGS)
				System.out.println("organismInfos map is not initialize (null value found)");
			return;
		}
		errorFound = true;
		boolean isLocusExtracted = false;
		boolean isDefinitionExtracted = false;
		boolean isAccessionExtracted = false;
		boolean isOrganismExtracted = false;
		boolean isReferenceExtracted = false;
		boolean isTitleExtracted = false;
		boolean finished = false;
		InputStreamReader streamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(streamReader);
		String line = null;
		field.setLength(0);

		while (!finished && (line = bufferedReader.readLine()) != null) {
			// Information sur le locus, date de modification
			if (!isLocusExtracted && startWithTerm(line, LOCUS)) {
				field.append(line.trim());
				while ((line = bufferedReader.readLine()) != null) {
					if (startWithTerm(line, DEFINITION))
						break;
					field.append("\n");
					field.append(line.trim());
				}
				organismInfos.put(LOCUS, field.toString());
				field.setLength(0);
				isLocusExtracted = true;
			}
			// Definition sur le type de molécule et l'oganisme
			if (!isDefinitionExtracted && startWithTerm(line, DEFINITION)) {
				field.append(line.trim());
				while ((line = bufferedReader.readLine()) != null) {
					if (startWithTerm(line, ACCESSION))
						break;
					field.append("\n");
					field.append(line.trim());
				}
				organismInfos.put(DEFINITION, field.toString());
				field.setLength(0);
				isDefinitionExtracted = true;
			}
			// Le numero d'accession
			if (!isAccessionExtracted && startWithTerm(line, ACCESSION)) {
				field.append(line.trim());
				while ((line = bufferedReader.readLine()) != null) {
					if (startWithTerm(line, "VERSION") || startWithTerm(line, "DBLINK") || startWithTerm(line, ORGANISM))
						break;
					field.append("\n");
					field.append(line.trim());
				}
				organismInfos.put(ACCESSION, field.toString());
				field.setLength(0);
				isAccessionExtracted = true;
			}
			// the organism name, and it's taxonomy
			if (!isOrganismExtracted && startWithTerm(line, ORGANISM)) {
				field.append(line.trim());
				while ((line = bufferedReader.readLine()) != null) {
					if (startWithTerm(line, REFERENCE) | startWithTerm(line, "COMMENT"))
						break;
					field.append("\n");
					field.append(line.trim());
				}
				organismInfos.put(ORGANISM, field.toString());
				field.setLength(0);
				isOrganismExtracted = true;

			}
			// nucleotide reference
			if (!isReferenceExtracted && startWithTerm(line, REFERENCE)) {
				field.append(line.trim());
				while ((line = bufferedReader.readLine()) != null) {
					if (startWithTerm(line, "AUTHOR") || startWithTerm(line, "CONSTRM") || startWithTerm(line, TITLE))
						break;
					field.append("\n");
					field.append(line.trim());
				}
				organismInfos.put(REFERENCE, field.toString());
				field.setLength(0);
				isReferenceExtracted = true;
			}
			// Title of annotation
			if (!isTitleExtracted && startWithTerm(line, TITLE)) {
				field.append(line.trim());
				while ((line = bufferedReader.readLine()) != null) {
					if (startWithTerm(line, "JOURNAL") || startWithTerm(line, "COMMENT"))
						break;
					field.append("\n");
					field.append(line.trim());
				}
				organismInfos.put(TITLE, field.toString());
				field.setLength(0);
				finished = true;
				isTitleExtracted = true;
			}
		}

		bufferedReader.close();
		streamReader.close();
		inputStream.close();
		extractOrganismInfos();
	}

	/**
	 * Says if an error occured during the parsing
	 * @return <b>true</b> if an error occured, <b>false</b> otherwise
	 */
	public boolean isErrorFound() {
		return errorFound;
	}

	/**
	 * Extract the organism meta Data
	 */
	private void extractOrganismInfos() {
		// mandatory parametters
		try {

			extractOrganismName();
			extractLastModificationDate();
			extractOrganismTaxonomy();
			errorFound = false;
		} catch (Exception e) {
			errorFound = true;
			return;
		}
		// Optional parameters
		try {
			refactorField();
			extractGeneticStructureType();
			extractOrganismSheetName();
			extractOrganismFilePath();
			extractNumberOfNuclotide();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clear field of some unwanted term
	 */
	private void refactorField() {
		String field;

		field = deleteTerm(LOCUS, organismInfos.get(LOCUS));
		organismInfos.put(LOCUS, field);

		field = deleteTerm(DEFINITION, organismInfos.get(DEFINITION));
		organismInfos.put(DEFINITION, field);

		field = deleteTerm(ACCESSION, organismInfos.get(ACCESSION));
		organismInfos.put(ACCESSION, field);

		field = deleteTerm(TITLE, organismInfos.get(TITLE));
		organismInfos.put(TITLE, field);

	}

	/**
	 * Extract the organism name
	 */
	private void extractOrganismName() {
		String[] lines = organismInfos.get(ORGANISM).trim().split("\n");
		if (extractTaxonomy) {
			String organismName = UNKNOWN;
			organismName = deleteTerm(ORGANISM, lines[0]);
			taxonomyStart = 1;

			for (int i = 1; i < lines.length; i++) {
				if (startWithKingdom(lines[i])) {
					taxonomyStart = i;
					break;

				}
				organismName += lines[i].trim();
			}
			organismInfos.put(ORGANISM_NAME, organismName);

		}
		else {

			for (int i = 1; i < lines.length; i++)
				if (startWithKingdom(lines[i])) {
					taxonomyStart = i;
					break;
				}

		}
	}

	/**
	 * Extract the organism structure update date
	 */
	private void extractLastModificationDate() {
		organismInfos.put(LAST_MODIFICATION_DATE, getDateString(organismInfos.get(LOCUS)));
	}

	/**
	 * Extract the organism number of nucleotid
	 */
	private void extractNumberOfNuclotide() {
		Pattern pattern = Pattern.compile("(^[0-9]+$)");
		String[] words = organismInfos.get(LOCUS).split(" ");
		Matcher matcher;
		for (String word : words) {
			matcher = pattern.matcher(word.trim());
			if (!matcher.find())
				continue;

			organismInfos.put(NUMBER_OF_NUCLEOTIDE, matcher.group(1).trim());
			return;
		}
		organismInfos.put(NUMBER_OF_NUCLEOTIDE, "0");
	}

	/**
	 * Extract the organism taxonomy <b>Kingdom, Group, SubGroup</b>
	 */
	private void extractOrganismTaxonomy() {
		String[] lines = organismInfos.get(ORGANISM).trim().split("\n");
		String taxonomy = "";

		for (int i = taxonomyStart; i < lines.length; i++)
			taxonomy += lines[i].trim() + " ";
		lines = taxonomy.split(";");
		int length = lines.length;

		if (length > 0)
			organismInfos.put(KINGDOM, lines[0].replaceAll("\\.", "").trim());
		else {
			organismInfos.put(KINGDOM, UNKNOWN);
			taxonomy = UNKNOWN;
		}
		if (extractTaxonomy) {

			if (length > 1)
				organismInfos.put(GROUP, lines[1].replaceAll("\\.", "").trim());
			else {
				organismInfos.put(GROUP, UNKNOWN);
				taxonomy += "; " + UNKNOWN;
			}

			if (length > 2)
				organismInfos.put(SUB_GROUP, lines[2].replaceAll("\\.", "").trim());
			else {
				organismInfos.put(SUB_GROUP, UNKNOWN);
				taxonomy += "; " + UNKNOWN;
			}
		}
		organismInfos.put(ORGANISM_TAXONOMY, taxonomy);
	}

	/**
	 * Extract the organism structure type( chromosome, plasmid,mitochondrion,...)
	 */
	private void extractGeneticStructureType() {
		if (!extractTaxonomy)
			return;
		String geneticStructureType = UNKNOWN;
		String definition = organismInfos.get(DEFINITION);

		for (String type : genomeType) {
			if (containsTerm(definition, type)) {
				geneticStructureType = type;
				break;
			}
		}
		if (geneticStructureType == UNKNOWN) {
			if (containsTerm(definition, "mitochondrial"))
				geneticStructureType = MITOCHONDRION;

			else if (containsTerm(definition, "complete.*genome") || containsTerm(definition, "draft.*genome")
					|| containsTerm(organismInfos.get(LOCUS), "DNA.*circ"))
				geneticStructureType = CHROMOSOME;
		}
		if (geneticStructureType.equals(UNKNOWN))
			System.err.println("Unknown:  " + definition);

		organismInfos.put(GENETIC_STRUCTURE_TYPE, geneticStructureType);
	}

	/**
	 * Extract the organism structure sheet name. It is represented by the structure type
	 * (chromosome, plasmid,...) and the organism structure accession id. that last field ensure the
	 * uniqueness of the sheet name in the excel file.
	 */
	private void extractOrganismSheetName() {
		String structure = organismInfos.get(GENETIC_STRUCTURE_TYPE);
		String accessionNumber = organismInfos.get(LOCUS).split("\\s")[0];
		if (accessionNumber.isEmpty())
			accessionNumber = organismInfos.get(ACCESSION).split("\\s")[0];
		structure = normalise(structure + "_" + accessionNumber);
		if (structure.length() > 30)
			structure = structure.substring(0, 25) + getRandomNumber();

		organismInfos.put(ORGANISM_SHEET_NAME, structure);

	}

	/**
	 * Extract the path to save organism data. this path is represented by the organism taxonomy
	 * Kingdom/Group/subgroup/organismName
	 */
	private void extractOrganismFilePath() {
		String kingdom = normalise(organismInfos.get(KINGDOM));
		String group = normalise(organismInfos.get(GROUP));
		String subGroup = normalise(organismInfos.get(SUB_GROUP));
		String organismName = normalise(organismInfos.get(ORGANISM_NAME));
		String organismFileDirectory = BASE_DIRECTORY + rootName + "/" + kingdom + "/" + group + "/" + subGroup + "/" + organismName;
		organismInfos.put(ORGANISM_FILE_PATH, organismFileDirectory);
	}

	/**
	 * Delete the term in the line
	 * @param term : term to delete
	 * @param line : line in which to deleter term
	 * @return return the line with the term deleted
	 */
	public static String deleteTerm(String term, String line) {
		if (line == null)
			return UNKNOWN;
		// (?i) ignore case
		String regex = "\\s*\\b(?i)" + term + "\\b\\s*";
		return line.replaceFirst(regex, "").replace("\n", " ").trim();

	}

	public String getRootName() {
		return rootName;
	}

	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

	public boolean isExtractTaxonomy() {
		return extractTaxonomy;
	}

	public void setExtractTaxonomy(boolean extractTaxonomy) {
		this.extractTaxonomy = extractTaxonomy;
	}

	public static int getRandomNumber() {
		return random.nextInt(100);

	}

	@Override
	public void parseFrom(int start) {}

	@Override
	public void parseTo(int end) {}

}
