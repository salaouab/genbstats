package fr.unistra.model.beans;

import static fr.unistra.utils.MainConstants.*;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * This class represents the {@link Organism} data information
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class OrganismData {

	private TreeMap<String, long[]>	  codingSequenceStatisticMap	= new TreeMap<String, long[]>(String.CASE_INSENSITIVE_ORDER);
	private TreeMap<String, long[]>	  tmpCdStatisticMap	         = new TreeMap<String, long[]>(String.CASE_INSENSITIVE_ORDER);
	private TreeMap<String, long[]>	  preferencedPhaseMap	     = new TreeMap<String, long[]>(String.CASE_INSENSITIVE_ORDER);
	private TreeMap<String, String>	  organismInfosMap	         = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
	private ArrayList<CodingSequence>	codingSequenceList	     = new ArrayList<CodingSequence>();
	private int	                      numberOfInvalidCds	     = 0;
	private int	                      numberOfSequence	         = 0;

	/**
	 * Default constructor
	 */
	public OrganismData() {
		initCodingSequnceStatMap();
	}

	/**
	 * Initialize coding sequence map , for statistic computation. The int array will contain the
	 * three phases
	 */
	public void initCodingSequnceStatMap() {
		String[] letterArray = { "A", "C", "G", "T" };
		for (String letter1 : letterArray)
			for (String letter2 : letterArray)
				for (String letter3 : letterArray) {
					codingSequenceStatisticMap.put(letter1 + letter2 + letter3, new long[3]);
					preferencedPhaseMap.put(letter1 + letter2 + letter3, new long[3]);
					tmpCdStatisticMap.put(letter1 + letter2 + letter3, new long[3]);
				}
	}

	/**
	 * Compute the coding sequence statistique to {@link #codingSequenceStatisticMap}
	 */
	public void computeCodingSequenceStat() {
		for (CodingSequence codingSequence : codingSequenceList)
			codingSequenceStat(codingSequence.getSequence());

	}

	/**
	 * Compute the nucleotid occurrence on phase 0, 1 and 2. Note let "ATG ... TAG" be the coding
	 * sequence , so:<br/>
	 * - Phase 0: <b>[</b>ATG ... <b>]</b>TAG<br/>
	 * - Phase 1: A<b>[</b>TG ... T<b>]</b>AG<br/>
	 * - Phase 2: AT<b>[</b>G ... TA<b>]</b>G
	 * 
	 * @param sequence : Coding sequence
	 * @param codingSequenceStatisticMap : Map in which to fullfil the result
	 * @return Return <b>true</b> if everything goes well, <b>false</b> otherwise
	 */
	public boolean codingSequenceStat(String sequence) {
		for (int i = 0; i < sequence.length() - 3; i++) {
			tmpCdStatisticMap.get(sequence.substring(i, i + 3))[i % 3]++;
		}
		for (Entry<String, long[]> statData : tmpCdStatisticMap.entrySet()) {
			for (int i = 0; i < statData.getValue().length; i++)
				codingSequenceStatisticMap.get(statData.getKey())[i] += statData.getValue()[i];
			updatePrefPhase(statData);
			statData.getValue()[0] = statData.getValue()[1] = statData.getValue()[2] = 0;
		}

		return true;
	}

	private void updatePrefPhase(Entry<String, long[]> statData) {
		long[] statValues = statData.getValue();
		// 3 equals numbers
		if (statValues[0] == statValues[1] && statValues[1] == statValues[2]) {
			if (statValues[0] != 0) {
				preferencedPhaseMap.get(statData.getKey())[0]++;
				preferencedPhaseMap.get(statData.getKey())[1]++;
				preferencedPhaseMap.get(statData.getKey())[2]++;
			}
			else
				return;
		}

		// 1 must number
		if (statValues[0] > statValues[1] && statValues[0] > statValues[2])
			preferencedPhaseMap.get(statData.getKey())[0]++;
		if (statValues[1] > statValues[0] && statValues[1] > statValues[2])
			preferencedPhaseMap.get(statData.getKey())[1]++;
		if (statValues[2] > statValues[0] && statValues[2] > statValues[1])
			preferencedPhaseMap.get(statData.getKey())[2]++;

		// 2 must numbers
		if (statValues[0] == statValues[1] && statValues[0] > statValues[2]) {
			preferencedPhaseMap.get(statData.getKey())[0]++;
			preferencedPhaseMap.get(statData.getKey())[1]++;
			return;
		}

		if (statValues[0] == statValues[2] && statValues[0] > statValues[1]) {
			preferencedPhaseMap.get(statData.getKey())[0]++;
			preferencedPhaseMap.get(statData.getKey())[2]++;
			return;
		}
		if (statValues[2] == statValues[1] && statValues[1] > statValues[0]) {
			preferencedPhaseMap.get(statData.getKey())[1]++;
			preferencedPhaseMap.get(statData.getKey())[2]++;
			return;
		}
	}

	public long[] getPrefPhaseValues(String key) {
		return preferencedPhaseMap.get(key);
	}

	/**
	 * Re-initialize the {@link OrganismData} field information
	 */
	public void clear() {
		numberOfSequence = 0;
		numberOfInvalidCds = 0;
		codingSequenceList.clear();
		organismInfosMap.clear();
		codingSequenceStatisticMap.clear();
		preferencedPhaseMap.clear();
		tmpCdStatisticMap.clear();
		initCodingSequnceStatMap();
	}

	/**
	 * Return the coding sequence statistic map
	 * 
	 * @return Return the statistic map
	 */
	public TreeMap<String, long[]> getCodingSequenceStatisticMap() {
		return codingSequenceStatisticMap;
	}

	/**
	 * Set the coding sequence statistic map
	 * 
	 * @param codingSequenceStatisticMap : the coding sequence statistic map
	 */
	public void setCodingSequenceStatisticMap(TreeMap<String, long[]> codingSequenceStatisticMap) {
		this.codingSequenceStatisticMap = codingSequenceStatisticMap;
	}

	/**
	 * Return the {@link Organism} related information map
	 * 
	 * @return Return the {@link Organism} related information map
	 */
	public TreeMap<String, String> getOrganismInfosMap() {
		return organismInfosMap;
	}

	/**
	 * Set the {@link Organism} related information map
	 * 
	 * @param organismInfosMap the {@link Organism} related information map
	 */
	public void setOrganismInfosMap(TreeMap<String, String> organismInfosMap) {
		this.organismInfosMap = organismInfosMap;
	}

	/**
	 * Return the {@link CodingSequence} list
	 * 
	 * @return Return the {@link CodingSequence} list
	 */
	public ArrayList<CodingSequence> getCodingSequenceList() {
		return codingSequenceList;
	}

	/**
	 * Set the {@link CodingSequence} list
	 * 
	 * @param codingSequenceList : the {@link CodingSequence} list
	 */
	public void setCodingSequenceList(ArrayList<CodingSequence> codingSequenceList) {
		this.codingSequenceList = codingSequenceList;
	}

	/**
	 * Return the number of {@link CodingSequence} in the list
	 * 
	 * @return Return the {@link CodingSequence} list size
	 */
	public int getNumberOfSequence() {
		// return codingSequenceList.size();
		return numberOfSequence;
	}

	public void setNumberOfSequence(int numberOfSequence) {
		this.numberOfSequence = numberOfSequence;
		organismInfosMap.put(NUMBER_OF_SEQUENCE, getNumberOfSequence() + "");
	}

	public int getNumberOfInvalidCds() {
		return numberOfInvalidCds;
	}

	public void setNumberOfInvalidCds(int numberOfInvalidCds) {
		this.numberOfInvalidCds = numberOfInvalidCds;
		organismInfosMap.put(NUMBER_OF_INVALID_CDS, getNumberOfInvalidCds() + "");
	}

	/**
	 * Set the {@link Organism} bioProject
	 * 
	 * @param organism : the organism information
	 */
	public void setOrganismBioProject(Organism organism) {
		String bioProject = organism.getBioProject();
		setOrganismBioProject(bioProject.isEmpty() ? UNKNOWN : bioProject);
	}

	/**
	 * Set the {@link Organism} bioProject
	 * 
	 * @param bioProject : the organism bioProject
	 */
	public void setOrganismBioProject(String bioProject) {
		if (bioProject == null || bioProject.trim().isEmpty())
			bioProject = UNKNOWN;
		organismInfosMap.put(BIOPROJECT, bioProject);
	}

	public void setOrganism(Organism organism) {
		organismInfosMap.put(ORGANISM_NAME, organism.getName());
		organismInfosMap.put(GROUP, organism.getGroup());
		organismInfosMap.put(SUB_GROUP, organism.getSubGroup());
		organismInfosMap.put(BIOPROJECT, organism.getBioProject());
		organismInfosMap.put(GENETIC_STRUCTURE_TYPE, organism.getIdStructure());
	}

}