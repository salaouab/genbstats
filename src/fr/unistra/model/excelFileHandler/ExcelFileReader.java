/**
 *
 */
package fr.unistra.model.excelFileHandler;

import static fr.unistra.utils.ExcelFileUtils.ADDITIONAL_DATA_ROWS;
import static fr.unistra.utils.ExcelFileUtils.SUM_SHEET_NAME;
import static fr.unistra.utils.MainConstants.getLongValue;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.*;

/**
 * This class handles the excel file reading. Below are possibilities enumerate: <br>
 * <b>Cell reading</b> <br>
 * <b> Line or Lines reading</b> <br>
 * <b>Column or Columns reading</b>
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class ExcelFileReader {
	protected File				workbookFile;
	protected OPCPackage		opcPackage;
	protected XSSFWorkbook		workbook;
	protected XSSFSheet			sheet;
	protected FormulaEvaluator	evaluator;

	/**
	 * Default constructor with the workbook {@link File} submit. <br>
	 * Note that the workbook is not yet open. You have to specify it with the
	 * {@link #open()} method. Also do not forget to {@link #close()} it after
	 * you're done, for memory leak avoiding!
	 */
	public ExcelFileReader() {
	}

	/**
	 * Constructor with the workbook {@link File} submit. <br>
	 * Note that the workbook is not yet open. You have to specify it with the
	 * {@link #open()} method. Also do not forget to {@link #close()} it after
	 * you're done, for memory leak avoiding!
	 * 
	 * @param workbookFile
	 *            the workbook file
	 */
	public ExcelFileReader(File workbookFile) {
		this.workbookFile = workbookFile;
	}

	/**
	 * Constructor with the workbook {@link File}path submit <br>
	 * Note that the workbook is not yet open. You have to specify it with the
	 * {@link #open()} method. Also do not forget to {@link #close()} it after
	 * you're done, for memory leak avoiding!
	 * 
	 * @param workbookFilePath
	 *            workbook file path
	 */
	public ExcelFileReader(String workbookFilePath) {
		workbookFile = new File(workbookFilePath);
	}

	/**
	 * Open the workbook for reading. <br>
	 * Do not forget to {@link #close()} it after you're done, for memory leak
	 * avoiding!
	 * 
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void open() throws InvalidFormatException, IOException {
		if (workbookFile == null) {
			System.err.println("You did not supply a valid workbook file.");
			return;
		}
		opcPackage = OPCPackage.open(workbookFile);
		// workbook = new XSSFWorkbook(new FileInputStream(workbookFile));
		workbook = new XSSFWorkbook(opcPackage);
		evaluator = workbook.getCreationHelper().createFormulaEvaluator();

	}

	/**
	 * Open the workbook for reading, and load the sheet name provided. <br>
	 * Do not forget to {@link #close()} it after you're done, for memory leak
	 * avoiding!
	 * 
	 * @param sheetName
	 *            The sheet to open name
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void open(String sheetName) throws InvalidFormatException, IOException {
		open();
		loadSheet(sheetName);
	}

	/**
	 * Open the workbook for reading, and load the sheet name provided. <br>
	 * Do not forget to {@link #close()} it after you're done, for memory leak
	 * avoiding!
	 * 
	 * @param sheetIndex
	 *            The sheet to open index
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void open(int sheetIndex) throws InvalidFormatException, IOException {
		open();
		loadSheet(workbook.getSheetName(sheetIndex));
	}

	/**
	 * Close the workbook.
	 */
	public void close() {
		try {
			workbook = null;
			evaluator = null;
			if (opcPackage != null)
				opcPackage.close();
		} catch (Exception e) {
		}
	}

	/**
	 * Load the workbook sheet to read in.
	 * 
	 * @param sheetName
	 *            the sheet name
	 */
	public void loadSheet(String sheetName) {
		if (sheet == null || sheet.getSheetName() != sheetName)
			sheet = workbook.getSheet(sheetName);

		if (sheet == null)
			System.out.println("Impossible de charger la feuille \"" + sheetName + "\" du fichier " + workbookFile.getName());
	}

	public boolean isSheetExist(String sheetName) {
		if (workbook == null) {
			System.out.println("The workbook is not open yet: Try to open it");
			return false;
		}
		return workbook.getSheet(sheetName) != null;
	}

	/**
	 * Read the content of a cell by supplying its row and column index.
	 * 
	 * @param rowId
	 *            the row index
	 * @param columnId
	 *            the column index
	 * @return Return the cell value. If it is not valid, an empty string is
	 *         return.
	 */
	public String readCell(int rowId, int columnId) {
		String cell = "";
		XSSFRow row = sheet.getRow(rowId);
		if (row != null && row.getCell(columnId) != null)
			cell = getCellStringValue(row.getCell(columnId));
		return cell;
	}

	/**
	 * Read sheet verticaly (lines) in 2 columns (columnId and columnId+1).
	 * 
	 * @param rowIds
	 *            array of line to retreive indexes
	 * @param columnId
	 *            first column index
	 * @return return Line value in the {@link TreeMap}.
	 */
	public TreeMap<String, String> readLinesFromColumn(int[] rowIds, int columnId) {
		int lastRow = sheet.getLastRowNum() + 1;
		XSSFRow row;
		String key;
		String value;
		TreeMap<String, String> dataRead = new TreeMap<String, String>();
		for (int rowId : rowIds) {
			if (rowId > lastRow)
				continue;
			row = sheet.getRow(rowId);
			if (row == null)
				continue;
			try {
				if (row.getCell(columnId) != null && row.getCell(columnId + 1) != null) {
					key = getCellStringValue(row.getCell(columnId));
					value = getCellStringValue(row.getCell(columnId + 1));
					dataRead.put(key, value);
				}
			} catch (Exception e) {
			}
		}

		return dataRead;
	}

	/**
	 * Read Lines in many columns (columnId and its next).
	 * 
	 * @param rowIdsList
	 *            list of array of rows to read indexes. Note that row arrays
	 *            must match the order of column list
	 * @param columnIdList
	 *            Column list identifying rows arrays.
	 * @return Return a list of {@link TreeMap} that hold each lines of column
	 *         values.
	 * @throws Exception
	 */
	public ArrayList<TreeMap<String, String>> readLinesFromColumns(ArrayList<int[]> rowIdsList, ArrayList<Integer> columnIdList) {
		int lastRow = sheet.getLastRowNum() + 1;
		XSSFRow row;
		int[] rowIds;
		int columnId;
		String key;
		String value;
		ArrayList<TreeMap<String, String>> dataRead = new ArrayList<TreeMap<String, String>>();
		TreeMap<String, String> linesValue;
		for (int i = 0; i < columnIdList.size(); i++) {
			rowIds = rowIdsList.get(i);
			columnId = columnIdList.get(i);
			linesValue = new TreeMap<String, String>();
			for (int rowId : rowIds) {
				if (rowId > lastRow)
					continue;
				row = sheet.getRow(rowId);
				if (row == null)
					continue;
				try {
					if (row.getCell(columnId) != null && row.getCell(columnId + 1) != null) {
						key = getCellStringValue(row.getCell(columnId));
						value = getCellStringValue(row.getCell(columnId + 1));
						linesValue.put(key, value);
					}
				} catch (Exception e) {
				}
			}
			if (!linesValue.isEmpty())
				dataRead.add(linesValue);
		}
		return dataRead;
	}

	/**
	 * Read columns value of a line.
	 * 
	 * @param rowId
	 *            line index
	 * @param columnIds
	 *            column indexes
	 * @return return the columns {@link String} value in the list.
	 * @throws Exception
	 */
	public ArrayList<String> readColumnsFromLine(String sheetName, int rowId, int[] columnIds) {
		loadSheet(sheetName);
		XSSFRow row;
		String value;
		ArrayList<String> dataRead = new ArrayList<String>();
		if (rowId <= sheet.getLastRowNum()) {
			row = sheet.getRow(rowId);
			if (row != null)
				for (int columnId : columnIds) {
					try {
						if (row.getCell(columnId) != null) {
							value = getCellStringValue(row.getCell(columnId));
							dataRead.add(value);
						}
					} catch (Exception e) {
					}
				}
		}
		return dataRead;
	}

	/**
	 * read columns of lines
	 * 
	 * @param rowIdList
	 *            line indexes (must macht the columns arrays)
	 * @param columnIdsList
	 *            columns indexes of each line.
	 * @return Return a list of list; each list stands for each line columns
	 *         values
	 * @throws Exception
	 */
	public ArrayList<ArrayList<String>> readColumnsFromLines(ArrayList<Integer> rowIdList, ArrayList<int[]> columnIdsList) {
		XSSFRow row;
		int[] columnIds;
		int rowId;
		String value;
		ArrayList<ArrayList<String>> dataRead = new ArrayList<ArrayList<String>>();
		ArrayList<String> columnValue;
		for (int i = 0; i < rowIdList.size(); i++) {
			rowId = rowIdList.get(i);
			columnIds = columnIdsList.get(i);
			columnValue = new ArrayList<String>();
			if (rowId <= sheet.getLastRowNum()) {
				row = sheet.getRow(rowId);
				if (row != null)
					for (int columnId : columnIds) {
						try {
							if (row.getCell(columnId) != null) {
								value = getCellStringValue(row.getCell(columnId));
								columnValue.add(value);
							}
						} catch (Exception e) {
						}
					}
			}
			if (!columnValue.isEmpty())
				dataRead.add(columnValue);
		}
		return dataRead;
	}

	/**
	 * Return the cell value as string. Cell with formula is evaluated.
	 * 
	 * @param cell
	 *            The cell
	 * @return Return the cell value
	 */
	public String getCellStringValue(XSSFCell cell) {
		String cellValue = "";
		switch (evaluator.evaluateInCell(cell).getCellType()) {
			case Cell.CELL_TYPE_BOOLEAN:
				cellValue = cell.getBooleanCellValue() + "";
				break;
			case Cell.CELL_TYPE_NUMERIC:
				cellValue = cell.getNumericCellValue() + "";
				break;
			case Cell.CELL_TYPE_STRING:
				cellValue = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_BLANK:
				break;
			case Cell.CELL_TYPE_ERROR:
				System.out.println(cell.getErrorCellValue());
				break;

			// CELL_TYPE_FORMULA will never occur
			case Cell.CELL_TYPE_FORMULA:
				break;
		}
		return cellValue;
	}

	/**
	 * Return the cell value as string. Cell with formula is evaluated.
	 * 
	 * @param cell
	 *            The cell
	 * @return Return the cell value
	 */
	public long getCellLongValue(XSSFCell cell) {
		long cellValue = 0;
		switch (evaluator.evaluateInCell(cell).getCellType()) {
			case Cell.CELL_TYPE_BOOLEAN:
				break;
			case Cell.CELL_TYPE_NUMERIC:
				cellValue = (long) cell.getNumericCellValue();
				break;
			case Cell.CELL_TYPE_STRING:
				cellValue = getLongValue(cell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_BLANK:
				break;
			case Cell.CELL_TYPE_ERROR:
				System.out.println(cell.getErrorCellValue());
				break;

				// CELL_TYPE_FORMULA will never occur
			case Cell.CELL_TYPE_FORMULA:
				break;
		}
		return cellValue;
	}

	/**
	 * Get the workbook file
	 * 
	 * @return Return the workbook file.
	 */
	public File getWorkbookFile() {
		return workbookFile;
	}

	/**
	 * Set the workbook file
	 * 
	 * @param workbookFile
	 *            the workbook file
	 */
	public void setWorkbookFile(File workbookFile) {
		close();
		this.workbookFile = workbookFile;
		sheet = null;
	}

	/**
	 * set the workbook file path
	 * 
	 * @param workbookFilePath
	 *            the workbook file path
	 */
	public void setWorbookFilePath(String workbookFilePath) {
		close();
		workbookFile = new File(workbookFilePath);
		sheet = null;
	}

	public static void main(String[] args) {
		String path = "Application_data/Kindom/Bacteria/Cyanobacteria/Oscillatoriophycideae/Acaryochloris_marina_MBIC11017.xlsx";
		ExcelFileReader excelFileReader = new ExcelFileReader(path);
		try {
			excelFileReader.open(SUM_SHEET_NAME);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TreeMap<String, String> map = excelFileReader.readLinesFromColumn(ADDITIONAL_DATA_ROWS, 3);
		for (Entry<String, String> entry : map.entrySet())
			System.out.println(entry.getKey() + " -> " + entry.getValue());

		excelFileReader.close();
	}
}
