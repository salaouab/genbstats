package fr.unistra.controller.dataFetchingHandler;

import static fr.unistra.utils.ExcelFileUtils.getLastModificationDate;
import static fr.unistra.utils.MainConstants.*;

import java.io.IOException;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.algosome.eutils.EntrezSearch;

import fr.unistra.controller.dataParser.CodingSequenceParser;
import fr.unistra.controller.dataParser.OrganismInfosParser;
import fr.unistra.model.beans.Organism;
import fr.unistra.model.beans.OrganismData;
import fr.unistra.model.excelFileHandler.ExcelFileWritter;

/**
 * Fetches data from ncbi databases by organism name as input and writes them in excel file
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class NcbiDataFetchingCdsForStats extends NcbiDataFetching {
	protected CodingSequenceParser	codingSequenceParser;
	protected ExcelFileWritter	   excelFileWritter;

	/**
	 * Initilize {@link EntrezSearch} , {@link ExcelFileWritter}, {@link OrganismInfosParser} ,
	 * {@link CodingSequenceParser} and the , {@link OrganismData}
	 */
	public NcbiDataFetchingCdsForStats() {
		super();
		codingSequenceParser = new CodingSequenceParser(organismData);
		excelFileWritter = new ExcelFileWritter();
		fetchDataFormat = CODING_SEQUENCE_IN_FASTA;
		organismInfosParser.setRootName(KINGDOM);
	}

	/**
	 * Constructor with organism named informed
	 * 
	 * @param organismName : the organism name
	 * @see #NcbicDataFetching()
	 */
	public NcbiDataFetchingCdsForStats(String organismName) {
		this();
		setOrganismName(organismName);
	}

	/**
	 * Constructor with the {@link Organism} informed
	 * 
	 * @param organism : the {@link Organism} information
	 */
	public NcbiDataFetchingCdsForStats(Organism organism) {
		this();
		setOrganism(organism);
	}

	/**
	 * make query based on {@link Organism} information, fetch each of its data and writes it in
	 * excel file. Data that are not updated are escaped.
	 */
	@Override
	public void fecthData() {
		super.fecthData();
		excelFileWritter.save();
	}

	@Override
	public void fetchAndSaveData(String genomeId) {
		// Get the organism cds.
		fetchCodingSequenceData();
		// return if there is no sequence to compute
		if (organismData.getNumberOfSequence() == 0 || codingSequenceParser.isErrorFound())
			return;

		// Compute the stats
		// organismData.computeCodingSequenceStat();
		// organismData.addNumberOfInvalidCds(codingSequenceParser.getNumberOfInvalidCds());
		// save the workbook if we are dealing with new organism, for a new
		// one opening
		if (!excelFileWritter.isPreviousOrganism(organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH)))
			excelFileWritter.save();
		// write the organism data to excel file
		excelFileWritter.setOrganismData(organismData);
		try {
			if (excelFileWritter.write() && PRINT_LOGS)
				System.out.println("  Ecris avec Succes - " + genomeId + " - " + organism.getName() + " --> "
				                + organismData.getOrganismInfosMap().get(ORGANISM_SHEET_NAME));
		} catch (InvalidFormatException | IOException exception) {
			if (DEBUG)
				exception.printStackTrace();
		}
	}

	/**
	 * Fetching coding sequence by retrieving data in fastat format
	 */
	protected void fetchCodingSequenceData() {
		int tryAgain = 0;
		boolean querySuccess = true;
		initFetchEngine(CODING_SEQUENCE_IN_FASTA);

		do {
			try {
				entrezFetch.doQuery(codingSequenceParser);
				querySuccess = true;
			} catch (IOException exception) {
				if (DEBUG)
					exception.printStackTrace();
				tryAgain++;
				querySuccess = false;
			}
		} while (!querySuccess && tryAgain < MAX_TRIAL);
	}

	/**
	 * Check if the file being download exists in the local disc and has been updated
	 * 
	 * @return Return <b>true</b> if the file being downloaded exists and an update occurred,
	 *         <b>false</b> otherwise
	 */
	@Override
	@SuppressWarnings("deprecation")
	protected boolean fileExistsAndIsNewer() {
		String oldFilePath = organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH);
		String sheetName = organismData.getOrganismInfosMap().get(ORGANISM_SHEET_NAME);
		// Date stored in file
		Date oldDate = getLastModificationDate(oldFilePath, sheetName);
		// Date newly Fetch
		try {
			Date newDate = new Date(organismData.getOrganismInfosMap().get(LAST_MODIFICATION_DATE));
			// check if an update occurred
			return newDate.after(oldDate);
		} catch (Exception e) {}
		return false;
	}

	public static void main(String[] args) {
		// Création de l'organisme
		Organism organism = new Organism("Oreochromis niloticus", "PRJNA72943");
		organism.addId("NC_013663");
		organism.setGroup("Animals");
		organism.setSubGroup("Fishes");
		NcbiDataFetchingCdsForStats dataFetchingEngine = new NcbiDataFetchingCdsForStats(organism);
		// Téléchargement des donnéds
		dataFetchingEngine.fecthData();
	}

}
