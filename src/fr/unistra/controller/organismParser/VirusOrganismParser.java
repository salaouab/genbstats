package fr.unistra.controller.organismParser;

import java.util.ArrayList;

import fr.unistra.model.beans.Organism;

/**
 * Parses Virus {@link Organism}s name and associated bioProject
 * 
 * @see OrganismParser
 * @author Abdoul-Djawadou SALAOU
 */
public class VirusOrganismParser extends OrganismParser {
	private int	numberOfSegmentsIndex	= -1;
	private int	numberOfRepliconsIndex	= -1;

	/**
	 * Default constructor
	 */
	public VirusOrganismParser() {
		this("");
	}

	/**
	 * Constructor with file path informed
	 * 
	 * @param filePath
	 *            : organisms file path
	 */
	public VirusOrganismParser(String filePath) {
		super(filePath);
		organismUrl = URL_BASE + "&orgn=&report=viruses&status=50|40|30|20|&host=All&group=--%20All%20Viruses%20--&subgroup=--%20All%20Viruses%20--";
		kingdomName = "Viruses";
	}

	/**
	 * Check wether to save this {@link Organism} or not.
	 * 
	 * @param line
	 *            : holds organism information
	 * @return Return <b>true</b> if the organism worth saving, <b>false</b>
	 *         otherwise
	 * @see OrganismParser#isThisOrganismWorthSaving(String)
	 */
	@Override
	protected boolean isThisOrganismWorthSaving(String line) {
		fieldsData = line.split(FIELD_SEPARATOR);
		try {
			if (useRepliconsField)
				return !fieldsData[numberOfRepliconsIndex].trim().matches("-");

			if (!fieldsData[numberOfSegmentsIndex].trim().matches("-"))
				return true;
		} catch (Exception exception) {
		}
		return false;
	}

	/**
	 * Extract some organism spécific information indexes
	 * 
	 * @param line
	 *            : holds the organism information
	 * @return Return <b>true</b> if it the extraction succeded, <b>false</b>
	 *         otherwise
	 * @see OrganismParser#extractIndexes(String)
	 */
	@Override
	protected boolean extractIndexes(String line) {

		if (super.extractIndexes(line)) {
			numberOfSegmentsIndex = getNumberOfSegmentsIndex(line);
			return true;
		}

		return false;
	}

	@Override
	protected boolean extractGenomeIds() {
		if (useRepliconsField)
			genomeIdIndex = numberOfRepliconsIndex;
		else
			genomeIdIndex = numberOfSegmentsIndex;

		return true;
	}

	/**
	 * Get the index of field giving the number of segments
	 * 
	 * @param line
	 *            : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	private int getNumberOfSegmentsIndex(String line) {
		return getTermIndex(line, ".*Segmemt.");
	}

	public static void main(String[] args) {
		ArrayList<Organism> list = new VirusOrganismParser().loadOrganismsByURL();
		int i = 0;
		System.out.println("===>> Total organism  " + list.size() + "\n");
		for (Organism organism : list) {
			System.out.println(organism);
			if (i++ > 20)
				break;
		}
	}
}
