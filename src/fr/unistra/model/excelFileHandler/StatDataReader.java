package fr.unistra.model.excelFileHandler;

import static fr.unistra.utils.ExcelFileUtils.ADDITIONAL_DATA_COLUMN;
import static fr.unistra.utils.ExcelFileUtils.NUMBER_OF_INVALID_CDS_ROW;
import static fr.unistra.utils.ExcelFileUtils.NUMBER_OF_SEQUENCE_ROW;
import static fr.unistra.utils.ExcelFileUtils.SUM_SHEET_NAME;
import static fr.unistra.utils.MainConstants.UNKNOWN;
import static fr.unistra.utils.MainConstants.containsTerm;
import static fr.unistra.utils.MainConstants.genomeType;
import static fr.unistra.utils.MainConstants.normalise;

import java.io.File;
import java.util.TreeMap;

import org.apache.poi.xssf.usermodel.XSSFRow;

/**
 * This class handle excel file reading plus stats value addition.
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class StatDataReader extends ExcelFileReader {
	/**
	 * <Genome , GenomeData> with<br>
	 * Genome= Chromosom, plasmid,...<br>
	 * GenomeData = Ph 0 |Frep Ph0| ... |Pref Ph1|Pref Ph2
	 */
	private TreeMap<String, TreeMap<String, long[]>>	organismSumValueMap	= new TreeMap<String, TreeMap<String, long[]>>();
	private TreeMap<String, long[]>	                 organismInfosMap	    = new TreeMap<String, long[]>(String.CASE_INSENSITIVE_ORDER);
	private TreeMap<String, long[]>	                 sheetMap;
	private long[]	                                 additionalValues;
	/** Is organism file being parsed ? */
	public static final int	                         CODON_COLUMN	        = 0;
	public static final int	                         PHASE_0_COLUMN	        = 1;
	public static final int	                         PHASE_1_COLUMN	        = 3;
	public static final int	                         PHASE_2_COLUMN	        = 5;
	public static final int	                         PHASE_PREF_COLUMN	    = PHASE_2_COLUMN + 2;

	/**
	 * Default Constructor
	 */
	public StatDataReader() {}

	/**
	 * Constructor with the workbook {@link File} submit. <br>
	 * Note that the workbook is not yet open. You have to specify it with the {@link #open()}
	 * method. Also do not forget to {@link #close()} it after you're done, for memory leak
	 * avoiding!
	 * 
	 * @param workbookFile the workbook file
	 */
	public StatDataReader(File workbookFile) {
		super(workbookFile);
	}

	/**
	 * Constructor with the workbook {@link File}path submit <br>
	 * Note that the workbook is not yet open. You have to specify it with the {@link #open()}
	 * method. Also do not forget to {@link #close()} it after you're done, for memory leak
	 * avoiding!
	 * 
	 * @param workbookFilePath workbook file path
	 */
	public StatDataReader(String workbookFilePath) {
		super(workbookFilePath);
	}

	/**
	 * Parse current workbook sheets for stats computation
	 * 
	 * @param isOrganismData True means the workbook contains organism level data, otherwise it
	 *            contains subgroup, group or kingdom data
	 */
	public void sumStatsData() {
		int numberOfSheet = workbook.getNumberOfSheets();
		String sheetName;
		String genomicType;

		for (int sheetIndex = 0; sheetIndex < numberOfSheet; sheetIndex++) {
			sheetName = workbook.getSheetName(sheetIndex);
			if (sheetName.equals(SUM_SHEET_NAME))
				continue;

			genomicType = getGenomeType(sheetName);
			sheetMap = organismSumValueMap.get(genomicType);
			if (sheetMap == null) {
				sheetMap = new TreeMap<String, long[]>(String.CASE_INSENSITIVE_ORDER);
				initStatMap(sheetMap);
				organismSumValueMap.put(genomicType, sheetMap);
				organismInfosMap.put(genomicType, new long[2]);
			}
			additionalValues = organismInfosMap.get(genomicType);
			sheet = workbook.getSheetAt(sheetIndex);

			readSheetData();
		}
	}

	/**
	 * Computes current sheet data stats
	 */
	private void readSheetData() {
		String codon;
		XSSFRow row;
		long[] phasesValue;
		for (int rowId = 1; rowId < 65; rowId++) {
			try {
				row = sheet.getRow(rowId);
				codon = getCellStringValue(row.getCell(CODON_COLUMN)).trim();
				phasesValue = sheetMap.get(codon);
				phasesValue[0] += getCellLongValue(row.getCell(PHASE_0_COLUMN));
				phasesValue[1] += getCellLongValue(row.getCell(PHASE_1_COLUMN));
				phasesValue[2] += getCellLongValue(row.getCell(PHASE_2_COLUMN));

				phasesValue[3] += getCellLongValue(row.getCell(PHASE_PREF_COLUMN));
				phasesValue[4] += getCellLongValue(row.getCell(PHASE_PREF_COLUMN + 1));
				phasesValue[5] += getCellLongValue(row.getCell(PHASE_PREF_COLUMN + 2));

				switch (rowId) {
					case NUMBER_OF_SEQUENCE_ROW:
						additionalValues[0] += getCellLongValue(row.getCell(ADDITIONAL_DATA_COLUMN + 1));
						break;
					case NUMBER_OF_INVALID_CDS_ROW:
						additionalValues[1] += getCellLongValue(row.getCell(ADDITIONAL_DATA_COLUMN + 1));
						break;
				}

			} catch (Exception e) {}
		}
	}

	/**
	 * Get the genomic type (chromosom, plasmid, ...) of the structure.
	 * 
	 * @param structure Structure to check
	 * @return Return the type of the structure
	 */
	public static String getGenomeType(String structure) {
		for (String type : genomeType)
			if (containsTerm(structure, normalise(type)))
				return normalise(type);

		return UNKNOWN;
	}

	/**
	 * Initialize this map entry values. The map is templated as <Codon, CodonData> with <br>
	 * Codon in AAA,AAC, ..., TTT <br>
	 * CodonData = Ph 0 | Ph1 | Ph2 | Pref Ph0 | Pref Ph1 | Pref Ph2
	 * 
	 * @param statMap Map to initialize
	 */
	public void initStatMap(TreeMap<String, long[]> statMap) {
		String[] letterArray = { "A", "C", "G", "T" };
		for (String letter1 : letterArray)
			for (String letter2 : letterArray)
				for (String letter3 : letterArray)
					statMap.put(letter1 + letter2 + letter3, new long[6]);
	}

	public TreeMap<String, TreeMap<String, long[]>> getOrganismSumValueMap() {
		return organismSumValueMap;
	}

	public void setOrganismSumValueMap(TreeMap<String, TreeMap<String, long[]>> organismSumValueMap) {
		this.organismSumValueMap = organismSumValueMap;
	}

	public long[] getAdditionalValues(String key) {
		return organismInfosMap.get(key);
	}

	public void clearAll() {
		organismSumValueMap.clear();
		organismInfosMap.clear();
		close();
	}
}
