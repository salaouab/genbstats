package fr.unistra.utils;

import java.io.*;

import com.algosome.eutils.*;
import com.algosome.eutils.io.InputStreamParser;

import fr.unistra.controller.dataParser.ESearchIdParser;

public class NcbiQueryTest {

	public static void main(String[] args) throws IOException {
		// String url =
		// "http://www.ncbi.nlm.nih.gov/genomes/Genome2BE/genome2srv.cgi?action=download&orgn=&report=proks&status=50|40|30|20|&group=--%20All%20Prokaryotes%20--&subgroup=--%20All%20Prokaryotes%20--";
		// URL entrez = new URL(url);
		// URLConnection conn = entrez.openConnection();
		// conn.setConnectTimeout(30 * 1000);
		//
		// ArrayList<Organism> list = new
		// ProkaryoteOrganismParser().loadOrganisms(conn.getInputStream());
		// int i = 0;
		// System.out.println("===>> Total organism  " + list.size() + "\n");
		// for (Organism organism : list) {
		// System.out.println(organism);
		// if (i++ > 20)
		// break;
		// }
		testDownloadNcbi();
	}

	public static void testDownloadNcbi() {
		// TODO Auto-generated method stub
		EntrezSearch search = new EntrezSearch();

		// search.setDatabase(EntrezSearch.DB_GENOME);
		search.setDatabase(Entrez.DB_NUCLEOTIDE);

		// search.setTerm("(Acaryochloris marina[organism]) AND cds[feature key] AND refseq[Filter]");

		// search.setTerm("(Acaryochloris marina[orgn]  AND Refseq[filter] ");
		search.setTerm("'Nostoc azollae' 0708[orgn] AND cds[fkey] AND Refseq[filter]");
		// search.setTerm("NC_009926");
		// // nuccore
		// assembly[Filter]");

		EntrezSearch search2 = new EntrezSearch();
		search2.setDatabase(Entrez.DB_NUCLEOTIDE);
		search2.setIds("NZ_CP009483.1");
		// search.setMaxRetrieval(20);
		// search.setRetStart(0);
		// search.setQueryMethod(false);
		search.setUseHistory("y");
		try {
			// search.setRetStart(20);
			search.doQuery(new ESearchIdParser());
			// search.doQuery();
			// search.doQuery(new Reader());
			Thread.sleep(1000);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			// System.err.println(e1.getMessage().toUpperCase().contains("NO SEARCH ID'S WERE FOUND"));
			e1.printStackTrace();
		}
		// if (true)
		// return;
		// search.setOutputFile("resultat");

		System.out.println("++++++++\n");
		System.out.println(search.getIds());
		System.out.println("+++++++++++++++++\n");
		// search2.setQueryMethod(false);
		EntrezFetch fetch = new EntrezFetch(search2);
		// fetch.setQueryMethod(false);
		fetch.setEmail(MainConstants.EMAIL);
		// fetch.setParameter("strand", "1");
		// fetch.setParameter("seq_start", "1627");
		// fetch.setParameter("seq_stop", "2319");
		// fetch.setParameter("complexity", "1");
		// fetch.setParameter("feature", "cds");

		// fetch.setRetMode("");
		// {"Default (native)", "Fasta Sequence View (fasta)",
		// "GenBank Report (gb)",
		// "GenPept Report (gp)",
		// "Genbank With Parts (gbwithparts)", "Nucleotide INSDSeq (gbc)",
		// "Protein INSDSeq (gbp)",
		// "EST Report (est)", "GSS Report (gss)",
		// "To Convert GIS to seqids (seqids)",
		// "To convert GIS to accessions (acc)", "Feature Table Report (ft)"};
		fetch.setRetMode("text");
		// fetch.setRetType("fasta_cds_na");// Genbank
		// fetch.setRetType("gp");// Genbank
		// fetch.setOutputFile("result.gb");
		// http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&id=21614549&strand=1&seq_start=83&seq_stop=1945&rettype=fasta
		// fetch.parseFrom(200);
		// fetch.parseTo(300);
		// BufferedReader br;
		try {
			fetch.setRetType("fasta_cds_na");
			fetch.doQuery(new Reader());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

class Reader implements InputStreamParser {

	@Override
	public void parseFrom(int start) {}// do nothing

	@Override
	public void parseTo(int end) {}// do nothing

	@Override
	public void parseInput(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		// PrintWriter writer = new PrintWriter("resultat.txt", "UTF-8");
		String line = null;
		// if (true)
		// return;
		while ((line = br.readLine()) != null) {

			// if (line.toLowerCase().startsWith("<id>"))

			System.out.println(line);
			// writer.println(line);
			// if (!line.isEmpty() && line.trim().charAt(0) == '>')
			// numberOfCDS++;
		}
		br.close();

	}

}
