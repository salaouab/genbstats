package fr.unistra.controller.dataParser;

import static fr.unistra.utils.MainConstants.*;

import java.io.*;
import java.util.TreeMap;

import com.algosome.eutils.io.InputStreamParser;

import fr.unistra.model.beans.OrganismData;

/**
 * Handles sequence data saving. Data are stored in txt files
 * 
 * @author Abdoul-Djawadou SALAOU
 *
 */
public class SequenceSaver implements InputStreamParser {
	private OrganismData	organismData;
	private File			organismFile;
	private PrintWriter		writer;
	private String[]		fieldToSave		= { LOCUS, DEFINITION, ACCESSION, ORGANISM, REFERENCE, TITLE };
	private boolean			downloadCdsOnly	= true;

	/**
	 * Constructor with the {@link OrganismData} informed
	 * 
	 * @param organismData
	 *            the {@link OrganismData}
	 */
	public SequenceSaver(OrganismData organismData) {
		this.organismData = organismData;
	}

	@Override
	public void parseInput(InputStream inputStream) throws IOException {
		try {

			organismFile = new File(organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH));
			if (!organismFile.exists())
				organismFile.getParentFile().mkdirs();

			InputStreamReader streamReader = new InputStreamReader(inputStream);
			BufferedReader bufferedReader = new BufferedReader(streamReader);
			writer = new PrintWriter(organismFile);
			String line = null;

			if (downloadCdsOnly)
				writeHeader();

			while ((line = bufferedReader.readLine()) != null) {
				writer.println(line);
			}
			writer.flush();
			writer.close();
			bufferedReader.close();
			streamReader.close();
			inputStream.close();
		} catch (Exception exception) {
			inputStream.close();
		}
	}

	/**
	 * Handles data writting on the local disc.
	 */
	public void writeHeader() {
		TreeMap<String, String> infosMap = organismData.getOrganismInfosMap();
		for (String field : fieldToSave) {
			if (infosMap.containsKey(field))
				switch (field) {
					case DEFINITION:
						writer.print(DEFINITION.toUpperCase() + "\t");
						break;
					case ACCESSION:
						writer.print(ACCESSION.toUpperCase() + "\t");
						break;
					case TITLE:
						writer.print(TITLE.toUpperCase() + "\t\t");
						break;
					case ORGANISM:
						try {
							int index = infosMap.get(field).trim().indexOf(ORGANISM.toUpperCase());
							String[] lines = infosMap.get(field).substring(index).split("\n");
							writer.println(ORGANISM.toUpperCase() + "\t" + lines[0].trim());
							for (int i = 1; i < lines.length; i++) {
								writer.println("\t\t\t" + lines[i].trim());
							}
						} catch (Exception exception) {
							writer.println(infosMap.get(field));
						}
						continue;
				}

			writer.println(infosMap.get(field));
		}
		writer.println("\n");
	}

	public OrganismData getOrganismData() {
		return organismData;
	}

	public void setOrganismData(OrganismData organismData) {
		this.organismData = organismData;
	}

	public boolean isDownloadCdsOnly() {
		return downloadCdsOnly;
	}

	public void setDownloadCdsOnly(boolean downloadCdsOnly) {
		this.downloadCdsOnly = downloadCdsOnly;
	}

	@Override
	public void parseFrom(int start) {
	}

	@Override
	public void parseTo(int end) {
	}

}
