package fr.unistra.view.browserHandler;

import static fr.unistra.utils.FileUtil.ORGANISM_BASE_DIRECTORY;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.*;

import fr.unistra.model.beans.FileBean;
import fr.unistra.view.language.Messages;

/**
 * The panel handling downloaded files browsing. You can double click or press Enter key to open
 * file. File opening is handling by the operating system default associated application.<br/>
 * Note that this browser offers searching capabilities
 * @author Abdoul-djawadou SALAOU
 */
public class BrowserPanel extends JPanel {
	private static final long	    serialVersionUID	  = 1L;
	private JTree	                browserTree;
	private DefaultTreeModel	    browserModel;
	private DefaultTreeModel	    searchModel;
	private File	                rootFile;
	private FileFinder	            fileFinder;
	private JTextField	            searchField;
	private boolean	                switchModel	          = true;
	private boolean	                treeUpdatingisEnabled	= true;
	private static Comparator<File>	fileComparator;

	public BrowserPanel() {

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(Color.DARK_GRAY);

		fileComparator = new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				if (o1.isDirectory())
					return o2.isDirectory() ? o1.compareTo(o2) : -1;
				else if (o2.isDirectory())
					return 1;

				return o1.compareTo(o2);
			}
		};

		searchField = new JTextField();
		searchField.setDragEnabled(true);
		searchField.setFont(new Font("SansSerif", Font.BOLD, 15));
		searchField.setMaximumSize(new Dimension(Integer.MAX_VALUE, searchField.getPreferredSize().height));
		setBorder(new EmptyBorder(5, 10, 5, 10));
		add(searchField);
		add(Box.createVerticalStrut(10));

		rootFile = new File(ORGANISM_BASE_DIRECTORY);
		if (!rootFile.exists())
			rootFile.mkdirs();
		searchModel = new DefaultTreeModel(null);
		fileFinder = new FileFinder(rootFile, searchModel);
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(new FileBean(rootFile));
		browserModel = new DefaultTreeModel(rootNode);

		browserTree = new JTree(browserModel);
		browserTree.setCellRenderer(new BrowserTreeCellRender());
		parseNode(rootNode, rootFile);
		browserTree.setBackground(Color.DARK_GRAY);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(browserTree);
		browserTree.setBackground(Color.DARK_GRAY);
		add(scrollPane);

		Color borderColor = new Color(72, 209, 204);
		LineBorder lineBorder = new LineBorder(borderColor, 4, false);
		scrollPane.setBorder(lineBorder);
		browserTree.setBorder(new EmptyBorder(5, 10, 5, 10));
		searchField.setBorder(lineBorder);
		searchField.setMargin(new Insets(3, 20, 3, 20));
		browserTree.expandRow(0);
		browserTree.addMouseListener(new BrowserTreeMouseListener());
		browserTree.addTreeWillExpandListener(new BrowserTreeExpansionListener());
		browserTree.addKeyListener(new BrowserTreeKeyListener());
		searchField.getDocument().addDocumentListener(new SearchDocumentListener());
		resetText();
	}

	public void resetText() {
		searchField.setToolTipText(Messages.getString("searchToolTip"));
	}

	/** Handles trees expansion and collapses */
	class BrowserTreeExpansionListener implements TreeWillExpandListener {
		@Override
		public void treeWillCollapse(TreeExpansionEvent arg0) throws ExpandVetoException {}

		@Override
		public void treeWillExpand(TreeExpansionEvent arg0) throws ExpandVetoException {
			updateTree();
		}

	}

	/** Update Browser tree for new files */
	public void updateTree() {
		if (!treeUpdatingisEnabled)
			return;
		DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) browserTree.getLastSelectedPathComponent();
		DefaultMutableTreeNode newNode;
		if (currentNode == null)
			return;
		FileBean currentNodeFile = (FileBean) currentNode.getUserObject();
		if (!currentNodeFile.getFile().isDirectory())
			return;
		for (File file : currentNodeFile.getFile().listFiles()) {
			if (!containsChild(currentNode, file)) {
				newNode = new DefaultMutableTreeNode(new FileBean(file));
				browserModel.insertNodeInto(newNode, currentNode, currentNode.getChildCount());
				parseNode(newNode, file);
			}
		}
	}

	/** Mouse double click to open file/folder */
	class BrowserTreeMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			// super.mouseClicked(e);
			if (e.getClickCount() == 1)
				return;
			openFile();
			updateTree();
		}
	}

	/** Enter key on the tree to open a file/folder */
	class BrowserTreeKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				openFile();
			}
		}
	}

	/**
	 * Open file selected on the browser tree
	 */
	private void openFile() {
		DefaultMutableTreeNode currentNode;
		TreePath[] selectedPath = browserTree.getSelectionPaths();
		if (selectedPath == null)
			return;
		for (TreePath path : selectedPath) {
			currentNode = (DefaultMutableTreeNode) path.getLastPathComponent();
			try {
				Desktop.getDesktop().open(((FileBean) currentNode.getUserObject()).getFile());
			} catch (Exception e) {}
		}

	}

	/**
	 * Check wether a tree node contains already a given child
	 * @param node The node within to check
	 * @param child The child to check
	 * @return return true if the node contains the child, false otherwise
	 */
	public static boolean containsChild(DefaultMutableTreeNode node, File child) {

		DefaultMutableTreeNode currentChild;
		for (int childIndex = 0; childIndex < node.getChildCount(); childIndex++) {
			currentChild = (DefaultMutableTreeNode) node.getChildAt(childIndex);
			if (currentChild.getUserObject().equals(child))
				return true;
		}

		return false;
	}

	/**
	 * Create and add the directory files to the hierarchical node
	 * @param parentNode The parent node
	 * @param DirFile directory to parse
	 */
	public static void parseNode(DefaultMutableTreeNode parentNode, File dirFile) {
		if (!dirFile.isDirectory())
			return;

		DefaultMutableTreeNode childNode;
		File[] dirFileList = dirFile.listFiles();
		Arrays.sort(dirFileList, fileComparator);
		for (File file : dirFileList) {
			childNode = new DefaultMutableTreeNode(new FileBean(file), file.isDirectory());
			parentNode.add(childNode);
			if (file.isDirectory())
				parseNode(childNode, file);
		}

	}

	/**
	 * Tree nodes renderer (font, icons, color) costumiser
	 */
	class BrowserTreeCellRender extends DefaultTreeCellRenderer {
		private static final long	serialVersionUID	= 1L;

		public BrowserTreeCellRender() {
			setForeground(Color.WHITE);
			setBackgroundNonSelectionColor(Color.DARK_GRAY);
			setBackgroundSelectionColor(Color.BLACK);
			setFont(new Font("SansSerif", Font.BOLD, 14));
		}

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row,
				boolean hasFocus) {
			super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

			if (value == null || !(value instanceof DefaultMutableTreeNode))
				return this;

			try {
				FileBean file = (FileBean) ((DefaultMutableTreeNode) value).getUserObject();
				setIcon(FileSystemView.getFileSystemView().getSystemIcon(file.getFile()));
			} catch (Exception e) {}
			if (hasFocus)
				setForeground(Color.GREEN);
			else
				setForeground(Color.WHITE);

			return this;
		}
	}

	/**
	 * Search field change listener
	 */
	class SearchDocumentListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent e) {}

		@Override
		public void removeUpdate(DocumentEvent e) {
			processSearching();
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			processSearching();
		}
	}

	/**
	 * Process searching of term typed in the search field
	 */
	private void processSearching() {
		String searchTerm = searchField.getText().trim();
		int currentLength = searchTerm.length();
		if (currentLength > 2) {
			fileFinder.startSearching(searchTerm);
			if (switchModel) {
				browserTree.setModel(searchModel);
				switchModel = false;
				treeUpdatingisEnabled = false;
			}
			browserTree.expandRow(0);
		}
		else if (!switchModel) {
			browserTree.setModel(browserModel);
			switchModel = true;
			treeUpdatingisEnabled = true;
		}
	}
}
