package fr.unistra.controller.organismParser;

import java.util.ArrayList;

import fr.unistra.model.beans.Organism;

/**
 * Parses Prokaryote {@link Organism}s name and associated bioProject
 * @see OrganismParser
 * @author Abdoul-Djawadou SALAOU
 */
public class ProkaryoteOrganismParser extends OrganismParser {
	private int	chromosomesAccessionIndex	= -1;
	private int	plasmidsAccessionIndex	  = -1;

	/**
	 * Default constructor
	 */
	public ProkaryoteOrganismParser() {
		this("");
	}

	/**
	 * Constructor with file path informed
	 * @param filePath : organisms file path
	 */
	public ProkaryoteOrganismParser(String filePath) {
		super(filePath);
		organismUrl = URL_BASE + "&orgn=&report=proks&status=50|40|30|20|&group=--%20All%20Prokaryotes%20--&subgroup=--%20All%20Prokaryotes%20--";
		kingdomName = "Prokaryotes";
	}

	/**
	 * Check wether to save this {@link Organism} or not.
	 * @param line : holds organism information
	 * @return Return <b>true</b> if the organism worth saving, <b>false</b> otherwise
	 * @see OrganismParser#isThisOrganismWorthSaving(String)
	 */
	@Override
	protected boolean isThisOrganismWorthSaving(String line) {
		fieldsData = line.split(FIELD_SEPARATOR);
		try {
			if (useRepliconsField)
				return !fieldsData[numberOfRepliconsIndex].trim().matches("-");

			if (!fieldsData[chromosomesAccessionIndex].trim().matches("-"))
				return true;
			if (!fieldsData[plasmidsAccessionIndex].trim().matches("-"))
				return true;
		} catch (Exception exception) {}
		return false;
	}

	/**
	 * Extract some organism spécific information indexes
	 * @param line : holds the organism information
	 * @return Return <b>true</b> if it the extraction succeded, <b>false</b> otherwise
	 * @see OrganismParser#extractIndexes(String)
	 */
	@Override
	protected boolean extractIndexes(String line) {

		if (super.extractIndexes(line)) {
			chromosomesAccessionIndex = getchromosomesAccessionIndex(line);
			plasmidsAccessionIndex = getPlasmidsAccessionIndex(line);
			// numberOfRepliconsIndex = getNumberOfRepliconsIndex(line);
			return true;
		}

		return false;
	}

	/**
	 * Get the index of field giving chromosomes accession id
	 * @param line : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	private int getchromosomesAccessionIndex(String line) {
		return getTermIndex(line, ".*chromosome.*Refseq");
	}

	/**
	 * Get the index of field giving plasmids accesion id
	 * @param line : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	private int getPlasmidsAccessionIndex(String line) {
		return getTermIndex(line, ".*plasmid.*Refseq");
	}

	public static void main(String[] args) {
		// ArrayList<Organism> list = new
		// ProkaryoteOrganismParser().loadOrganismsByURL();
		OrganismParser organismParser = new ProkaryoteOrganismParser();
		ArrayList<Organism> list = organismParser.loadOrganismsByURL();
		int i = 0;
		System.out.println("===>> Total organism  " + list.size() + "\n");
		for (Organism organism : list) {
			System.out.println(organism);
			if (i++ > 20)
				break;
		}
	}
}
