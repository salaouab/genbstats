package fr.unistra.model.beans;

/**
 * Represents the coding sequence Data and associated header
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class CodingSequence {
	private String	header	 = "";
	private String	sequence	= "";

	/**
	 * Default constructor
	 */
	public CodingSequence() {}

	/**
	 * Constructor with sequence and herder informed
	 * 
	 * @param sequence : the coding sequence data composed of <b>A, C, G, T</b> letters
	 * @param header : Its metadata (gene name, protein name, locus, location ...)
	 */
	public CodingSequence(String sequence, String header) {
		this.sequence = sequence;
		this.header = header;
	}

	/**
	 * Return the coding sequence {@link #clone()}
	 * 
	 * @return The {@link #header}
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * Set the coding sequence {@link #header}
	 * 
	 * @param header coding sequence header
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * Return the coding {@link #sequence}
	 * 
	 * @return The {@link #sequence}
	 */
	public String getSequence() {
		return sequence;
	}

	/**
	 * Set the coding {@link #sequence}
	 * 
	 * @param sequence:
	 */
	public void setSequences(String sequence) {
		this.sequence = sequence;
	}

}
