package fr.unistra.model.beans;

import java.io.File;

/**
 * Holds file associated data for browser tree nodes
 * 
 * @author Abdoul-Djawadou SALAOU
 *
 */
public class FileBean {
	File	file;

	public FileBean(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof File)
			return file.equals(obj);
		else if (obj instanceof FileBean)
			return file.equals(((FileBean) obj).file);

		return false;
	}

	@Override
	public String toString() {
		return file.getName();
	}
}
