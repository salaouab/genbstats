package fr.unistra.model.interfaces;

/**
 * Service to be implemented by the download ending process manager
 * @author Abdoul-djawadou SALAOU
 */
public interface DownloadEndingProcessor {
	public void process();
}
