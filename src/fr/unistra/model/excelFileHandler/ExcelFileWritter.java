package fr.unistra.model.excelFileHandler;

import static fr.unistra.model.excelFileHandler.ComputeSum.*;
import static fr.unistra.utils.ExcelFileUtils.*;
import static fr.unistra.utils.MainConstants.*;

import java.awt.Color;
import java.io.*;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import fr.unistra.model.beans.OrganismData;

/**
 * This class handles organism data writting in excel file
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class ExcelFileWritter {
	private XSSFWorkbook	         workbook;
	private XSSFSheet	             sheet;
	private XSSFFont	             sheetFont;
	// Styles
	private XSSFCellStyle	         headerStyle;
	private XSSFCellStyle	         totalValueStyle;
	private XSSFCellStyle	         totalStyle;
	private XSSFCellStyle	         phaseStyle;
	private XSSFCellStyle	         phaseFreqStyle;
	private XSSFCellStyle	         triNucleotidNameStyle;
	private XSSFCellStyle	         phaseStyle1;
	private XSSFCellStyle	         phaseFreqStyle1;
	private XSSFCellStyle	         triNucleotidNameStyle1;
	private XSSFCellStyle	         phaseStyle2;
	private XSSFCellStyle	         phaseFreqStyle2;
	private XSSFCellStyle	         triNucleotidNameStyle2;
	private XSSFCellStyle	         additionalDataStyle;
	private XSSFCellStyle	         additionalDataStyle2;
	private XSSFCellStyle	         prefPhaseStyle;
	private XSSFCellStyle	         prefPhaseStyle2;
	// Data
	private OrganismData	         organismData;
	private OPCPackage	             opcPackage;
	private FileInputStream	         organismFileInputStream;
	private String	                 organismFilePath	      = "";
	private File	                 organismFile;
	private int	                     additionalDataColumn	  = ADDITIONAL_DATA_COLUMN;
	// Flags
	private boolean	                 reInitHeaderSyle	      = true;
	private boolean	                 reInitDataStyle	      = true;
	private boolean	                 reInitAdditonalDataStyle	= true;
	private boolean	                 reInitTotalStyle	      = true;
	private boolean	                 isOrganismLevel	      = true;
	private TreeMap<String, Integer>	sumMap	              = new TreeMap<String, Integer>(String.CASE_INSENSITIVE_ORDER);

	/**
	 * Default constructor
	 */
	public ExcelFileWritter() {}

	/**
	 * Constructor with data level informed
	 * 
	 * @param isOrganismLevel : True if it is an organism data , false otherwise (means group,
	 *            subgroup)
	 */
	public ExcelFileWritter(boolean isOrganismLevel) {
		this.isOrganismLevel = isOrganismLevel;
	}

	/**
	 * Constructor with the organism data informed
	 * 
	 * @param organismData conatains data to write
	 */
	public ExcelFileWritter(OrganismData organismData) {
		this.organismData = organismData;
	}

	/**
	 * Writes the organism data to excel file. If the file does not exist, it will be created.<br>
	 * Note <b>this just write data into the workbook without saving it</b>. You have to explicitly
	 * save your modifications by calling {@link #save()} method.
	 * 
	 * @param writeAdditionalData : If true, writes additonal data value to file.
	 * @return Return <b>true</b> if it ends well, <b>false</b> otherwise
	 * @throws IOException
	 * @throws InvalidFormatException
	 */
	public boolean write(boolean writeAdditionalData) throws IOException, InvalidFormatException {
		if (organismData == null)
			return false;

		String organismSheetName = organismData.getOrganismInfosMap().get(ORGANISM_SHEET_NAME);
		openWorkbook();
		// get or create the sheet
		sheet = workbook.getSheet(organismSheetName);
		// if sheet already exists ,remove it
		if (sheet != null)
			workbook.removeSheetAt(workbook.getSheetIndex(sheet));

		sheet = workbook.createSheet(organismSheetName);
		// write sheet main header ( Ph0 , freq Ph0,...)
		writeHeader();
		int rowIndex = 1;
		// Write statistic data
		initDataStyles();
		for (Entry<String, long[]> entry : organismData.getCodingSequenceStatisticMap().entrySet())
			writeStatisticEntry(entry, rowIndex++);

		writeTotals();
		writeAdditionalData();
		autosizeColumns(true);
		if (workbook.getNumberOfSheets() % 50 == 0)
			saveAndWait(false, 1.3);
		return true;
	}

	/**
	 * Writes the organism stats data and its additonal data to excel file.
	 * 
	 * @see #write(boolean)
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public boolean write() throws InvalidFormatException, IOException {
		return write(true);
	}

	/**
	 * Writes the organism stats data .
	 * 
	 * @see #write(boolean)
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public boolean writeStatData() throws InvalidFormatException, IOException {
		return write(false);
	}

	private void openWorkbook() throws IOException, InvalidFormatException {
		if (workbook == null) {
			organismFilePath = organismData.getOrganismInfosMap().get(ORGANISM_FILE_PATH);
			organismFile = new File(organismFilePath);
			organismFileInputStream = null;
			opcPackage = null;
			// Check file existence and ... Open the workbook
			if (!organismFile.exists()) {
				organismFile.getParentFile().mkdirs();
				organismFile.createNewFile();
				workbook = new XSSFWorkbook();
			}
			else {
				organismFileInputStream = new FileInputStream(organismFile);
				opcPackage = OPCPackage.open(organismFileInputStream);
				workbook = new XSSFWorkbook(opcPackage);
			}
		}
	}

	/**
	 * Writes and formats satistic entry data for <br/>
	 * Nucleotid name <br/>
	 * Phase 0 and its frequency <br/>
	 * Phase 1 and its frequency <br/>
	 * Phase 2 and its frequency
	 * 
	 * @param entry : Entry data to write
	 * @param rowIndex : the index of the current row
	 * @see #updateStyles(boolean)
	 */
	private void writeStatisticEntry(Entry<String, long[]> entry, int rowIndex) {
		long[] statValues = entry.getValue();
		Row dataRow = sheet.createRow(rowIndex);
		int currentRowNumber = rowIndex + 1;
		int totalRowNumber = TOTAL_ROW + 1;
		Cell cell;

		// condition style formating
		updateStyles(rowIndex % 2 == 0);

		// Tri nucleotide name
		cell = dataRow.createCell(0);
		cell.setCellValue(entry.getKey());
		cell.setCellStyle(triNucleotidNameStyle);

		// phase 0 and its frequence
		cell = dataRow.createCell(1);
		cell.setCellValue(statValues[0]);
		cell.setCellStyle(phaseStyle);
		cell = dataRow.createCell(2);
		cell.setCellFormula("100 * B" + currentRowNumber + "/B" + totalRowNumber);
		cell.setCellStyle(phaseFreqStyle);

		// phase 1 and its frequence
		cell = dataRow.createCell(3);
		cell.setCellValue(statValues[1]);
		cell.setCellStyle(phaseStyle);
		cell = dataRow.createCell(4);
		cell.setCellFormula("100 * D" + currentRowNumber + "/D" + totalRowNumber);
		cell.setCellStyle(phaseFreqStyle);

		// phase 2 and its frequence
		cell = dataRow.createCell(5);
		cell.setCellValue(statValues[2]);
		cell.setCellStyle(phaseStyle);
		cell = dataRow.createCell(6);
		cell.setCellFormula("100 * F" + currentRowNumber + "/F" + totalRowNumber);
		cell.setCellStyle(phaseFreqStyle);

		// Add preferential phases
		if (isOrganismLevel) {
			long[] prefPhaseValues = organismData.getPrefPhaseValues(entry.getKey());
			for (int i = 0; i < 3; i++) {
				cell = dataRow.createCell(7 + i);
				cell.setCellStyle(prefPhaseStyle);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				cell.setCellValue(prefPhaseValues[i]);
			}
		}
		else {
			for (int i = 0; i < 3; i++) {
				cell = dataRow.createCell(7 + i);
				cell.setCellStyle(prefPhaseStyle);
				cell.setCellType(Cell.CELL_TYPE_STRING);
				cell.setCellValue(statValues[3 + i]);
			}
		}
	}

	/**
	 * writes and formats header title <b>{@value #STATISTIC_HEADER}</b>
	 * 
	 * @see #initHeaderStyle()
	 * @see #STATISTIC_HEADER
	 * @see #STATISTIC_HEADER2
	 */
	private void writeHeader() {
		XSSFRow headerRow = sheet.createRow(0);
		XSSFCell cell;
		initHeaderStyle();
		for (int cellIndex = 1; cellIndex < STATISTIC_HEADER.length + 1; cellIndex++) {
			cell = headerRow.createCell(cellIndex);
			cell.setCellValue(STATISTIC_HEADER[cellIndex - 1]);
			cell.setCellStyle(headerStyle);
		}

		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
		cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headerRow.createCell(0).setCellStyle(cellStyle);

	}

	/**
	 * writes Total row data with sum formulas
	 * 
	 * @see #setStyleColor(int)
	 */
	private void writeTotals() {
		XSSFRow totalsRow = sheet.createRow(TOTAL_ROW);
		XSSFCell cell;

		// Update style for total row
		initTotalStyle();
		// total Cell
		cell = totalsRow.createCell(0);
		cell.setCellValue("TOTAL");
		cell.setCellStyle(totalStyle);
		// Phase 0
		cell = totalsRow.createCell(1);
		cell.setCellFormula("SUM(B2:B65)");
		cell.setCellStyle(totalValueStyle);
		// Phase 0 Frequency
		cell = totalsRow.createCell(2);
		cell.setCellFormula("SUM(C2:C65)");
		cell.setCellStyle(totalValueStyle);
		// Phase 1
		cell = totalsRow.createCell(3);
		cell.setCellFormula("SUM(D2:D65)");
		cell.setCellStyle(totalValueStyle);
		// Phase 1 Frequency
		cell = totalsRow.createCell(4);
		cell.setCellFormula("SUM(E2:E65)");
		cell.setCellStyle(totalValueStyle);
		// Phase 2
		cell = totalsRow.createCell(5);
		cell.setCellFormula("SUM(F2:F65)");
		cell.setCellStyle(totalValueStyle);
		// Phase 2 Frequency
		cell = totalsRow.createCell(6);
		cell.setCellFormula("SUM(G2:G65)");
		cell.setCellStyle(totalValueStyle);
	}

	/**
	 * Write additional data : <br/>
	 * - Number of nucleotid <br/>
	 * - Number of valid sequence considered <br/>
	 * - Last modification date <br/>
	 * - Organism name <br/>
	 * - Organism taxonomy <br/>
	 * - Title <br/>
	 * - Definition <br/>
	 * 
	 * @see #addMergedRegion()
	 */
	private void writeAdditionalData() {
		TreeMap<String, String> organismInfosMap = organismData.getOrganismInfosMap();
		XSSFRow row;
		additionalDataColumn = ADDITIONAL_DATA_COLUMN;
		if (isOrganismLevel) {
			addMergedRegion(ADDITIONAL_DATA_ROWS, 3);
			for (int i = 0; i < ADDITIONAL_DATA.length; i++) {
				row = sheet.getRow(ADDITIONAL_DATA_ROWS[i]);
				row.getCell(additionalDataColumn).setCellValue(ADDITIONAL_DATA[i]);
				row.getCell(additionalDataColumn + 1).setCellValue(organismInfosMap.get(ADDITIONAL_DATA[i]));
			}
		}
		else {
			addMergedRegion2(ADDITIONAL_DATA_ROWS, 3);
			for (int i = 2; i < 4; i++) {
				row = sheet.getRow(ADDITIONAL_DATA_ROWS[i]);
				row.getCell(additionalDataColumn).setCellValue(ADDITIONAL_DATA[i]);
				row.getCell(additionalDataColumn + 1).setCellValue(organismInfosMap.get(ADDITIONAL_DATA[i]));
			}
		}
	}

	private void writeSumAdditionalData() {
		addMergedRegion(SUM_ADDITIONAL_DATA_ROWS, 2);
		TreeMap<String, String> organismInfosMap = organismData.getOrganismInfosMap();
		XSSFRow row;

		for (int i = 0; i < SUM_ADDITIONAL_DATA.length; i++) {
			row = sheet.getRow(SUM_ADDITIONAL_DATA_ROWS[i]);
			row.getCell(additionalDataColumn).setCellValue(SUM_ADDITIONAL_DATA[i]);
			row.getCell(additionalDataColumn + 1).setCellValue(organismInfosMap.get(SUM_ADDITIONAL_DATA[i]));
		}

	}

	/**
	 * Merge some cells for additional data and set the reginons style.
	 * 
	 * @see #additionalDataStyle
	 */
	private void addMergedRegion(int[] additional_data_rows, int doubleSizeLimit) {
		int firstC = additionalDataColumn + 1;
		int lastC = firstC + ADDITIONAL_DATA_WIDTH;
		initAdditonalDataStyle();
		CellRangeAddress region;
		int data_size = additional_data_rows.length;

		for (int i = 0; i < data_size; i++) {
			sheet.getRow(additional_data_rows[i]).createCell(additionalDataColumn).setCellStyle(additionalDataStyle);
			sheet.getRow(additional_data_rows[i]).createCell(additionalDataColumn + 1).setCellStyle(additionalDataStyle2);
			if (i < data_size - doubleSizeLimit)
				region = new CellRangeAddress(additional_data_rows[i], additional_data_rows[i], firstC, lastC);
			else
				region = new CellRangeAddress(additional_data_rows[i], additional_data_rows[i] + 2, firstC, lastC);
			sheet.addMergedRegion(region);
		}
	}

	private void addMergedRegion2(int[] additional_data_rows, int doubleSizeLimit) {
		int firstC = additionalDataColumn + 1;
		int lastC = firstC + 1;
		initAdditonalDataStyle();
		CellRangeAddress region;

		for (int i = 2; i < 4; i++) {
			sheet.getRow(additional_data_rows[i]).createCell(additionalDataColumn).setCellStyle(additionalDataStyle);
			sheet.getRow(additional_data_rows[i]).createCell(additionalDataColumn + 1).setCellStyle(additionalDataStyle2);
			region = new CellRangeAddress(additional_data_rows[i], additional_data_rows[i], firstC, lastC);
			sheet.addMergedRegion(region);
		}
	}

	/**
	 * Create and initialize generic styles
	 * 
	 * @param identifier : the styles identifier
	 * @see #initStyle(XSSFCellStyle, XSSFCellStyle, XSSFCellStyle)
	 */
	private void createStyle(int identifier) {
		if (identifier == 1) {
			triNucleotidNameStyle1 = workbook.createCellStyle();
			phaseFreqStyle1 = workbook.createCellStyle();
			phaseStyle1 = workbook.createCellStyle();
			initStyle(triNucleotidNameStyle1, phaseFreqStyle1, phaseStyle1);
		}
		else {
			triNucleotidNameStyle2 = workbook.createCellStyle();
			phaseFreqStyle2 = workbook.createCellStyle();
			phaseStyle2 = workbook.createCellStyle();
			initStyle(triNucleotidNameStyle2, phaseFreqStyle2, phaseStyle2);
		}
	}

	/**
	 * Initialize statistic data styles
	 */
	private void initDataStyles() {
		if (!reInitDataStyle)
			return;

		createStyle(1);
		createStyle(2);
		setStyleColor(1);
		prefPhaseStyle2 = workbook.createCellStyle();
		prefPhaseStyle2.setFont(sheetFont);
		prefPhaseStyle2.setAlignment(CellStyle.ALIGN_CENTER);
		prefPhaseStyle2.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		prefPhaseStyle2.setDataFormat(workbook.createDataFormat().getFormat("#;#;[Red]0"));
		prefPhaseStyle2.setFillForegroundColor(new XSSFColor(new Color(198, 224, 180)));
		prefPhaseStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		reInitDataStyle = false;
	}

	/**
	 * Initialize styles
	 * 
	 * @param triNucleotidNameStyle0
	 * @param phaseFreqStyle0
	 * @param phaseStyle0
	 */
	private void initStyle(XSSFCellStyle triNucleotidNameStyle0, XSSFCellStyle phaseFreqStyle0, XSSFCellStyle phaseStyle0) {
		initSheetFont();
		triNucleotidNameStyle0.setFont(sheetFont);
		triNucleotidNameStyle0.setAlignment(CellStyle.ALIGN_LEFT);
		triNucleotidNameStyle0.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

		phaseFreqStyle0.setFont(sheetFont);
		phaseFreqStyle0.setAlignment(CellStyle.ALIGN_CENTER);
		phaseFreqStyle0.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		phaseFreqStyle0.setDataFormat(workbook.createDataFormat().getFormat("??0.0??;??0.0??;[Red]0?"));

		phaseStyle0.setFont(sheetFont);
		phaseStyle0.setAlignment(CellStyle.ALIGN_CENTER);
		phaseStyle0.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		phaseStyle0.setDataFormat(workbook.createDataFormat().getFormat("#;#;[Red]0"));
	}

	/**
	 * Initialize total row style
	 */
	private void initTotalStyle() {
		if (!reInitTotalStyle)
			return;

		initSheetFont();
		// total cell style
		totalStyle = workbook.createCellStyle();
		totalStyle.setFont(sheetFont);
		totalStyle.setAlignment(CellStyle.ALIGN_LEFT);
		totalStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		totalStyle.setFillForegroundColor(new XSSFColor(new Color(102, 178, 255)));
		totalStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		// tatal values style
		totalValueStyle = workbook.createCellStyle();
		totalValueStyle.setFont(sheetFont);
		totalValueStyle.setAlignment(CellStyle.ALIGN_CENTER);
		totalValueStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		totalValueStyle.setFillForegroundColor(new XSSFColor(new Color(255, 128, 0)));
		totalValueStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		// totalValueStyle.setDataFormat(workbook.createDataFormat().getFormat("#;#;[Red]0"));

		reInitTotalStyle = false;
	}

	/**
	 * Initialize additional data style
	 */
	private void initAdditonalDataStyle() {
		if (!reInitAdditonalDataStyle)
			return;

		initSheetFont();
		additionalDataStyle = workbook.createCellStyle();
		additionalDataStyle.setAlignment(CellStyle.ALIGN_LEFT);
		additionalDataStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		additionalDataStyle.setFont(sheetFont);
		additionalDataStyle.setFillForegroundColor(new XSSFColor(new Color(255, 228, 196)));
		additionalDataStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		additionalDataStyle2 = workbook.createCellStyle();
		additionalDataStyle2.setAlignment(CellStyle.ALIGN_LEFT);
		additionalDataStyle2.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		additionalDataStyle2.setWrapText(true);
		additionalDataStyle2.setFont(sheetFont);
		additionalDataStyle2.setFillForegroundColor(new XSSFColor(new Color(176, 196, 222)));
		additionalDataStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);

		reInitAdditonalDataStyle = false;
	}

	/**
	 * Initialise the sheet font and the header style
	 */
	private void initHeaderStyle() {

		if (!reInitHeaderSyle)
			return;
		// set up header style
		initSheetFont();
		headerStyle = workbook.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		headerStyle.setFont(sheetFont);
		headerStyle.setFillForegroundColor(new XSSFColor(new Color(255, 128, 0)));
		headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		reInitHeaderSyle = false;
	}

	/**
	 * Initalize a font for the sheet
	 */
	private void initSheetFont() {
		if (sheetFont != null)
			return;
		// initialize font
		sheetFont = workbook.createFont();
		sheetFont.setFontName("Arial");
		sheetFont.setFontHeightInPoints((short) 12);
		// sheetFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		sheet.setDefaultRowHeightInPoints(18);

	}

	/**
	 * Update style to default values or sets color
	 * 
	 * @param alternate : indicate whether syles must be set to default or set colors
	 * @see #initDataStyles()
	 * @see #setStyleColor(int)
	 */
	private void updateStyles(boolean alternate) {
		if (alternate) {
			triNucleotidNameStyle = triNucleotidNameStyle2;
			phaseStyle = phaseStyle2;
			phaseFreqStyle = phaseFreqStyle2;
			prefPhaseStyle = prefPhaseStyle2;

		}
		else {
			triNucleotidNameStyle = triNucleotidNameStyle1;
			phaseStyle = phaseStyle1;
			phaseFreqStyle = phaseFreqStyle1;
			prefPhaseStyle = phaseStyle1;
		}
	}

	/**
	 * Set style color for cells background. colorType can either be 1 or 2 for color setting
	 * 
	 * @param colorType
	 * @see #initDataStyles()
	 */
	private void setStyleColor(int colorType) {
		XSSFColor color1;
		XSSFColor color2;

		if (colorType == 1) {
			color1 = new XSSFColor(new Color(255, 128, 0));
			color2 = new XSSFColor(new Color(102, 178, 255));
		}
		else {
			color2 = new XSSFColor(new Color(255, 128, 0));
			color1 = new XSSFColor(new Color(102, 178, 255));
		}
		triNucleotidNameStyle2.setFillForegroundColor(color1);
		triNucleotidNameStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);

		phaseStyle2.setFillForegroundColor(color2);
		phaseStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);

		phaseFreqStyle2.setFillForegroundColor(color2);
		phaseFreqStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);

	}

	/**
	 * Autosize colomn to fit data size <br/>
	 * Create collapsable zones
	 */
	private void autosizeColumns(boolean createRowGroup) {
		// create collapsable row zone
		try {
			int endColumn = additionalDataColumn + ADDITIONAL_DATA_WIDTH + 1;

			if (isOrganismLevel) {
				sheet.groupColumn((short) 1, (short) additionalDataColumn - 2);
				sheet.setColumnGroupCollapsed((short) 1, false);

				sheet.groupColumn((short) additionalDataColumn, (short) endColumn);
				sheet.setColumnGroupCollapsed((short) additionalDataColumn + 1, false);
			}
			else {
				additionalDataColumn += 1;
				sheet.groupColumn((short) 1, (short) ADDITIONAL_DATA_COLUMN - 3);
				sheet.groupColumn((short) ADDITIONAL_DATA_COLUMN - 1, (short) additionalDataColumn - 1);
				sheet.setColumnGroupCollapsed((short) 1, false);
				sheet.setColumnGroupCollapsed((short) additionalDataColumn + 1, false);
			}

			if (createRowGroup) {
				sheet.groupRow(2, 16);
				sheet.groupRow(18, 32);
				sheet.groupRow(34, 48);
				sheet.groupRow(50, 64);
			}
			else {
				sheet.groupRow(5, 12);
				sheet.groupRow(14, 23);
				sheet.groupRow(25, 33);
			}
		} catch (Exception e) {}

		// resize column
		for (int i = 0; i < additionalDataColumn + 2; i++)
			try {
				sheet.autoSizeColumn(i);
			} catch (Exception e) {}

		sheet.setColumnWidth(additionalDataColumn - 1, 7000);
	}

	/**
	 * Check wether the path name submit is the same as the current organism one.
	 * 
	 * @param organismFilePath : the path to check
	 * @return Return true if paths are the same, false otherwise .
	 */
	public boolean isPreviousOrganism(String organismFilePath) {
		return this.organismFilePath.compareToIgnoreCase(organismFilePath) == 0;
	}

	/**
	 * Compute sum of chromosome,plasmid, ... to sum_sheet and save changes made in the workbook
	 */
	public void save() {
		computeSumAndSave(true);
	}

	/**
	 * @param computeSum wheter to compute number of chromosome , plasmid ... to the sum_sheet
	 * @param d : Number or second to sleep the program
	 */
	public void saveAndWait(boolean computeSum, double d) {
		computeSumAndSave(computeSum);
		sleep(d);
	}

	/**
	 * Save modifications made in workbook. <br>
	 * Note: After this method calling the workbook is not available anymore.
	 * 
	 * @param computeSum wheter to compute number of chromosome , plasmid ... to the sum_sheet
	 */
	public void computeSumAndSave(boolean computeSum) {
		try {
			if (workbook != null) {
				if (computeSum)
					computeGenomeSum();
				else if (PRINT_LOGS)
					System.out.println("\t==> Intermediary saving");

				OutputStream organismOutputStream = new FileOutputStream(organismFile);
				workbook.write(organismOutputStream);
				organismOutputStream.close();
				if (PRINT_LOGS) {
					System.out.println("\t==> Saving " + organismFilePath);
					System.out.println("\t==> Number of sheets: " + workbook.getNumberOfSheets());
				}
				workbook = null;
				closeWorkbook();

			}
			if (organismFileInputStream != null) {
				organismFileInputStream.close();
				opcPackage.close();
			}
		} catch (Exception e) {
			closeWorkbook();
			e.printStackTrace();
		}
	}

	/**
	 * Compute the genome (chromosome, plasmid, plastid ...) sum in the sheet identify by name
	 * {@value #SUM_SHEET_NAME}
	 */
	public void computeGenomeSum() {
		XSSFCell cell;
		long total_cds_sequence = 0;
		long total_invalid_cds = 0;
		long total_nucleotide = 0;
		String genome_sheet_name;

		ComputeSum.initSumMap(sumMap);
		if (workbook.getSheet(SUM_SHEET_NAME) != null)
			workbook.removeSheetAt(workbook.getSheetIndex(SUM_SHEET_NAME));

		int numberOfSheet = workbook.getNumberOfSheets();
		for (int sheetIndex = 0; sheetIndex < numberOfSheet; sheetIndex++) {
			genome_sheet_name = workbook.getSheetName(sheetIndex);
			sheet = workbook.getSheetAt(sheetIndex);
			cell = sheet.getRow(NUMBER_OF_NUCLEOTIDE_ROW).getCell(ADDITIONAL_DATA_COLUMN + 1);
			total_nucleotide += getIntValue(cell.getStringCellValue());

			cell = sheet.getRow(NUMBER_OF_SEQUENCE_ROW).getCell(ADDITIONAL_DATA_COLUMN + 1);
			total_cds_sequence += getIntValue(cell.getStringCellValue());

			cell = sheet.getRow(NUMBER_OF_INVALID_CDS_ROW).getCell(ADDITIONAL_DATA_COLUMN + 1);
			total_invalid_cds += getIntValue(cell.getStringCellValue());

			ComputeSum.updateSumMap(sumMap, genome_sheet_name);
		}

		organismData.getOrganismInfosMap().put(NUMBER_OF_NUCLEOTIDE, total_nucleotide + "");
		organismData.getOrganismInfosMap().put(NUMBER_OF_SEQUENCE, total_cds_sequence + "");
		organismData.getOrganismInfosMap().put(NUMBER_OF_INVALID_CDS, total_invalid_cds + "");
		sheet = workbook.createSheet(SUM_SHEET_NAME);
		for (int i = 0; i < 40; i++)
			sheet.createRow(i);

		reInitTotalStyle = true;
		initTotalStyle();
		totalStyle.setAlignment(CellStyle.ALIGN_CENTER);
		totalValueStyle.setAlignment(CellStyle.ALIGN_LEFT);

		sheet.getRow(SUM_GENOME_LINE_START).createCell(SUM_GENOME_COLUMN).setCellStyle(totalValueStyle);
		sheet.getRow(SUM_GENOME_LINE_START).createCell(SUM_GENOME_COLUMN + 1).setCellStyle(totalStyle);
		sheet.getRow(SUM_GENOME_LINE_START).getCell(SUM_GENOME_COLUMN).setCellValue(SUM_GENOME_HEADER[0]);
		sheet.getRow(SUM_GENOME_LINE_START).getCell(SUM_GENOME_COLUMN + 1).setCellValue(SUM_GENOME_HEADER[1]);

		int rowNumber = SUM_GENOME_LINE_START + 2;
		for (Entry<String, Integer> entry : sumMap.entrySet()) {
			if (entry.getValue() == 0)
				continue;

			sheet.getRow(rowNumber).createCell(SUM_GENOME_COLUMN).setCellStyle(totalValueStyle);
			sheet.getRow(rowNumber).createCell(SUM_GENOME_COLUMN + 1).setCellStyle(totalStyle);
			sheet.getRow(rowNumber).getCell(SUM_GENOME_COLUMN).setCellValue(entry.getKey());
			sheet.getRow(rowNumber).getCell(SUM_GENOME_COLUMN + 1).setCellValue(entry.getValue());
			rowNumber += 2;
		}

		additionalDataColumn = SUM_ADDITIONAL_DATA_COLUMN;
		writeSumAdditionalData();

		autosizeColumns(false);
		additionalDataColumn = ADDITIONAL_DATA_COLUMN;
		workbook.setSheetOrder(sheet.getSheetName(), 0);
	}

	/**
	 * Compute the number of each genome of the organism
	 * 
	 * @param filepath the excel file path
	 */
	public void writeOrganismGenomeSum(String filepath) {
		try {
			TreeMap<String, String> organismInfosMap;
			organismData = new OrganismData();
			organismData.getOrganismInfosMap().put(ORGANISM_FILE_PATH, filepath);
			openWorkbook();
			if (workbook.getSheet(SUM_SHEET_NAME) != null)
				workbook.removeSheetAt(workbook.getSheetIndex(SUM_SHEET_NAME));

			ExcelFileReader excelFileReader = new ExcelFileReader(filepath);
			excelFileReader.open(0);
			organismInfosMap = excelFileReader.readLinesFromColumn(ADDITIONAL_DATA_ROWS, ADDITIONAL_DATA_COLUMN);
			excelFileReader.close();

			organismData.setOrganismInfosMap(organismInfosMap);
			computeSumAndSave(true);
		} catch (InvalidFormatException | IOException exception) {}
	}

	/**
	 * Reinitialize the workbook space. Styles are mark as invalid.
	 */
	public void closeWorkbook() {
		sheetFont = null;
		workbook = null;
		reInitAdditonalDataStyle = true;
		reInitDataStyle = true;
		reInitHeaderSyle = true;
		reInitTotalStyle = true;
		additionalDataColumn = ADDITIONAL_DATA_COLUMN;
	}

	public boolean isOrganismLevel() {
		return isOrganismLevel;
	}

	public void setOrganismLevel(boolean isOrganismLevel) {
		this.isOrganismLevel = isOrganismLevel;
	}

	/**
	 * Return the organism data
	 * 
	 * @return return the organism data
	 */
	public OrganismData getOrganismData() {
		return organismData;
	}

	/**
	 * set the data to write to excel file
	 * 
	 * @param organismData : data to set
	 */
	public void setOrganismData(OrganismData organismData) {
		this.organismData = organismData;
	}

}
