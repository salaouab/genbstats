package fr.unistra.controller.dataParser;

import static fr.unistra.utils.MainConstants.LOCATION;
import static fr.unistra.utils.MainConstants.PRINT_LOGS;
import static fr.unistra.utils.MainConstants.containsTerm;
import static fr.unistra.utils.MainConstants.getTerm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.algosome.eutils.io.InputStreamParser;

import fr.unistra.model.beans.CodingSequence;
import fr.unistra.model.beans.OrganismData;

/**
 * This class parses the fetching {@link CodingSequence} from ncbi databases
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class CodingSequenceParser implements InputStreamParser {
	private static final String	      START_CODON	     = "ATA|ATC|ATG|ATT|CTG|GTG|TTA|TTG";
	private static final String	      STOP_CODON	     = "TAA|TAG|TGA";
	private boolean	                  errorFound	     = false;
	private boolean	                  emptyErrorFound	 = false;
	private int	                      numberOfInvalidCds	= 0;
	private StringBuilder	          sequence;
	private StringBuilder	          sequenceHeader;
	private ArrayList<CodingSequence>	codingSequenceList;
	private int	                      numberOfSequence;
	private OrganismData	          organismData;

	/**
	 * Constructor with the {@link OrganismData} informed
	 * 
	 * @param organismData the {@link OrganismData}
	 */
	public CodingSequenceParser(OrganismData organismData) {
		this.organismData = organismData;
		codingSequenceList = organismData.getCodingSequenceList();
		sequence = new StringBuilder();
		sequenceHeader = new StringBuilder();
	}

	/**
	 * Parse and store {@link CodingSequence}s in the arrayList, by checking their validation
	 * 
	 * @see com.algosome.eutils.io.InputStreamParser#parseInput(java.io.InputStream)
	 */
	@Override
	public void parseInput(InputStream inputStream) throws IOException {
		if (codingSequenceList == null) {
			errorFound = true;
			if (PRINT_LOGS)
				System.err.println("Coding sequence list is not initialize (null value found)");
			return;
		}

		InputStreamReader streamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(streamReader);
		String line = null;
		String codingSeq;
		boolean readNewLine = true;
		numberOfSequence = 0;
		numberOfInvalidCds = 0;
		emptyErrorFound = false;
		errorFound = false;
		// sequence.setLength(0);
		// sequenceHeader.setLength(0);

		while (!readNewLine || (line = bufferedReader.readLine()) != null) {
			readNewLine = true;
			// the sequence header: protein, location, ...
			if (containsTerm(line, LOCATION)) {
				sequence.setLength(0);
				sequenceHeader.setLength(0);
				sequenceHeader.append(line.trim());
				if (!isLocationValid(sequenceHeader.toString())) {
					numberOfInvalidCds++;
					continue;
				}
				// Sequence value (acgt....acgt)
				while ((line = bufferedReader.readLine()) != null) {
					if (containsTerm(line, LOCATION) || line.trim().isEmpty()) {
						readNewLine = false;
						break;
					}
					sequence.append(line.trim());
				}

				// store the sequence if it is valid
				codingSeq = sequence.toString();
				if (isCodingSequenceValid(codingSeq)) {
					// codingSequenceList.add(new CodingSequence(codingSeq,
					// sequenceHeader.toString()));
					organismData.codingSequenceStat(codingSeq);
					numberOfSequence++;
				}
				else
					numberOfInvalidCds++;
			}
		}
		organismData.setNumberOfSequence(numberOfSequence);
		organismData.setNumberOfInvalidCds(numberOfInvalidCds);

		bufferedReader.close();
		streamReader.close();
		inputStream.close();
	}

	/**
	 * Says if an error occured
	 * 
	 * @return <b>true</b> if an error occured, <b>false</b> otherwise
	 */
	public boolean isErrorFound() {
		return errorFound;
	}

/**
	 * Check for the gene location :<br/>
	 *   - Whether its start or end is unknow ( contain '>' or '<') <br/>
	 *  - If is there no one point '.' between Number <br/>
	 *  - If is there no charachters immediately before or after numbers<br/>
	 *  - If itsLength is modulo 3 <br/><br/>
	 * @param location
	 * @return Return <b>true</b> if these condition are fullfiled, <b>false</b> otherwise
	 * @see #getIntervalValue(String)
	 */
	public boolean isLocationValid(String sequenceHeader) {
		String location = extractLocation(sequenceHeader);
		String intervalPatern = "([0-9]+\\.\\.[0-9]+)";// Patern for interval
		// retrieval
		String brackts = "[<>]";
		String pointBetweenNumbers = "[0-9]+\\.[0-9]+";
		String alphaInFrontOfNumber = "[a-zA-Z_][0-9]";
		String alphaAfterNumber = "[0-9]+.*[a-zA-Z]";
		int geneLength = 0;
		// Regex for unwanted Coding Sequence
		String prohibitCharset = "(" + brackts + "|" + pointBetweenNumbers + "|" + alphaInFrontOfNumber + "|" + alphaAfterNumber + ")";
		Pattern pattern = Pattern.compile(prohibitCharset);
		Matcher matcher = pattern.matcher(location);
		// check prohibit charset
		if (matcher.find())
			return false;

		pattern = Pattern.compile(intervalPatern);
		matcher = pattern.matcher(location);
		int intervalValue = 0;
		boolean intervalFound = false;// wether the loop bloc is visited or not!

		// get gene Sequence length
		while (matcher.find()) {
			intervalFound = true;
			String geneInterval = matcher.group(1);
			intervalValue = getIntervalValue(geneInterval);

			if (intervalValue <= 0)// if born inf > born Sup , or wrong number
				// format given
				return false;

			geneLength += intervalValue;
		}

		if (intervalFound)
			return geneLength % 3 == 0;
		else
			return false;
	}

	/**
	 * Verify wheter the coding sequence is valid, by checking start codon, stop codon, and if it
	 * contains some character else than <b>A, C, G, T</b>.
	 * 
	 * @param sequence
	 * @return Return <b>true</b> if the sequence is valid, <b>false</b> otherwise
	 * @see #hasNormalStartCodon(String)
	 * @see #hasNormalStopCodon(String)
	 * @see #containsElseThanACTG(String)
	 */
	public boolean isCodingSequenceValid(String sequence) {

		try {
			return hasNormalStartCodon(sequence) && hasNormalStopCodon(sequence) && containsElseThanACTG(sequence);
		} catch (Exception e) {
			emptyErrorFound = true;
			return false;
		}
	}

	/**
	 * Return the coding sequence location on the entier gene sequence. It may be :<br/>
	 * - <i>Number..Number</i><br/>
	 * - <i>Complement (Number..Number)</i><br/>
	 * - <i>Join (Number..Number,Number..Number,<b>...</b>,Number..Number)</i><br/>
	 * - <i>Complement(Join (Number..Number,Number..Number,<b>...</b>,Number..Number))</i><br/>
	 * 
	 * @param geneInfos : String containing gene infos,specific formatted.
	 * @return Return a string representing
	 * @see #getTerm(String, String)
	 */
	private String extractLocation(String geneInfos) {
		return getTerm(LOCATION, geneInfos);
	}

	/**
	 * Compute the interval value between the numbers
	 * 
	 * @param intervalString : String representing two numbers mark off with double point
	 *            <b>'..'</b>
	 * @return return the interval length
	 */
	public static int getIntervalValue(String intervalString) {
		int intervalValue = 0;
		String[] values = intervalString.trim().split("\\.\\.");
		try {
			intervalValue = Integer.parseInt(values[1].trim()) - Integer.parseInt(values[0].trim()) + 1;
		} catch (NumberFormatException exception) {
			intervalValue = Integer.MIN_VALUE;
		}
		return intervalValue;
	}

	/**
	 * Check if the coding sequence contains other letters than <b>A,C,T,G,</b>.
	 * 
	 * @param sequence : Contains the coding sequence
	 * @return Return <b>true</b> if it contains only <b>A, C, G, T</b> well, <b>false</b> otherwise
	 */
	private boolean containsElseThanACTG(String sequence) {
		return sequence.toUpperCase().matches("[ACGT]+");

	}

	/**
	 * Check wether the sequence begin with start codon <b>{@value #START_CODON} </b>
	 * 
	 * @param sequence : Contains the coding sequence
	 * @return Return <b>true</b> if it starts well, <b>false</b> otherwise
	 */
	private boolean hasNormalStartCodon(String sequence) {
		return sequence.toUpperCase().substring(0, 3).matches(START_CODON);
	}

	/**
	 * Check wether the sequence end with stop codon <b>{@value #START_CODON} </b>
	 * 
	 * @param sequence : Contains the coding sequence
	 * @return Return <b>true</b> if it ends well, <b>false</b> otherwise
	 */
	private boolean hasNormalStopCodon(String sequence) {
		return sequence.toUpperCase().substring(sequence.length() - 3).matches(STOP_CODON);
	}

	public int getNumberOfInvalidCds() {
		return numberOfInvalidCds;
	}

	public void setNumberOfInvalidCds(int numberOfInvalidCds) {
		this.numberOfInvalidCds = numberOfInvalidCds;
	}

	@Override
	public void parseFrom(int start) {}

	@Override
	public void parseTo(int end) {}
}
