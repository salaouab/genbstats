package fr.unistra.controller.organismParser;

import static fr.unistra.utils.MainConstants.*;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import fr.unistra.model.beans.Organism;

/**
 * Parses {@link Organism}s name and associated bioProject
 * @author Abdoul-Djawadou
 */
public class OrganismParser {
	public static final String	  FIELD_SEPARATOR	     = "\t";
	protected String	          filePath;
	protected String	          kingdomName	         = "Organisms";
	protected int	              groupIndex	         = -1;
	protected int	              subGroupIndex	         = -1;
	protected int	              nameIndex	             = -1;
	protected int	              bioProjectIndex	     = -1;
	protected int	              numberOfRepliconsIndex	= -1;
	protected boolean	          useRepliconsField	     = false;
	protected int	              genomeIdIndex	         = 0;
	protected String[]	          fieldsData	         = null;
	protected ArrayList<Organism>	organismList;
	protected static final String	URL_BASE	         = "http://www.ncbi.nlm.nih.gov/genomes/Genome2BE/genome2srv.cgi?action=download";
	/** Default url for organism list retrieval from genome databank of Ncbi */
	protected String	          organismUrl	         = URL_BASE + "&orgn=&report=overview&king=All&group=All&subgroup=All";
	public static boolean	      NCBI_ERROR_TRIGGERED	 = false;
	public static String	      NCBI_ERROR_MESSAGE	 = "";

	/**
	 * Constructor with organism file path provided
	 * @param filePath organism file path
	 */
	public OrganismParser(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * Default constructor
	 */
	public OrganismParser() {
		this("");
	}

	/**
	 * Load organism by parsing organism file pointed by the file path
	 * @param filePath : organism file path
	 * @return return the list of organism, or null if an error occured
	 * @throws IOException
	 */
	public ArrayList<Organism> loadOrganisms(String filePath) throws IOException {
		setFilePath(filePath);
		return loadOrganisms();
	}

	/**
	 * Load organisms by parsing organism file
	 * @return return the list of organism , or null if an error occured
	 * @throws IOException
	 */
	public ArrayList<Organism> loadOrganisms() throws IOException {
		FileInputStream fileInputStream = new FileInputStream(new File(filePath));
		return loadOrganisms(fileInputStream);
	}

	/**
	 * F
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public ArrayList<Organism> loadOrganisms(InputStream inputStream) throws IOException {
		organismList = new ArrayList<Organism>();
		String line;
		NCBI_ERROR_MESSAGE = "";
		NCBI_ERROR_TRIGGERED = false;
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		// Get field indexes
		while ((line = bufferedReader.readLine()) != null) {
			// if we get the right page
			if (extractIndexes(line)) {
				break;
			}
			// if the ncbi site return an error message
			if (containsTerm(line, "DOCTYPE")) {
				do
					NCBI_ERROR_MESSAGE += getModifiedLine(line) + "\n";
				while ((line = bufferedReader.readLine()) != null);

				NCBI_ERROR_TRIGGERED = true;
				return organismList;
			}
			System.out.println("line");

		}

		// Verification
		if (nameIndex < 0) {
			bufferedReader.close();
			return organismList;
		}

		// Extract field values
		while ((line = bufferedReader.readLine()) != null)
			if (isThisOrganismWorthSaving(line))
				addOrganism();

		bufferedReader.close();
		inputStreamReader.close();
		inputStream.close();

		if (PRINT_LOGS)
			System.out.println(kingdomName + " Loaded succesfully!");

		return organismList;
	}

	public ArrayList<Organism> loadOrganismsByURL() {
		return loadOrganismsByURL(organismUrl);
	}

	public ArrayList<Organism> loadOrganismsByURL(String url) {
		URL entrez;
		URLConnection conn;
		int numberOfTrial = 0;
		boolean tryAgain = false;
		do
			try {
				entrez = new URL(url);
				conn = entrez.openConnection();
				conn.setConnectTimeout(100000);// 100 s for timeout
				conn.connect();
				return loadOrganisms(conn.getInputStream());
			} catch (Exception exception) {
				if (DEBUG)
					exception.printStackTrace();
				if (PRINT_LOGS)
					System.err.println("Unable to connect to the :" + url);
				tryAgain = true;
				numberOfTrial++;
			}
		while (tryAgain && numberOfTrial < 3);
		if (PRINT_LOGS)
			System.err.println("Some trouble for " + kingdomName + " Loading. Please check you internet connection and try again.");
		return new ArrayList<Organism>();
	}

	/**
	 * Add new construct {@link Organism} to the {@link #organismList}
	 */
	protected void addOrganism() {
		Organism organism = new Organism(fieldsData[nameIndex], fieldsData[bioProjectIndex]);
		if (organismList.contains(organism))
			return;
		if (extractGenomeIds()) {
			String[] genomeIdRef = fieldsData[genomeIdIndex].split(";");
			for (String idRef : genomeIdRef) {
				String idValue = getFieldValue(idRef);
				if (!idValue.isEmpty()) {
					organism.addId(idValue);
					organism.addStructure(getStructureValue(idRef));
				}
			}
		}
		organism.setGroup(fieldsData[groupIndex]);
		organism.setSubGroup(fieldsData[subGroupIndex]);
		// organism.setGroup(kingdomName);
		organismList.add(organism);

	}

	/**
	 * Check wether to save this {@link Organism} or not.
	 * @param line : holds organism information
	 * @return Return <b>true</b> if the organism worth saving, <b>false</b> otherwise
	 */
	protected boolean isThisOrganismWorthSaving(String line) {
		fieldsData = line.split(FIELD_SEPARATOR);

		return fieldsData.length > 0;
	}

	/**
	 * Extract some organism spécific information indexes
	 * @param line : holds the organism information
	 * @return Return <b>true</b> if it the extraction succeded, <b>false</b> otherwise
	 */
	protected boolean extractIndexes(String line) {

		nameIndex = getOrganismNameIndex(line);
		bioProjectIndex = getOrganismBioProjectIndex(line);
		numberOfRepliconsIndex = getNumberOfRepliconsIndex(line);
		groupIndex = getGroupIndex(line);
		subGroupIndex = getSubgroupIndex(line);
		if (numberOfRepliconsIndex > -1)
			useRepliconsField = true;
		else
			useRepliconsField = false;

		return nameIndex > -1 && bioProjectIndex > -1;
	}

	protected boolean extractGenomeIds() {
		if (!useRepliconsField)
			return false;

		genomeIdIndex = numberOfRepliconsIndex;
		return true;
	}

	protected static String getFieldValue(String rawField) {
		try {
			int index = rawField.indexOf(":");
			if (index >= 0)
				return rawField.substring(index + 1).split("/")[0].trim();

			return rawField.split("/")[0].trim();
		} catch (Exception exception) {
			return "";
		}
	}

	protected static String getStructureValue(String rawField) {
		try {
			int index = rawField.indexOf(":");
			if (index >= 0)
				return rawField.split(":")[0].trim();

		} catch (Exception exception) {}
		return UNKNOWN;
	}

	/**
	 * Get the index of field giving the organism name
	 * @param line : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	protected static int getOrganismNameIndex(String line) {
		return getTermIndex(line, ".*organism.*name");
	}

	/**
	 * Get the index of field giving the bioproject id
	 * @param line : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	protected static int getOrganismBioProjectIndex(String line) {
		return getTermIndex(line, ".*BioProject.*");

	}

	/**
	 * Get the index of field giving the number of replicons(chromosome, plasmids,...)
	 * @param line : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	protected static int getNumberOfRepliconsIndex(String line) {
		return getTermIndex(line, ".*replicon.");
	}

	/**
	 * Get the index of field giving the group name
	 * @param line : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	protected static int getGroupIndex(String line) {
		return getTermIndex(line, ".*Group");
	}

	/**
	 * Get the index of field giving the subgroup name
	 * @param line : holds the organism information
	 * @return return the field index
	 * @see OrganismParser#getTermIndex(String, String)
	 */
	protected static int getSubgroupIndex(String line) {
		return getTermIndex(line, ".*SubGroup");
	}

	/**
	 * Get the index of field holding the term
	 * @param line : holds the organism information
	 * @param term : term to get the index
	 * @return return the field index
	 */
	protected static int getTermIndex(String line, String term) {
		int index = 0;
		for (String field : line.split(FIELD_SEPARATOR)) {
			if (field.trim().matches("(?i)" + term))
				return index;

			index++;
		}
		return -1;
	}

	protected static String getOrganismName(String line, int index) {
		try {
			return line.split(FIELD_SEPARATOR)[index].trim();
		} catch (Exception exception) {
			return "";
		}
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	protected static String getModifiedLine(String line) {
		boolean textHtmlChecked = false;
		boolean charsetChecked = false;
		boolean fontSizeChecked = false;
		boolean minWidthChecked = false;
		boolean widthChecked = false;

		if (!textHtmlChecked && containsTerm(line, "text/html?;")) {
			line = line.replaceFirst("text/html?;", "text/html\" ");
			textHtmlChecked = true;
		}

		if (!charsetChecked && containsTerm(line, "charset?=[ ]*[a-zA-Z]")) {
			line = line.replaceFirst("charset?=[ ]*", "charset=\"");
			charsetChecked = true;
		}

		if (!minWidthChecked && containsTerm(line, "min-width?:[ ]*[7-9][0-9]{2}[ ]*px")) {
			line = line.replaceFirst("min-width?:[ ]*[7-9][0-9]{2}[ ]*px", "min-width: 500px");
			minWidthChecked = true;
		}

		if (!widthChecked && containsTerm(line, "width?:[ ]*[7-9][0-9]{2}[ ]*px")) {
			line = line.replaceFirst("width?:[ ]*[7-9][0-9]{2}[ ]*px", "width: 100%");
			widthChecked = true;
		}

		if (!fontSizeChecked && containsTerm(line, "font-size?:[ ]*[2-9][0-9]{2}[ ]*%")) {
			line = line.replaceFirst("font-size?:[ ]*[2-9][0-9]{2}[ ]*%", "font-size: 130%");
			fontSizeChecked = true;
		}

		return line;
	}

	public static void main(String[] args) throws IOException {
		ArrayList<Organism> list = new OrganismParser().loadOrganismsByURL();
		int i = 0;
		System.out.println("===>> Total organism  " + list.size() + "\n");
		for (Organism organism : list) {
			System.out.println(organism);
			if (i++ > 20)
				break;
		}

	}
}
