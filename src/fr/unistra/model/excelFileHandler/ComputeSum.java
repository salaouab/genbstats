/**
 *
 */
package fr.unistra.model.excelFileHandler;

import static fr.unistra.model.excelFileHandler.WriteSumData.getSumWorkbookName;
import static fr.unistra.utils.ExcelFileUtils.*;
import static fr.unistra.utils.FileUtil.FILE_SEPARATOR;
import static fr.unistra.utils.MainConstants.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import fr.unistra.model.beans.OrganismData;

/**
 * Computes hierarchical sum of organisms
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class ComputeSum {
	public static final int	        SUM_GENOME_COLUMN	       = 0;
	public static final int	        SUM_GENOME_LINE_START	   = 2;
	public static final int	        SUM_ADDITIONAL_DATA_COLUMN	= 3;
	public static final int	        S_BIOPROJECT_ROW	       = 10;
	public static final int	        S_ORGANISM_TAXONOMY_ROW	   = 12;
	public static final int	        S_TITLE_ROW	               = 16;
	public static final int[]	    SUM_ADDITIONAL_DATA_ROWS	= { ORGANISM_NAME_ROW, NUMBER_OF_NUCLEOTIDE_ROW, NUMBER_OF_SEQUENCE_ROW,
		NUMBER_OF_INVALID_CDS_ROW, S_BIOPROJECT_ROW, S_ORGANISM_TAXONOMY_ROW, S_TITLE_ROW };
	public static final String[]	SUM_ADDITIONAL_DATA	       = { ORGANISM_NAME, NUMBER_OF_NUCLEOTIDE, NUMBER_OF_SEQUENCE, NUMBER_OF_INVALID_CDS,
		BIOPROJECT, ORGANISM_TAXONOMY, TITLE	   };
	public static final String[]	SUM_GENOME_HEADER	       = { "Genome", "Total" };
	public static final String	    NUMBER_OF_ORGANISM	       = "Number of organisms";
	public static final String	    NUMBER_OF_SUBGROUP	       = "Number of subgroups";
	public static final String	    NUMBER_OF_GROUP	           = "Number of groups";
	public static final String	    NUMBER_OF_KINGDOM	       = "Number of kingdoms";
	public static final String[]	SUM_ADDITIONAL_DATA2	   = { ORGANISM_NAME, NUMBER_OF_ORGANISM, NUMBER_OF_SUBGROUP, NUMBER_OF_GROUP,
		NUMBER_OF_KINGDOM, NUMBER_OF_NUCLEOTIDE, NUMBER_OF_SEQUENCE, NUMBER_OF_INVALID_CDS, };
	private TreeMap<String, Long>	additionalValueSum	       = new TreeMap<String, Long>(String.CASE_INSENSITIVE_ORDER);
	private TreeMap<String, Long>	sumMap	                   = new TreeMap<String, Long>(String.CASE_INSENSITIVE_ORDER);
	private OrganismData	        organismData	           = new OrganismData();
	private ExcelFileWritter	    excelFileWritter;
	private StatDataReader	        statsReader	               = new StatDataReader();
	private TreeMap<String, String>	organismInfosMap	       = organismData.getOrganismInfosMap();
	private File	                kingdomsRootFile;
	private boolean	                isOrganismLevel;

	/**
	 * Default constructor
	 */
	public ComputeSum() {
		excelFileWritter = new ExcelFileWritter(organismData);
		excelFileWritter.setOrganismLevel(false);
	}

	/**
	 * Constructor with root path informed
	 * 
	 * @param KindomsRootPath : Kingdoms root file path
	 */
	public ComputeSum(String KindomsRootPath) {
		this(new File(KindomsRootPath));
	}

	/**
	 * Constructor with root file informed
	 * 
	 * @param KindomsRootFile : Kingdoms root file
	 */
	public ComputeSum(File kingdomsRootFile) {
		this();
		this.kingdomsRootFile = kingdomsRootFile;
	}

	/**
	 * Computes the hierarchical sum of organisms.
	 */
	public void compute() {
		if (kingdomsRootFile == null) {
			System.err.println("Unable to open the file. Please check your file paths!");
			return;
		}
		System.out.println("==> Start of hierarchical sum computing");
		File[] kindomsFile = kingdomsRootFile.listFiles();
		for (File kingdomFile : kindomsFile) {
			if (kingdomFile.isDirectory())
				computeKingdomSum(kingdomFile);
		}
		computeWholeKingdomsSum(kindomsFile);
		computeProkaryoteKingdomSum();
		System.out.println("\n==> End of hierarchical sum computing");
	}

	/**
	 * Computes all kingdoms hierarchical sum and save result in excel file
	 * 
	 * @param kingdomsFile : Kingdom files array
	 */
	public void computeWholeKingdomsSum(File[] kingdomsFile) {
		ArrayList<File> listKingdomSum = new ArrayList<File>();
		String kingdomsList = "";
		File file;
		for (File kingdom : kingdomsFile) {
			if (kingdom.isDirectory())
				try {
					file = new File(kingdom.getPath() + FILE_SEPARATOR + getSumWorkbookName(kingdom.getPath()));
					if (file.exists()) {
						listKingdomSum.add(file);
						kingdomsList += kingdomsList.isEmpty() ? kingdom.getName().trim() : "; " + kingdom.getName().trim();
					}

				} catch (Exception exception) {}
		}
		String path = kingdomsFile[0].getPath();
		path = path.substring(0, path.lastIndexOf(FILE_SEPARATOR));
		WriteSumData sumDataWriter = new WriteSumData();
		sumDataWriter.setKingdomList(kingdomsList);
		try {
			isOrganismLevel = false;
			long numberOfElement = computeFilesSum(listKingdomSum.toArray(new File[listKingdomSum.size()]));
			additionalValueSum.put(NUMBER_OF_KINGDOM, numberOfElement);
			sumDataWriter.setParameter(sumMap, additionalValueSum, 0, path);
			sumDataWriter.write();
			saveStatsSumData(path);
		} catch (Exception exception) {}
	}

	/**
	 * Computes prokaryote kingdom hierarchical sum and save result in excel file
	 */
	public void computeProkaryoteKingdomSum() {
		ArrayList<File> listKingdomSum = new ArrayList<File>();
		File archaeaSumFile, bacteriaSumFile;
		String kingdomsList = "Archaea ; Bacteria";
		String archaeaPath = kingdomsRootFile.getPath() + FILE_SEPARATOR + "Archaea";
		String bacteriaPath = kingdomsRootFile.getPath() + FILE_SEPARATOR + "Bacteria";
		try {
			archaeaSumFile = new File(archaeaPath + FILE_SEPARATOR + getSumWorkbookName(archaeaPath));
			bacteriaSumFile = new File(bacteriaPath + FILE_SEPARATOR + getSumWorkbookName(bacteriaPath));
			if (!archaeaSumFile.exists() || !bacteriaSumFile.exists()) {
				if (DEBUG)
					System.err.println(" One Kindom is missing for prokaryota sum agglomaration!");
				return;
			}
			listKingdomSum.add(archaeaSumFile);
			listKingdomSum.add(bacteriaSumFile);
		} catch (Exception exception) {
			return;
		}
		String path = kingdomsRootFile.getPath();
		path += FILE_SEPARATOR + SUM_SHEET_NAME + "_Prokaryota" + EXCEL_FILE_EXTENSION;
		WriteSumData sumDataWriter = new WriteSumData();
		sumDataWriter.setKingdomList(kingdomsList);
		try {
			isOrganismLevel = false;
			long numberOfElement = computeFilesSum(listKingdomSum.toArray(new File[listKingdomSum.size()]));
			additionalValueSum.put(NUMBER_OF_KINGDOM, numberOfElement);
			sumDataWriter.setParameter(sumMap, additionalValueSum, 4, path);
			sumDataWriter.write(false);
			saveStatsSumData(path, false);
		} catch (Exception exception) {}
	}

	/**
	 * Computes a kingdom hierarchical sum
	 * 
	 * @param kingdomFile : The kingdom file
	 */
	public void computeKingdomSum(File kingdomFile) {
		ArrayList<File[]> listFichiers = new ArrayList<File[]>();
		String path = null;
		String currentParentPath = "";
		String parentPath = null;
		// Group-Subgroup-Organism
		int level = 3;
		ArrayList<File> parentFile = new ArrayList<File>();
		ArrayList<File[]> parentFileList = new ArrayList<File[]>();
		WriteSumData sumDataWriter = new WriteSumData();
		boolean stop = false;
		isOrganismLevel = true;
		// Get organisms list file
		getOrganismFilesList(listFichiers, kingdomFile);
		long numberOfElement;
		do {

			for (File[] files : listFichiers) {
				path = files[0].getPath();
				path = path.substring(0, path.lastIndexOf(FILE_SEPARATOR));
				if (!isOrganismLevel) {
					path = path.substring(0, path.lastIndexOf(FILE_SEPARATOR));
				}
				try {
					numberOfElement = computeFilesSum(files);
					if (level == 2)
						additionalValueSum.put(NUMBER_OF_SUBGROUP, numberOfElement);
					if (level == 1)
						additionalValueSum.put(NUMBER_OF_GROUP, numberOfElement);

					sumDataWriter.setParameter(sumMap, additionalValueSum, level, path);
					sumDataWriter.write();
					saveStatsSumData(path);
				} catch (Exception exception) {
					exception.printStackTrace();
				}

				parentPath = path.substring(0, path.lastIndexOf(FILE_SEPARATOR));
				if (parentPath.compareToIgnoreCase(currentParentPath) == 0)
					parentFile.add(new File(path + FILE_SEPARATOR + getSumWorkbookName(path)));
				else {
					if (!parentFile.isEmpty()) {
						parentFileList.add(parentFile.toArray(new File[parentFile.size()]));
						parentFile.clear();
					}
					currentParentPath = parentPath;
					parentFile.add(new File(path + FILE_SEPARATOR + getSumWorkbookName(path)));
				}
			}
			if (!parentFile.isEmpty()) {
				parentFileList.add(parentFile.toArray(new File[parentFile.size()]));
				parentFile.clear();
			}
			listFichiers = parentFileList;
			parentFileList = new ArrayList<File[]>();
			currentParentPath = "";
			level--;
			isOrganismLevel = false;
			stop = path != null && kingdomFile.getPath().compareTo(path) == 0;

		} while (!stop);
	}

	/**
	 * Check wether the array file does not contain directory
	 * 
	 * @param fileList : The array file
	 * @return Return <b>true</b> if it does not contain directory<b>false</b> otherwise
	 */
	public static boolean isAllFile(File[] fileList) {
		for (File file : fileList)
			if (file.isDirectory())
				return false;

		return true;
	}

	/**
	 * Save stats data to the workbook identified by the file path
	 * 
	 * @param filePath Path to save stats data
	 * @see #saveStatsSumData(String, boolean)
	 */
	private void saveStatsSumData(String filePath) {
		saveStatsSumData(filePath, true);
	}

	/**
	 * Save stats data to the workbook identified by the file path
	 * 
	 * @param filePath Path to save stats data
	 * @param addSuffix If true add sum_sheet_xxx to the file name ending
	 */
	private void saveStatsSumData(String filePath, boolean addSuffix) {
		if (addSuffix)
			filePath += FILE_SEPARATOR + getSumWorkbookName(filePath);
		organismInfosMap.put(ORGANISM_FILE_PATH, filePath);

		long[] additionalValues;
		for (Entry<String, TreeMap<String, long[]>> entry : statsReader.getOrganismSumValueMap().entrySet()) {
			additionalValues = statsReader.getAdditionalValues(entry.getKey());
			organismInfosMap.put(ORGANISM_SHEET_NAME, entry.getKey() + "_sum");
			organismInfosMap.put(NUMBER_OF_SEQUENCE, additionalValues[0] + "");
			organismInfosMap.put(NUMBER_OF_INVALID_CDS, additionalValues[1] + "");
			organismData.setCodingSequenceStatisticMap(entry.getValue());
			try {
				excelFileWritter.writeStatData();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		excelFileWritter.computeSumAndSave(false);
	}

	/**
	 * Recursively parse directory to get file list.
	 * 
	 * @param listFichiers : File list
	 * @param directory : Directory file to parse
	 */
	public static void getOrganismFilesList(ArrayList<File[]> listFichiers, File directory) {
		if (!directory.isDirectory())
			return;
		File[] list = directory.listFiles();
		if (isAllFile(list))
			listFichiers.add(list);
		else
			for (File file : list)
				getOrganismFilesList(listFichiers, file);
	}

	/**
	 * Compute files values sum
	 * 
	 * @param fileList : Array of files
	 * @return Return the effective number of files process.
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public long computeFilesSum(File[] fileList) throws InvalidFormatException, IOException {
		long value = 0;
		long totalValidCds = 0;
		long totalInvalidCds = 0;
		long numberOfElements = 0;
		String stringValue;
		statsReader.clearAll();
		sumMap.clear();
		additionalValueSum.clear();

		TreeMap<String, String> fileSumMap;
		// Rows to retreive numbers
		int[] fieldToRetreive = { 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40 };

		initSumMapL(sumMap);
		for (File file : fileList) {

			// Skip these files
			if (file.getName().startsWith(".~") || !file.getName().endsWith(EXCEL_FILE_EXTENSION)
							|| (isOrganismLevel && file.getName().contains(SUM_SHEET_NAME)))
				continue;

			// Open workbook
			statsReader.setWorkbookFile(file);
			try {
				statsReader.open();

				if (isOrganismLevel && !statsReader.isSheetExist(SUM_SHEET_NAME)) {
					statsReader.close();
					new ExcelFileWritter().writeOrganismGenomeSum(file.getPath());
					statsReader.setWorkbookFile(file);
					statsReader.open();
				}
			} catch (InvalidFormatException e) {
				System.err.println("Opening   " + file.getPath());
				file.delete();
				continue;
			} catch (Exception exception) {
				System.err.println("Autre   " + file.getPath());
				file.delete();
				// exception.printStackTrace();
				continue;
			}
			// sum up codon stats values
			statsReader.sumStatsData();
			// load sum sheet
			statsReader.loadSheet(SUM_SHEET_NAME);
			// Get values
			fileSumMap = statsReader.readLinesFromColumn(fieldToRetreive, SUM_GENOME_COLUMN);
			// fill the values map
			for (Entry<String, String> entry : fileSumMap.entrySet()) {
				try {
					value = getLongValue(entry.getValue()) + sumMap.get(entry.getKey());
					sumMap.put(entry.getKey(), value);
				} catch (Exception e) {
					System.out.println("Reading  " + file.getPath());
					System.out.println(e.getMessage());
				}
			}
			if (isOrganismLevel) {
				stringValue = statsReader.readCell(NUMBER_OF_SEQUENCE_ROW, SUM_ADDITIONAL_DATA_COLUMN + 1);
				totalValidCds += getLongValue(stringValue);
				stringValue = statsReader.readCell(NUMBER_OF_INVALID_CDS_ROW, SUM_ADDITIONAL_DATA_COLUMN + 1);
				totalInvalidCds += getLongValue(stringValue);
			}
			else {
				// Collect additional data
				fileSumMap = statsReader.readLinesFromColumn(fieldToRetreive, SUM_ADDITIONAL_DATA_COLUMN);
				for (Entry<String, String> entry : fileSumMap.entrySet()) {
					try {
						if (additionalValueSum.get(entry.getKey()) == null) {
							additionalValueSum.put(entry.getKey(), (long) 0);
						}
						value = getLongValue(entry.getValue()) + additionalValueSum.get(entry.getKey());
						additionalValueSum.put(entry.getKey(), value);
					} catch (Exception e) {}
				}
			}
			statsReader.close();
			numberOfElements++;

		}

		if (isOrganismLevel) {
			additionalValueSum.put(NUMBER_OF_SEQUENCE, totalValidCds);
			additionalValueSum.put(NUMBER_OF_INVALID_CDS, totalInvalidCds);
			additionalValueSum.put(NUMBER_OF_ORGANISM, numberOfElements);
		}
		return numberOfElements;
	}

	/**
	 * Initialize map by filling its values to 0.
	 * 
	 * @param map
	 */
	public static void initSumMap(TreeMap<String, Integer> map) {
		for (String type : genomeType)
			map.put(normalise(type), 0);
	}

	/**
	 * Initialise long values map by filling it with 0
	 * 
	 * @param map
	 */
	public static void initSumMapL(TreeMap<String, Long> map) {
		for (String type : genomeType)
			map.put(normalise(type), (long) 0);
	}

	/**
	 * Update map value by incrementing the structure entry by one.
	 * 
	 * @param sumMap : Map containing structures values
	 * @param structure : structure to increment
	 */
	public static void updateSumMap(TreeMap<String, Integer> sumMap, String structure) {

		boolean found = false;
		for (String type : genomeType) {
			type = normalise(type);
			if (containsTerm(structure, type)) {
				sumMap.put(type, sumMap.get(type) + 1);
				found = true;
				break;
			}
		}
		if (!found)
			sumMap.put(UNKNOWN, sumMap.get(UNKNOWN) + 1);
	}

	public File getKingdomsRootFile() {
		return kingdomsRootFile;
	}

	public void setKingdomsRootFile(File kingdomsRootFile) {
		this.kingdomsRootFile = kingdomsRootFile;
	}

	public static void main(String[] args) throws InvalidFormatException, IOException {
		String path = "Application_data/Kingdoms";
		File kingdomFile = new File(path);
		ComputeSum sum = new ComputeSum(kingdomFile);
		sum.compute();
	}
}
