package fr.unistra.model.beans;

import static fr.unistra.utils.MainConstants.*;

import java.util.ArrayList;

/**
 * This class represent an organisme: its name and associated bioProject
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class Organism {

	private String				name		= "";
	private String				bioProject	= "";
	private String				group		= "";
	private String				subGroup	= "";
	private String				kingdom		= "";
	private ArrayList<String>	idList;
	private ArrayList<String>	structureList;
	private String				activeId	= "";

	/**
	 * Default constructor
	 */
	public Organism() {
		idList = new ArrayList<String>();
		structureList = new ArrayList<String>();
	}

	/**
	 * Constructor with the organism name and associated bioProject informed
	 * 
	 * @param name
	 *            : the organism name
	 * @param bioProject
	 *            : the organism bioProject
	 */
	public Organism(String name, String bioProject) {
		this();
		setName(name);
		setBioProject(bioProject);
	}

	/**
	 * Return the organism {@link #name}
	 * 
	 * @return The organism name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the organism {@link #name}
	 * 
	 * @param name
	 *            the organism name
	 */
	public void setName(String name) {
		if (name != null)
			this.name = name.trim();
	}

	/**
	 * Return the organism {@link #bioProject}
	 * 
	 * @return The organism {@link #bioProject}
	 */
	public String getBioProject() {
		return bioProject;
	}

	/**
	 * Set the organism {@link #bioProject}
	 * 
	 * @param bioProject
	 *            the organism bioProject
	 */
	public void setBioProject(String bioProject) {
		if (bioProject != null)
			this.bioProject = bioProject.trim();
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getSubGroup() {
		return subGroup;
	}

	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}

	public String getKingdom() {
		return kingdom;
	}

	public void setKingdom(String kingdom) {
		this.kingdom = kingdom;
	}

	public ArrayList<String> getIdList() {
		return idList;
	}

	public void setIdList(ArrayList<String> idList) {
		this.idList = idList;
	}

	public void addId(String id) {
		idList.add(id);
	}

	public void addStructure(String structure) {
		structureList.add(structure);
	}

	public String getIdStructure() {
		return getIdStructure(activeId);
	}

	public String getIdStructure(String Id) {
		try {
			String structure = structureList.get(idList.indexOf(Id));
			for (String type : genomeType) {
				if (containsTerm(structure, type)) {
					if (type == UNKNOWN)
						return CHROMOSOME;
					return normalise(type);
				}
			}
		} catch (Exception e) {
		}
		return CHROMOSOME;
	}

	public void setActiveId(String Id) {
		activeId = Id;
	}

	@Override
	public String toString() {
		return name + "[" + bioProject + "] [" + idList.size() + " genomeIdRef] " + group + " - " + subGroup;
	}

	@Override
	public boolean equals(Object object) {

		if (object == null)
			return false;

		if (object instanceof Organism) {
			Organism organism = (Organism) object;
			return organism.getName().compareToIgnoreCase(name) == 0;
		}
		if (object instanceof String) {
			String organismName = (String) object;
			return organismName.compareToIgnoreCase(name) == 0;
		}

		return false;
	}

	public boolean isIdListEmpty() {
		return idList.size() == 0;
	}

}
